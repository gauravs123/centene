({
	isLoggedInUserAppointed : function(component) {
		var action = component.get('c.isAppointedBroker');
        action.setCallback(this, (res) =>{
            var state = res.getState();
            if (state === "SUCCESS") {
                if(res.getReturnValue()){
                	component.set('v.isAppointed',true);
            	}
        	}
        });
        $A.enqueueAction(action);
	}
})