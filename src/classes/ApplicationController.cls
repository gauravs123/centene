public with sharing class ApplicationController {
    
    private final vlocity_ins__Application__c application;
    public List<vlocity_ins__Application__c> applicationData{get;set;}
    
    public static Map<Id, Id> contactAppOmniScriptIdMap{get;set;}
    
    public ApplicationController(ApexPages.StandardController standardController){
        application = (vlocity_ins__Application__c)standardController.getRecord();
        applicationData = [SELECT Id, Name,vlocity_ins__Status__c,Broker__c,Broker__r.id, CreatedById,CreatedDate
                           FROM vlocity_ins__Application__c
                           WHERE Id = :application.Id];
        
        
    }
    
    public PageReference applicationStatusChange() { 
        
        System.debug(' Application Status Change ');
        
        List<Id> borkerIdsList = new List<id>();
        List<vlocity_ins__Application__c> updAppStatusObjects = new List<vlocity_ins__Application__c>();
        
       contactAppOmniScriptIdMap = new Map<Id, Id>();
        for(vlocity_ins__Application__c applicationObj : applicationData ) { 
            String applicationStatus = applicationObj.vlocity_ins__Status__c;
            if (applicationStatus != null && applicationStatus == 'Manual Review') {
                borkerIdsList.add(applicationObj.broker__r.id);
                applicationObj.vlocity_ins__Status__c = 'Manual Reviewed';
                updAppStatusObjects.add(applicationObj);
                
                 for(vlocity_ins__OmniScriptInstance__C appOmniscript : 
                    [ select id, 
                     vlocity_ins__ResumeLink__C , 
                     vlocity_ins__ObjectName__C, 
                     LastModifiedDate, 
                     vlocity_ins__ObjectId__C, 
                     vlocity_ins__ObjectLabel__C, 
                     vlocity_ins__Status__C 
                     from vlocity_ins__OmniScriptInstance__C  
                     where vlocity_ins__ObjectLabel__c = 'Application'  
                     and vlocity_ins__ObjectId__C = :applicationObj.Id
                     order by vlocity_ins__ObjectId__C, LastModifiedDate desc limit 1 ]
                   ) 
                {
                       
                     //  contactAppOmniScriptMap.put(contact.Id, appOmniScript.vlocity_ins__ResumeLink__C);
                    contactAppOmniScriptIdMap.put(applicationObj.Broker__R.Id,appOmniScript.Id );
                    
                 } // end application to omniscript selection for loop
                
                
            }
            
        }
        
        if (updAppStatusObjects.size() >0 ) {
            update updAppStatusObjects;
            
        }
        
        System.debug('Sending Email to Broker');
        
       sendEmailToBroker(borkerIdsList, 'Manual_Review_Completed');
        
        
        
        // PageReference pageRef = new PageReference('lightning/r/vlocity_ins__Application__c/'+application.Id+'/view');
        PageReference pageRef = new PageReference('/'+application.Id);
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
    public static void sendEmailToBroker(List<Id> borkerIdsList, string templateName) {
        
         List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = :templateName];
        
        List<Contact> contacts = new List<Contact>();
        contacts = [select Id, Email, Contact_Type__c from Contact where id in :borkerIdsList];
        String contactType = '';

        



        
        for (Contact cnt : contacts) {
            contactType = cnt.Contact_Type__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);  
          System.debug(' Sending  Email ==> ' + contactType  + ' ====> ' + cnt.Email);
            mail.setTargetObjectId(cnt.id);// Any contact or User id of your record
            mail.setToAddresses(new list<string>{cnt.Email});
          
            mail.setWhatId(contactAppOmniScriptIdMap.get(cnt.id)); // Enter your record Id whose merge field you want to add in template
        

            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        }
        //Please replace "Your Email Template Unique Name" with your template name
        //
        //
        //
        
        
        
    }
    
    
    
}