public class AsyncIPHandler implements Queueable,Database.AllowsCallouts {
    
    public class AsyncIPHandlerException extends Exception {}
        
    static final String IPTYPE = 'IPType';
    static final String IPSUBTYPE = 'IPSubType';
    static final String IPDATA = 'vlcIPData';
    static final String IPSTATUS = 'vlcStatus';
    static final String SUCCESS = 'success';
    static final String IPException = 'result';
    static final String IPExceptionMessage ='error';
    static final String INPROGRESS ='InProgress';
    static string IP_SuccessMessage,IP_FailureMessage ;
    Map<String,Object> input,options;
    
    public AsyncIPHandler(Map<String, Object> input,Map<String, Object> options){
        this.input = input;
        this.options = options;
        system.debug('test'+options);
    }
    
    public void execute(QueueableContext context) {
        
        Map<string,object> IpOutput = new Map<string,object>();
        try{
            system.debug('test'+options+' ---Limits.getLimitQueueableJobs() '+Limits.getLimitQueueableJobs()+'-- Limits.getQueueableJobs()'+Limits.getQueueableJobs());
            if((Limits.getLimitQueueableJobs() - Limits.getQueueableJobs() > 0 && options.containskey(IPTYPE)&& options.containskey(IPSubType))) {
                IpOutput = (Map<string,object>)vlocity_ins.IntegrationProcedureService.runIntegrationService(options.get(IPTYPE)+'_'+options.get(IPSubType), input, options); 
                system.debug('IpOutput'+IpOutput);
                if(IpOutput.containskey('vlcStatus') && IpOutput.containskey(IPDATA)){
                    if(((string)IpOutput.get(IPSTATUS)).equalsIgnoreCase(INPROGRESS)){
                        options.put(IPDATA,IpOutput.get(IPDATA));
                        System.enqueueJob(new AsyncIPHandler(input,options));
                    }  
                }
                
                else if(IpOutput.get(SUCCESS)== false){
                    
                    IP_FailureMessage = 'Exception occured in IP '+options.get(IPTYPE)+'_'+options.get(IPSubType);
                    Throw new AsyncIPHandlerException(IP_FailureMessage);
                    
                }
                
                else{
                    IP_SuccessMessage = 'IP '+options.get(IPTYPE)+'_'+options.get(IPSubType)+' was Successfully executed' ;
                    NIPR_LogMessages.LogTransactions(AsyncIPHandler.class.getName() + ': ' + IP_SuccessMessage ,'' , JSON.serialize(IpOutput));

                }
                
            }
        }
        catch(Exception e){
            system.debug('fdsf'+e);
            system.debug('Exception'+e.getMessage());
            NIPR_LogMessages.LogTransactions(AsyncIPHandler.class.getName() + ': ' + IP_FailureMessage , e.getStackTraceString(), JSON.serialize(IpOutput));
        }
        
    }   
    
}