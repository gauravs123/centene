public class ResubmitAppointmentController {
    private vlocity_ins__Application__c application;
    //public static string applicationId{get;set;}
    public ResubmitAppointmentController(ApexPages.StandardController controller){
        application = (vlocity_ins__Application__c)controller.getRecord();
        //applicationId = application.Id;
        system.debug('application--->'+application);
    }
    public void onLoad() {
        String procedureName = 'NIPR_Appointment';
        system.debug('application--->'+application);
        Map<String, Object> input = new Map<String, Object>();
        input.put('ContextId', application.Id);
        Map<String, Object> options = new Map<String, Object>();
        options.put('chainable', true);
      
        Map<String,Object> ipOutput = (Map <String, Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, input, options);
        system.debug('Test--->'+JSON.serialize(ipOutput));
        
        // Agency Appointments Integration Procedure
        // RK - Feb 11, 2020
    	String agencyProcedureName = 'NIPR_AgencyAppointment';
        Map<String, Object> ipAgencyOutput = new Map<String, Object>();   
        
        ipAgencyOutput = (Map <String, Object>)
            vlocity_ins.IntegrationProcedureService.runIntegrationService(agencyProcedureName, input, options);
        System.debug(' IP Agency Output ' + ipAgencyOutput);
        
         NIPR_LogMessages.LogMessage(' Class: ResubmitAppointmentController - Executed IP ' + procedureName  + ' ===> ' + ' IP ' + agencyProcedureName +  application.Id);
      
        
        
    }
}