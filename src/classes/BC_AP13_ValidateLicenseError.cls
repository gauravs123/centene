/************************************************************************************************************
@Author      : IDC Offshore
@Name        : BC_AP13_ValidateLicenseError
@CreateDate  : 09/12/2018
@Description : This class is used to validate/check License & Certificate related data in License/Certificate Broker API(Novasys system,3rd Party API)
@Version     : <1.0>  
************************************************************************************************************/
public without sharing class BC_AP13_ValidateLicenseError {
    private static final string CLASS_NAME = 'BC_AP13_ValidateLicenseError';
    private static final string METHOD_NAME = 'beforeInsertLogic';
    private static final string METHOD_NAME1 = 'beforeUpdateLogic';
    private static final string MODULE_NAME = 'Broker Commissions'; 
/**************************************************************************************************************************************
    * Method Name : beforeInsertLogic
    * Parameters  : List
    * Return Type : void
    * Description : Before Insert Method for License object to validate/check License state data and throw error if try to insert more than 1 license per state
**************************************************************************************************************************************/
    public void beforeInsertLogic(List<vlocity_ins__ProducerLicense__c> licnobjlst)
    {
        try{
        vlocity_ins__ProducerLicense__c prdlst = licnobjlst[0];
         List<vlocity_ins__ProducerLicense__c> prdlist = [select id,vlocity_ins__ProducerId__c,vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerLicense__c where vlocity_ins__ProducerId__c =:prdlst.vlocity_ins__ProducerId__c and vlocity_ins__Jurisdiction__c=:prdlst.vlocity_ins__Jurisdiction__c and vlocity_ins__LicenseType__c =: prdlst.vlocity_ins__LicenseType__c and Name =: prdlst.Name limit 1];
        if(prdlist !=null && prdlist.size() > 0){
            prdlst.addError(System.Label.BC_LicenseStateErrorMessage);
        } 
        }catch(Exception exObj){ExceptionHandler.logHandledExceptions(exObj, CLASS_NAME, METHOD_NAME, MODULE_NAME);}
    }
/**************************************************************************************************************************************
    * Method Name : beforeUpdateLogic
    * Parameters  : List
    * Return Type : void
    * Description : Before Update Method for License object to validate/check License state data and throw error if try to update more than 1 license per state
**************************************************************************************************************************************/
    public void beforeUpdateLogic(List<vlocity_ins__ProducerLicense__c> licnobjlst)
    {
        try{
        vlocity_ins__ProducerLicense__c prdlst = licnobjlst[0];
        List<vlocity_ins__ProducerLicense__c> prdlist = [select id,vlocity_ins__ProducerId__c,vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerLicense__c where vlocity_ins__ProducerId__c =:prdlst.vlocity_ins__ProducerId__c and vlocity_ins__Jurisdiction__c=:prdlst.vlocity_ins__Jurisdiction__c limit 1];
        if(prdlist !=null && prdlist.size() > 0 && prdlist[0].Id != prdlst.Id){
            prdlst.addError(System.Label.BC_LicenseStateErrorMessage);
        }         
        }catch(Exception exObj){ExceptionHandler.logHandledExceptions(exObj, CLASS_NAME, METHOD_NAME1, MODULE_NAME);}
    } 
/**************************************************************************************************************************************
    * Method Name : beforeInsertLogic
    * Parameters  : List
    * Return Type : void
    * Description : Before Insert Method for Certificate object to validate/check Certificate state data and throw error if try to insert more than 1 Certificate per state
**************************************************************************************************************************************/
    public void beforeInsertLogic(List<vlocity_ins__ProducerEducation__c> certobjlst)
    {
        try{
        vlocity_ins__ProducerEducation__c prded = certobjlst[0];
         List<vlocity_ins__ProducerEducation__c> prdedu = [select id,vlocity_ins__ProducerId__c,vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerEducation__c where vlocity_ins__ProducerId__c =:prded.vlocity_ins__ProducerId__c and PlanYear__c=:prded.PlanYear__c and npn__c=:prded.npn__c limit 1];
        if(prdedu !=null && prdedu.size() > 0){
            prded.addError(System.Label.BC_CertificateStateErrorMessage);
        } 
        }catch(Exception exObj){ExceptionHandler.logHandledExceptions(exObj, CLASS_NAME, METHOD_NAME, MODULE_NAME);}
    } 
/**************************************************************************************************************************************
    * Method Name : beforeUpdateLogic
    * Parameters  : List
    * Return Type : void
    * Description : Before Update Method for Certificate object to validate/check Certificate state data and throw error if try to update more than 1 Certificate per state
**************************************************************************************************************************************/
    public void beforeUpdateLogic(List<vlocity_ins__ProducerEducation__c> certobjlst)
    {
        /*try{
        set<String> setA = new set<String> ();
        set<String> setB = new set<String> ();
        set<String> setC = new set<String> ();
        set<String> setExistingKey = new set<String> ();
        for(vlocity_ins__ProducerEducation__c edu: certobjlst){
            setA.add(edu.vlocity_ins__ProducerId__c);
            setB.add(edu.npn__c);
            setC.add(edu.PlanYear__c );
        }
        vlocity_ins__ProducerEducation__c prded = certobjlst[0];
         List<vlocity_ins__ProducerEducation__c> prdedu = [select id,vlocity_ins__ProducerId__c,vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerEducation__c 
                                                            where ID Not In:certobjlst and vlocity_ins__ProducerId__c IN: setA and PlanYear__c In: setC and npn__c In: setB];
        for(vlocity_ins__ProducerEducation__c edu: prdedu){
            setExistingKey.add(edu.vlocity_ins__ProducerId__c+'-'+edu.npn__c+'-'+edu.PlanYear__c );
        }
        for(vlocity_ins__ProducerEducation__c edu: certobjlst){
            if(setExistingKey.contains(edu.vlocity_ins__ProducerId__c+'-'+edu.npn__c+'-'+edu.PlanYear__c )){
                 prded.addError(System.Label.BC_CertificateStateErrorMessage);
            }
        }
      
        }catch(Exception exObj){ExceptionHandler.logHandledExceptions(exObj, CLASS_NAME, METHOD_NAME1, MODULE_NAME);}*/
    } 
}