/************************************************************/
/* Test Class for : VerifiedFirstScheduler
/* Modified Date | By  | Changes 
/* Feb 24, 2020  | Plum Tree Group - RK | Initial Version 
/************************************************************/

@isTest
public class VerifiedFirstQueueable_Test {

  /**********************************************************/
  // Create Test Data    
  /**********************************************************/
    
    @testSetup static void setup() {
        
        // Create Test Contact
        Contact testContact = new Contact();
        testContact.FirstName = 'Hank';
        testContact.LastName  = 'Mess';
        testContact.Birthdate = Date.newInstance(1977, 07, 07);
        testContact.Email     = 'rkona@plumtreegroup.net';
        testContact.vlocity_ins__SocialSecurityNumber__c = '333221111';
        testContact.Tax_ID__c = '333221111';
        testContact.TaxId__c = '333221111';
         testContact.Is_Pilot_Broker__c = true;
        insert testContact;
        
        System.debug('RK: Created Contact Id ' + testContact.Id);
     
    // Create Test Application with Status Signed
        vlocity_ins__Application__C testApplication = new vlocity_ins__Application__C();
        testApplication.Name = 'Test Application';
        testApplication.Broker__c = testContact.Id;
        testApplication.vlocity_ins__Status__c = 'Signed';
    
        insert testApplication;
        
        System.debug('RK: Created Application Id ' + testApplication.Id);
    }
    
  // Test Method to to run Queueable Execute Method
    @isTest static void testVerifiedFirstQueueable() {
         ID jobID = System.enqueueJob(new VerifiedFirstQueueable());
    }    
    
    
}