@isTest(seeAllData=false)
public class NIPRSubmissionInvocableTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	}
    static testMethod void testNIPRSubmission()
    {
        getAllMetadata();
        List<Id> appIds = new List<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(20);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        NIPRSubmissionInvocable NIPRSub = new NIPRSubmissionInvocable();
        NIPRSubmissionInvocable.executeIntegrationProcedure(appIds);
    }
}