public class BackgroundCheckFailed {
    
    public static String contactTypes;
    
    public static String getContactTypes() {
        return contactTypes;
    }
    
    @InvocableMethod
    
    public static void sendBackgroundCheckFailedEmail(List<ID> contactIds ) {
        
        System.debug('Send Sub Producer Email to ' + contactIds);
        
        Map<Id, Contact> contacts = new Map<Id, Contact>();
        List<Id> accntIds = new List<Id>();
        List<Id> producerIds = new List<Id>();
        
        String contactType = '';
        for (Contact contact: [select Id, AccountId , Contact_Type__c  FROM Contact WHERE ID in :contactIds]) {
            if (contact.Contact_Type__c != null) { 
                contactType = contact.Contact_Type__C;
            }
            else {
                contactType = '';
            }
            if ( (contactType.contains('Sub Producer')) && (contact.AccountId != null)) {
                accntIds.add(contact.AccountId);
            }
        }
        
        if (accntIds.size() > 0 ) {
            for(Account acc:[Select Id, (Select Id, Contact_Type__c From Contacts where Contact_Type__c ='Principal'  and Broker_Status__c = 'Active' limit 1) From Account Where Id=:accntIds]) {
                if(acc.Contacts != null && acc.Contacts.size() >0) {
                    System.debug('Found Principal Record ' + acc.Contacts);
                    for(Contact contact : acc.Contacts) {
                        producerIds.add(contact.id);
                        System.debug(' Principal Record Id ' + contact.id);
                    }
                        
                        
                } else {
                    System.debug('No Principal Record Found');
                }
            }
        }
        
        if (producerIds.size() > 0 ) {
            SendEmailToPrincipal(producerIds);
        }

    }
    
    public static void SendEmailToPrincipal(List<Id> producerIds) {
        
        List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'BG_Check_Failed'];
        
        List<Contact> contacts = new List<Contact>();
        contacts = [select Id, Email from Contact where id in :producerIds];
        
        for (Contact cnt : contacts) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(lstEmailTemplates[0].Id);
        mail.setSaveAsActivity(false);        
        mail.setTargetObjectId(cnt.id);// Any contact or User id of your record
        mail.setToAddresses(new list<string>{cnt.Email});
        
        //mail.setWhatId(cnt.id); // Enter your record Id whose merge field you want to add in template
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        }
        //Please replace "Your Email Template Unique Name" with your template name
        //
        //
        //
        
        
    }
   
}