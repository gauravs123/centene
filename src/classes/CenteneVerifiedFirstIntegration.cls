global without sharing class CenteneVerifiedFirstIntegration implements vlocity_ins.VlocityOpenInterface2 {
    
    
    public CenteneVerifiedFirstIntegration() {
        
    }
    
    global Boolean invokeMethod(String methodName, 
                                Map < String, Object > inputs, 
                                Map < String, Object > output, 
                                Map < String, Object > options) 
    {
        if(methodName == 'VerifyContact'){
            
            String producerId   = (String) inputs.get('ContactId');
            System.debug(' Got Producer Id');
            
            VerifyContact(producerId);
            
            return true;
        }  
        
        return false;
        
    }
    
    public static Boolean VerifyContact(String producerId) {
        
        if (producerId != null) {
            System.debug('Centene Verification for Producer Id ' + producerId);
            producerId = '0030x00000bGlMEAA0';   
            if(getExistingReport(producerId).isEmpty()){
                System.debug('Centene Verification for Producer static Id ' + producerId);
                List<vfirst.SearchObjectWhenCreate.Request> requests = new List<vfirst.SearchObjectWhenCreate.Request>();
                requests.add(new vfirst.SearchObjectWhenCreate.Request());
                requests[0].objectId = producerId;
                requests[0].orderMethod = 'instant';
                vfirst.SearchObjectWhenCreate.searchObject(requests);
            }
            //updateBGCheckFieds(producerId);
        }
        return true;
        
    }
    
    
    public static List<vfirst__Verified_First_Report__c> getExistingReport(String producerId){
        return [select id from vfirst__Verified_First_Report__c where CreatedDate = LAST_N_DAYS:365 and vfirst__Stage__c = 'Complete' And vfirst__Contact__c= :producerId];
    }
    
    public static boolean updateBGCheckFieds(String producerId) {
        
        List<Contact> cnts = new List<Contact>();
        
        if (producerId != null ) {
            
            cnts = [select id, BGCheck_Status__C, LastBGCheck__c  from Contact where id = :producerId];
            if(cnts.size() >0) {
                cnts[0].BGCheck_Status__C = 'Passed';
                cnts[0].LastBGCheck__c = Date.today() - 10;
                update cnts;
            }
        }
        return true;
    }
    
    
}