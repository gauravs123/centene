public class NIPRSingleUserHandler {
    @InvocableMethod
    public static void callIP(List<String> testParam){
        System.debug('Flow Run');
        Contact loggedContact = [SELECT ToBeInNIPRBatch__c,Is_Pilot_Broker__c, NPN__c FROM Contact WHERE Id =: testParam[0]];
        //loggedContact.ToBeInNIPRBatch__c = false;
        //update loggedContact;
        /* Initialize variables */
        if(String.isNotBlank(loggedContact.NPN__c) && loggedContact.Is_Pilot_Broker__c){
            String procedureName = 'NIPR_ProducerAdditionalInfoUpdate';
            Map <String, Object> ipInput = new Map <String, Object> ();
            Map <String, Object> ipOutput = new Map <String, Object> ();
            Map <String, Object> ipOptions = new Map <String, Object> ();
            
            /* Populating input map for an Integration Procedure. 
			Follow whatever structure your VIP expects */
            ipInput.put('ContextId', loggedContact.Id);
            
            /* Call the IP via runIntegrationService, 
			and save the output to ipOutput */
            ipOutput = (Map <String, Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, ipInput, ipOptions);
            
            System.debug('IP Output: ' + ipOutput);
        }
    }
    
    public class FlowInputs {
        @InvocableVariable
        public String contactId;
    }
    
}