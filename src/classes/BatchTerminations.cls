/************************************************************
* Call Vlocity Integration Procedure to Terminate Contracts
/* Modified Date | By  | Changes 
// Dec 20, 2019  | Rangesh Kona | Initial Version 
*/

global class BatchTerminations implements 
Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
    
    // get the Application Object Data
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        System.debug(' Collecting Batch Data by Query ');        
        return Database.getQueryLocator(
            'SELECT ID FROM vlocity_ins__Application__C ' + 
            'Where Check_Contract_Termination__c  = true and IsPilotBroker__c = true'
        );
    }
    
     global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
        // We now call the batch class to be scheduled
        BatchTerminations b = new BatchTerminations ();
        
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,1);
    }
    
    // Call Integration Procedure
    global void execute(Database.BatchableContext bc, List<vlocity_ins__Application__C> appObjects){
        
        System.debug(' Executing Batch  Code ');
        for(vlocity_ins__Application__c appObj : appObjects) {
            // call Integration Procedure
            System.debug('RK : Inside execute command');
            // Agenct Submission
            System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => appObj.Id},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' => 'Terminations',
                                                     					'queueableChainable'=> true}));
        } 
    }    
    
    // How many Application Records processed? 
    global void finish(Database.BatchableContext bc) {

    }  
    
} 
// end of Batch Termination Class