@isTest(seeAllData=false)
public class RegenerateAppContractDetailContTest 
{
    @TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	}
    static testMethod void TestReGenerateApp()
    {
        getAllMetadata();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(200);
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController(applicationList[0]);
            RegenerateAppContractDetailController controller = new RegenerateAppContractDetailController(sc);
            controller.regenerateContract();
        Test.stopTest();
    }
}