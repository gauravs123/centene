@isTest(seeAllData=false)
private class NIPRBatchHandlerTest {
    //@TestVisible 
     private static testMethod void doTest(){   
                     Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
            Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
            agency.Name = 'Agency Test';
            agency.TaxId__c = 'AGENCY';
            agency.BillingCity = 'Chandler';
            agency.BillingState = 'AZ';
            agency.RecordTypeId = accountAgencyRT;
            insert agency;
            
            Contact agent = new Contact();
            agent.FirstName = 'John';
            agent.LastName = 'Adams';
            agent.TaxId__c = 'ABCDEFGHI';
            agent.MailingCity = 'Tempe';
            agent.MailingState = 'AZ';
            agent.MailingStreet = '20 S Mill Ave' ;
            agent.MailingPostalCode = '85251';
            agent.Phone = '3334445555';
            agent.RecordTypeId = contactAgentRT;
            agent.AccountId = agency.Id;
            agent.ContractType__c = 'Agency Authorized/Affiliated agent';
        	insert agent;

        
        String procedureName = 'NIPRProducerBatchLookupReport';
        Map <String, Object> ipInput = new Map <String, Object> ();
        Map <String, Object> ipOutput = new Map <String, Object> ();
        Map <String, Object> ipOptions = new Map <String, Object> ();
		NIPRBatchHandler NIPRSub = new NIPRBatchHandler();
           Test.startTest();
        Contact con = [select id from Contact Limit 1];
        System.debug('*****con*****'+con);

         
         Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        NIPRHttpCallout NIPRCallout = new NIPRHttpCallout();
        NIPRBatchHandler.NIPRBatchHandler(); 
         
         

        Test.stopTest();
        	
     }

}