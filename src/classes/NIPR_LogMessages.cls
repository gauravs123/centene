global without sharing class NIPR_LogMessages implements vlocity_ins.VlocityOpenInterface2{
    
   global Boolean invokeMethod(String methodName, 
                                Map < String, Object > inputs, 
                                Map < String, Object > output, 
                                Map < String, Object > options) 
    {
        String message = (String) inputs.get('LogMessage');
        
        if(methodName == 'LogMessage'){
            LogMessage(message);
        }
        else if(methodName == 'LogNIPRTransaction'){
            LogNIPRTransaction(inputs, output, options);
        }
        return true;
    }
    
    
    public static void LogNiprTransaction(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options){
        String message = options.get('LogMessage') != null ? (String)options.get('LogMessage') : (String)inputs.get('LogMessage');
        String existingLogId = options.get('processLogId') != null ? (String)options.get('processLogId') : (String)inputs.get('processLogId');
        LogTransactions(existingLogId, message, JSON.serialize(inputs).unescapeHtml4(),'', output);
    }
     
    public static void LogTransactions(String message, String input, String output){
        LogTransactions(null, message, input, output, new Map<String,Object>());
    }
    
    public static void LogTransactions(String recordId, String message, String input, String output, Map<String,Object> omnioutput){
        NIPR_Logs__c  niprLog = new NIPR_Logs__c();
        if(String.isNotBlank(recordId)){
            niprLog.Id = recordId;
        }
        niprLog.Message__c = message;
        niprLog.Request_Input__c  = input.length() > 32768  ? input.substring(0,32768 ) : input;
        niprLog.NIPR_Output__c   = output.length() > 131072 ? output.substring(0,131072) : output;
        Database.UpsertResult result = database.upsert(niprLog,false);
        omnioutput.put('processLogId', result.getId());
    }
            
            
    public static void LogMessage(String message) {
        System.debug(' Log Message : ' + message);
        NIPR_Logs__c  niprLogs = new NIPR_Logs__c();
        niprLogs.Message__c = message;
      //  insert niprLogs;
    }

}