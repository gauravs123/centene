@isTest(seeAllData = false)
public class CounterSignatureActionControllerTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	}
    static testMethod void TestCounterSignatureAction()
    {
        getAllMetadata();
        
        Map<Id,Id> mapDocumentContent = new Map<Id, Id>();
        Map<Id,Id> parentIdContentDocumentIdMap = new Map<Id, Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(2);
        List<ContentDocumentLink> CDLList = TestDataFactory.createCDL(applicationList[0].Id);
        PageReference pageRef = Page.CounterSignature;
		Test.setCurrentPage(pageRef);
		ApexPages.standardSetController sc = new ApexPages.standardSetController(applicationList);
        sc.setSelected(applicationList);
		CounterSignatureActionController controller = new CounterSignatureActionController(sc);
        CounterSignatureActionController.setApplicationId(applicationList);    
        controller.updateApplications();
        //System.debug('CDLList in test class::::'+CDLList);
        Test.startTest();
        controller.signApplications();
        CounterSignatureActionController.logMessage('Test Str');
        CounterSignatureActionController.callSignApplications();
        
        
        Test.stopTest();   
    }
    
}