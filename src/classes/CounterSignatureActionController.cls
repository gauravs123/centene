/**************************************************************
* Author : PlumTreeGroup - RK
* Created Date: Dec 10th, 2019
* Purpose: Counter Signaure for Carrier
**************************************************************/

public class CounterSignatureActionController {
    
    private ApexPages.StandardSetController standardController;
    public static  List<vlocity_ins__Application__c> selectedApps;
    
    // Carrier Signed Document Id    
    public static ID signedDocumentId;
    
    // Carrier Signed Document Version Id
    public static ID signedDocVersionId;
    
    public CounterSignatureActionController(ApexPages.StandardSetController standardController) {
        this.standardController = standardController;
    }
    
    // update application id for details view button
    public static void setApplicationId(List<vlocity_ins__Application__c> applicationObjList) {        
        selectedApps = applicationObjList; 
    }
    
    // for the list view button    
    public PageReference signApplications() {       
        
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        selectedApps = (List<vlocity_ins__Application__c>) standardController.getSelected();
       
        // process each application
        callSignApplications();
        
        // go back to Application List View
        PageReference pageRef = new PageReference('/lightning/o/vlocity_ins__Application__c/list?filterName=Recent');
        pageRef.setRedirect(true);
        return pageRef;
        // return null;        
    }
    
    // update CounterSign checkbox 
    public static void updateCounterSignCheckbox() {
        List<vlocity_ins__Application__c> appObjects = new List<vlocity_ins__Application__c>(); 
        if (selectedApps.size() > 0) {
            for (vlocity_ins__Application__c appObj : selectedApps ) {
                appObj.CounterSignature__c = true;
               appObjects.add(appObj);
            }
        }
        if ( appObjects.size() > 0) {
            update appObjects;
        }
    }
    // end-method-updateCounterSignCheckbox
    
    // find the attached carrier signature document and Ambetter Producer document
    // merge it using DocuSign
    public static void callSignApplications() {  
        Map<Id,Id> mapDocumentContent = new Map<Id,Id>();
        Map<Id,Id> parentIdContentDocumentIdMap = new Map<Id,Id>();
        Set<Id> appsIds = new Set<Id>();
        for(vlocity_ins__Application__c app : selectedApps) {
            appsIds.add(app.Id);
        }
        System.debug('appsIds --> ' + appsIds);
        updateCounterSignCheckbox();

        // get the map <appid>, <contentDocumentId> 
        for ( ContentDocumentLink cl: [ SELECT 
                                       Id, 
                                       ContentDocumentId, 
                                       ContentDocument.LatestPublishedVersionId,
                                       LinkedEntityId 
                                       FROM 
                                       ContentDocumentLink 
                                       WHERE LinkedEntityId IN: appsIds 
                                       AND  ( 
                                             ContentDocument.title like '%Ambetter Producer Agreement.pdf%' OR 
                                             ContentDocument.title like '%Ambetter Sub Producer Agreement.pdf%'
                                       ) ] 
            ) 
        {                            
            parentIdContentDocumentIdMap.put(cl.LinkedEntityId, cl.ContentDocumentId );                          
        }
        
        // get the latest Document version from the ContentVersion 
        // in the map <documentId>, <LatestVersion Document>
        for ( ContentVersion cv:  [SELECT VersionData,ContentDocumentId,Id FROM ContentVersion WHERE ContentDocumentId IN: parentIdContentDocumentIdMap.values()
                                   AND IsLatest = true AND ( Title LIKE '%Ambetter Producer Agreement.pdf%' OR 
                                                             Title LIKE '%Ambetter Sub Producer Agreement.pdf%' )
                                   AND FileType = 'PDF' ] ) {
                                       System.debug('Document ID ' + cv.Id);
                                       mapDocumentContent.put(cv.ContentDocumentId, cv.Id);
                                   }
        
        System.debug('Adding Signed Document ID ==> ...  ' + signedDocumentId + ' ====> ' + signedDocVersionId);
        for (Id appId: appsIds) {
            //  if (signedDocumentId != null ) {
            attachSignatureFile(appId);
            //  }
            CounterSignatureActionController.sendEmail(appId,mapDocumentContent,parentIdContentDocumentIdMap, signedDocVersionId);
        } // end-for        
    } // end-method-callSignApplications
    
    
    // Make a future call to DocuSign
    // DocuSign successfully merges then status gets updated to 'Counter Sign' ( Check DocuSign connections)
    @future(callout=true)
    public static void sendEmail(Id appId,Map<Id,Id> mapDocumentContent, Map<Id,Id> parentIdContentDocumentIdMap, Id signedDocVersionId ) {
        
        Id mySourceId = appId;        
        dfsle.Recipient myEmployer = dfsle.Recipient.fromSource('dummy@emaik.net', 'dummy@emaik.net', null,'',new dfsle.Entity(appId));       
        List<dfsle.Document> allDocumentList = new List<dfsle.Document>();
        Id myDocumentId = mapDocumentContent.get(parentIdContentDocumentIdMap.get(appId));

        System.debug('Ambetter Document Version Id ' + myDocumentId);
        System.debug('Carrier Signed Document Version Id ' + signedDocVersionId);
        if(myDocumentId != null && String.isNotEmpty(signedDocVersionId) && !Test.isRunningTest())
        {
            allDocumentList.addAll(dfsle.DocumentService.getDocuments(ContentVersion.getSObjectType(),new Set<Id> {myDocumentId, signedDocVersionId}));  
        }
        dfsle.CustomField myCustomField1 = new dfsle.CustomField('text', 'Contract_Type__C', 'Principal' , null, true, true);
        
        if(!Test.isRunningTest())
        {
            dfsle.Envelope myEnvelope = dfsle.EnvelopeService.getEmptyEnvelope(new dfsle.Entity(mySourceId)).withDocuments(allDocumentList).withRecipients(new List<dfsle.Recipient> { myEmployer }).withCustomFields(new List<dfsle.CustomField> { myCustomField1 } );
            myEnvelope = dfsle.EnvelopeService.sendEnvelope(myEnvelope, true);
        }
        
    }
    
    public static void logMessage(String message) {
        System.debug(' RK Debug ' + message);
    }
    
    public static void attachSignatureFile(Id appId) {
        
        // generate signature PDF file
        PageReference pdf = Page.CarrierDynamicSignaturePDF;
        pdf.getParameters().put('appId', appId);
        Blob body;
        
        System.debug('PDF Object Details ' + pdf);
        if (pdf != null) {
            body = Test.isRunningTest() ? Blob.valueOf('223') : pdf.getContent();
            
            // attach generated pdf to ContentVersion
            ContentVersion cv       = new ContentVersion();
            cv.Title                = 'CarrierSignature';
            cv.PathOnClient         = 'CarrierSignature.pdf';
            cv.VersionData          = body;
            cv.IsMajorVersion       = true;
            Insert cv; 
                      
            List<ContentVersion> docVersionList = [select id, ContentDocumentId from ContentVersion where Id = :cv.id];
            if (docVersionList.size() > 0 ) {
                signedDocVersionId  = docVersionList[0].id;
                signedDocumentId    = docVersionList[0].ContentDocumentId;
            }
            
            // Attach Document to the Application
            //Get Content Documents
            List<ContentDocument> docList = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument where id = :signedDocumentId];
            
            //Create ContentDocumentLink 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = appId;
            cdl.ContentDocumentId = docList[0].Id;
            cdl.shareType = 'V';
            Insert cdl;
        } // end-if-pdf
    } //end-attachSignatureFile
    
    public PageReference updateApplications() {       
        // Call StandardSetController 'save' method to update (optional, you can use your own DML)
        PageReference pageRef = new PageReference('/');
        pageRef.setRedirect(true);
        return pageRef;
        //return standardController.save();   
    }
}