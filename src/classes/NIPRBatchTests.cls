@isTest(seeAllData=false)
public class NIPRBatchTests {

    
    static testMethod void TestBatchTerminations()
    {
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Carrier').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        agency.Is_Applicable_For_Agency__c = True;
        agency.is_Pilot_Broker__c = true;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Sub Producer';
        agent.LastBGCheck__c = System.Today();
        agent.is_Pilot_Broker__c = true;
        insert agent;
        
        Contact agent11 = new Contact();
        agent11.FirstName = 'John';
        agent11.LastName = 'Adams';
        agent11.email = 'email@gmail.com';
        agent11.TaxId__c = 'ABCDEFGHI';
        agent11.MailingCity = 'Tempe';
        agent11.MailingState = 'AZ';
        agent11.MailingStreet = '20 S Mill Ave' ;
        agent11.MailingPostalCode = '85251';
        agent11.Phone = '3334445555';
        agent11.RecordTypeId = contactAgentRT;
        agent11.AccountId = agency.Id;
        agent11.Contact_Type__c = 'Principal'; 
        agent11.is_Pilot_Broker__c = true;
        insert agent11;        
        
        Account a = new Account(
            Name = 'TestAcc',
            Agent_Id__c = '12345',
            Novasys_DBA_Name__c = 'Test',
            BillingStreet = '123 Test Street',
            BillingCity = 'Test City',
            BillingState = 'TX',
            BillingPostalCode = '12345',
            //Agent_Master_Id__c = '19782',
            TaxId__c = '987654321',
            Email__c = 'testEmail@test.com',
            vlocity_ins__NPNNumber__c = '123456',
            NPN__c = '123456',
            Phone = '5555555555',
            vlocity_ins__TaxID__c = '12345',
            agentsync__ID_FEIN__c= '1234141',
            Fax = '6666666666',
            is_Pilot_Broker__C= true
        );
        
        insert a; 
        
        Contact c = new Contact(
            AccountId = a.Id,
            Agent_Id__c = '12345',
            Agent_Master_Id__c = '123456',
            BirthDate = Date.today(),
            MailingStreet = '555 Test Street',
            MailingCity  = 'Test City',
            MailingState = 'TX',
            MailingPostalCode = '55555',
            Broker_Lead_Status__c = 'Appointed',
            Contact_Type__c = '',
            Contract_Term__c = Date.today(),
            Email = 'test@test.com',
            FirstName = 'Test',
            LastName = 'Jacque',
            NPN__c = '55555',
            Phone = '5555555555',
            vlocity_ins__ProducerNumber__c = '12345',
            Status__c = 'Active',
            Tech_Awaiting_Novasys_Receipt__c = true,
            is_Pilot_Broker__C= true
        );
        
        insert c;
        Contract cont = new Contract();
        cont.AccountId = agency.Id;
        cont.Contract_Status__c = 'Active';
        insert cont;
        
        vlocity_ins__Application__c appli = new vlocity_ins__Application__c();
        appli.State_Carrier__c= 'AR - Celtic Insurance Company';
        appli.Broker__c = agent.Id;
        appli.vlocity_ins__Status__c = 'New';
        insert appli;
        
        vlocity_ins__AgencyAppointment__c agenApp = new vlocity_ins__AgencyAppointment__c();
        agenApp.vlocity_ins__AgencyBrokerageId__c = agency.Id;
        agenApp.vlocity_ins__Status__c='Pending';
        insert agenApp;
        
        vlocity_ins__ProducerAppointment__c propApp = new vlocity_ins__ProducerAppointment__c();
        propApp.vlocity_ins__ProducerId__c = agent11.Id;
        propApp.vlocity_ins__Status__c='Pending';
        insert propApp;
        Date today = Date.Today();
        Date nextYear = today.addDays(365);
        vlocity_ins__ProducerLicense__c testLicense = new vlocity_ins__ProducerLicense__c(
            
            vlocity_ins__EffectiveDate__c = Date.today().addDays(0-90),
            Name = 'testLicense',
            vlocity_ins__LicenseType__c = 'General Lines Agncy/Agnt',
            vlocity_ins__Status__c = 'Active',
            vlocity_ins__ProducerId__c = c.Id,
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ExpirationDate__c = Date.today().addDays(270),
			Novasys_Primary_Id__c = '12345'
        );
        
        insert testLicense;
        System.debug(testLicense.Id);
        testLicense.Tech_Awaiting_Novasys_Receipt__c = true;
        update testLicense;
        testLicense.Name = 'testLicense1';
        update testLicense;
        testLicense.vlocity_ins__Jurisdiction__c = 'TX';
        update testLicense;
        testLicense.vlocity_ins__ExpirationDate__c = nextYear;
        update testLicense;
        testLicense.vlocity_ins__EffectiveDate__c = today;
        update testLicense;
        
        
        vlocity_ins__AgencyLicense__c testLicense1 = new vlocity_ins__AgencyLicense__c(
            vlocity_ins__EffectiveDate__c = today.addDays(0-90),
            Name = 'testLicense',
            vlocity_ins__AgencyBrokerageId__c = a.Id,
            vlocity_ins__LicenseType__c = 'General Lines Agncy/Agnt',
            vlocity_ins__Status__c = 'Active',
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ExpirationDate__c = today.addDays(270),
            Novasys_Primary_Id__c = '12345'
		);
        insert testLicense1;
        System.debug(testLicense1.Id);
        testLicense1.Tech_Awaiting_Novasys_Receipt__c = true;
        update testLicense;
        testLicense1.Name = 'testLicense1';
        update testLicense;
        testLicense1.vlocity_ins__Jurisdiction__c = 'TX';
        update testLicense1;
        testLicense.vlocity_ins__ExpirationDate__c = nextYear;
        update testLicense1;
        testLicense1.vlocity_ins__EffectiveDate__c = today;
        update testLicense1;
        Test.setCreatedDate(agenApp.Id, system.Now().addDays(-4));
        Test.setCreatedDate(propApp.Id, system.Now().addDays(-4));
        vlocity_ins__AgencyLicense__c ageLic = new vlocity_ins__AgencyLicense__c();
        ageLic.vlocity_ins__AgencyBrokerageId__c=agency.Id;
        ageLic.vlocity_ins__Status__c= 'Active';
        insert ageLic;
        
        RecordType rtype = [SELECT Id from RecordType where Name = 'Ambetter Agent' LIMIT 1];
        RecordType rtype1 = [SELECT Id from RecordType where Name = 'Ambetter Agency' LIMIT 1];
        insert new Account(RecordTypeId =rtype1.Id,NPN__c='12323323',is_Pilot_Broker__c = true,Name = 'Last Account',NIPRLastRundate__c=Date.today().addDays(-16));
        
        insert new Contact(RecordTypeId =rtype.Id, Tech_Partner_User_Created__c=true, NPN__c='12323323',is_Pilot_Broker__c = true,LastName = 'Last Contact',NIPRLastRundate__c=Date.today().addDays(-16));
        database.executeBatch(new NIPRAccountUpdateBatch());
        system.schedule('Schedukle', '0 0 0/12 * * ? *', new NIPRAccountUpdateBatch());
        database.executeBatch(new NIPRContactUpdateBatch());
        system.schedule('Schedukle3', '0 0 0/12 * * ? *', new NIPRContactUpdateBatch());
        
        database.executeBatch(new NIPRProducerPollingBatch());
        system.schedule('Schedukle1', '0 0 0/12 * * ? *', new NIPRProducerPollingBatch());
        
        database.executeBatch(new NIPRAgencyPollingBatch());
        system.schedule('Schedukle2', '0 0 0/12 * * ? *', new NIPRAgencyPollingBatch());
        database.executeBatch(new CancelQueuedJobs());
        
    }
    
    
}