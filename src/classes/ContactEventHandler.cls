public class ContactEventHandler{
    public static boolean isfired = false;
    public static void afterUpdate(List<Contact> contacts,Map<Id, Contact> oldContacts,Map<Id, Contact> contactNewMap){
        System.debug('ContactEvent Trigger Update');
        Map<Id, List<vlocity_ins__ProducerLicense__c>> licensesByContact = new Map<Id, List<vlocity_ins__ProducerLicense__c>>();
        Map<Id, List<Contract>> contractsByContact = new Map<Id, List<Contract>>();
        Map<Id, List<vlocity_ins__ProducerEducation__c>> educationsByContact = new Map<Id, List<vlocity_ins__ProducerEducation__c>>();
        Map<Id, List<vlocity_ins__ProducerAppointment__c>> appointmentsByContact = new Map<Id, List<vlocity_ins__ProducerAppointment__c>>();
        Map<Id, List<vlocity_ins__AgencyAppointment__c>> agencyAppointmentsByAccount = new Map<Id, List<vlocity_ins__AgencyAppointment__c>>();
        Map<Id, List<vlocity_ins__AgencyLicense__c>> agencyLicensesByAccount = new Map<Id, List<vlocity_ins__AgencyLicense__c>>();
        
                        
       
        Set<Id> conIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        
        Map<Id, Account> accountMap = new Map<Id, Account>(); 
        for(Contact con : contacts){
            Contact contactOld = oldContacts.get(con.Id);
            if (((con.Agent_Master_Id__c != contactOld.Agent_Master_Id__c) || (contactOld.Agent_Id__c != con.Agent_Id__c)) && 
            (!String.isBlank(con.Agent_Master_Id__c) && (!String.isBlank(con.Agent_Id__c)))) {
                conIds.add(con.Id);
                if (!String.isBlank(con.accountId)) {
                    accIds.add(con.accountId);
                }
            }
        }
        Account[] accs = [SELECT Id, Agent_Master_Id__c, Agent_Id__c FROM Account WHERE Id IN: accIds];
        for (Account a : accs) {
            accountMap.put(a.Id, a);
        }
        //System.assert(false,'>>>>'+accIds);
        if(!accIds.isEmpty()) {
            System.debug(accIds);
            //NEED TO UPDATE THE BELOW QUERY TO FILTER FOR 'WHERE vlocity_ins__AgencyBrokerageId__c IN: accIds' AFTER DEMO MEETING
            for (vlocity_ins__AgencyLicense__c license : [SELECT Id, vlocity_ins__EffectiveDate__c, Name, vlocity_ins__LicenseType__c, 
                                                       vlocity_ins__AgencyBrokerageId__c, vlocity_ins__Jurisdiction__c, Applicable_License_Type__c, 
                                                       vlocity_ins__ExpirationDate__c, Novasys_Primary_Id__c, vlocity_ins__Status__c
                                                       FROM vlocity_ins__AgencyLicense__c 
                                                       WHERE vlocity_ins__AgencyBrokerageId__c  IN: accIds]) {
                System.debug(license);
                if (!agencyLicensesByAccount.containsKey(license.vlocity_ins__AgencyBrokerageId__c)) {
                    agencyLicensesByAccount.put(license.vlocity_ins__AgencyBrokerageId__c, new List<vlocity_ins__AgencyLicense__c>());
                }
                agencyLicensesByAccount.get(license.vlocity_ins__AgencyBrokerageId__c).add(license);
            }
            //NEED TO UPDATE THE BELOW QUERY TO FILTER FOR 'WHERE vlocity_ins__AgencyBrokerageId__c IN: accIds' AFTER DEMO MEETING
            for (vlocity_ins__AgencyAppointment__c app : [SELECT Id, vlocity_ins__CarrierId__r.Novasys_DBA_Name__c, vlocity_ins__EffectiveDate__c,
                                                          vlocity_ins__Jurisdiction__c, vlocity_ins__AgencyBrokerageId__c, Contract_Number__c, 
                                                          vlocity_ins__Status__c, vlocity_ins__ExpirationDate__c, Novasys_Primary_Id__c, Applicable_Appointment_Type__c
                                                          FROM vlocity_ins__AgencyAppointment__c
                                                          WHERE vlocity_ins__AgencyBrokerageId__c IN: accIds]) {
                if (!agencyAppointmentsByAccount.containsKey(app.vlocity_ins__AgencyBrokerageId__c)) {
                    agencyAppointmentsByAccount.put(app.vlocity_ins__AgencyBrokerageId__c, new List<vlocity_ins__AgencyAppointment__c>());
                }
                agencyAppointmentsByAccount.get(app.vlocity_ins__AgencyBrokerageId__c).add(app);
            }
        }
        if (!conIds.isEmpty()) {
            isfired = true;
            System.debug(conIds);
            for(vlocity_ins__ProducerLicense__c license : [SELECT Id, vlocity_ins__EffectiveDate__c, Name, Novasys_Primary_Id__c,
                                                           vlocity_ins__LicenseType__c, vlocity_ins__ProducerId__c, vlocity_ins__Status__c,
                                                           vlocity_ins__Jurisdiction__c, vlocity_ins__ExpirationDate__c, Applicable_License_Type__c
                                                           FROM vlocity_ins__ProducerLicense__c 
                                                           WHERE vlocity_ins__ProducerId__c IN :conIds]){
               if(!licensesByContact.containsKey(license.vlocity_ins__ProducerId__c)){
                    licensesByContact.put(license.vlocity_ins__ProducerId__c, new List<vlocity_ins__ProducerLicense__c>());                                            
               }
               licensesByContact.get(license.vlocity_ins__ProducerId__c).add(license);                                           
            }
            
            for(vlocity_ins__ProducerAppointment__c appointment :  [SELECT Id, vlocity_ins__EffectiveDate__c, vlocity_ins__CarrierId__r.Novasys_DBA_Name__c, 
                                                                    vlocity_ins__Jurisdiction__c, vlocity_ins__ProducerId__c, vlocity_ins__CarrierId__c, Applicable_Appointment_Type__c,
                                                                    vlocity_ins__Status__c, vlocity_ins__ExpirationDate__c, Contract_Number__c, Novasys_Primary_Id__c
                                                                    FROM vlocity_ins__ProducerAppointment__c 
                                                                    WHERE vlocity_ins__ProducerId__c IN :conIds]){
               if(!appointmentsByContact.containsKey(appointment.vlocity_ins__ProducerId__c)){
                    appointmentsByContact.put(appointment.vlocity_ins__ProducerId__c, new List<vlocity_ins__ProducerAppointment__c>());                                                    
               }
               appointmentsByContact.get(appointment.vlocity_ins__ProducerId__c).add(appointment);                                                   
            }
            
            for(Contract contracts : [SELECT Id, vlocity_ins__ProducerId__c, Carrier_Lookup__r.Novasys_DBA_Name__c,
                                      Carrier_lookup__c, Carrier_lookup__r.Id, Carrier_lookup__r.Name, Novasys_Primary_Id__c,
                                      Contract_Status__c, vlocity_ins__ContractType__c, StartDate, State__c, 
                                      vlocity_ins__ProducerId__r.Id, vlocity_ins__TerminateDate__c
                                      FROM Contract 
                                      WHERE vlocity_ins__ProducerId__c IN :conIds]){
               if(!contractsByContact.containsKey(contracts.vlocity_ins__ProducerId__c)){
                       contractsByContact.put(contracts.vlocity_ins__ProducerId__c, new List<Contract>());                   
               }
               contractsByContact.get(contracts.vlocity_ins__ProducerId__c).add(contracts);                      
            }
        
            for(vlocity_ins__ProducerEducation__c education : [SELECT Id, vlocity_ins__CertificationNumber__c,vlocity_ins__ProducerId__c, 
                                                               Effective_Date__c, Termination_Date__c, Novasys_Primary_Id__c,
                                                               vlocity_ins__Status__c, Status__c, vlocity_ins__Jurisdiction__c 
                                                               FROM vlocity_ins__ProducerEducation__c 
                                                               WHERE vlocity_ins__ProducerId__c IN :conIds]){
               if(!educationsByContact.containsKey(education.vlocity_ins__ProducerId__c)){
                       educationsByContact.put(education.vlocity_ins__ProducerId__c, new List<vlocity_ins__ProducerEducation__c>());                   
               }
               educationsByContact.get(education.vlocity_ins__ProducerId__c).add(education);                      
            }
            System.debug(licensesByContact);
            System.debug(appointmentsByContact);
            System.debug(contractsByContact);
            System.debug(educationsByContact);
            System.debug(agencyLicensesByAccount);
            System.debug(agencyAppointmentsByAccount);
            
            if (!licensesByContact.isEmpty()) {
                PlatformEventsHandler.LicenseEvents(licensesByContact, contactNewMap);
            }
            
            if (!appointmentsByContact.isEmpty()) {
                PlatformEventsHandler.AppointmentEvents(appointmentsByContact, contactNewMap);
            }
            
            if (!contractsByContact.isEmpty()) {
                PlatformEventsHandler.ContractEvents(contractsByContact, contactNewMap, accountMap);
            }
            
            if (!educationsByContact.isEmpty()) {
                PlatformEventsHandler.ContinuingEducationEvents(educationsByContact, contactNewMap);
            }
            
            if (!agencyLicensesByAccount.isEmpty()) {
                PlatformEventsHandler.AccountLicenseEvents(agencyLicensesByAccount, accountMap);
            }
            
            if(!agencyAppointmentsByAccount.isEmpty()) {
                PlatformEventsHandler.AccountAppointmentEvents(agencyAppointmentsByAccount, accountMap);
            }
        }
    }
}