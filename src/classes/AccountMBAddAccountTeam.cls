/*
 * Class AccountMBAddAccountTeam
 * Class used to add a co-owner for a Medicare Broker Agency 
 *   Steps:  1. check if account has co-owner - this was set via AccountMBAssignOwner  
*            2. Owner assignment only executes on insert of agency or when address data is first
 *				populated via the integration. 
 *           3. Co-Owner can be modified after insert and this logic will trigger
 *				 to add the new co-owner to the account team
 *			 4. build a record for that co-owner as a member with edit capability 
 *				of the account team for the account associated with the contact
 *				with Edit capability
 *   NOTE: logic will NOT remove a user from an Account Team as that user may   
 *		still have access via agents.
 *
 * @version 1.0
 * @author  Dianna Guilinger - Slalom Consulting
 *
 */
public with sharing class AccountMBAddAccountTeam 
{
  public static void addMBAccountCoOwner (List<Account> newValues, Map<Id, Account> beforeMap, Map<Id,RecordType> mbAccountRTbyId, String triggerEvent) 
  {  
//                                               only execute once per commit scope
    if (!helper_AccountMBAddAccountTeam.happened)
	{
		Set<Id> affectedAccts = new Set<Id> ();
		Set<Id> assignedUsers = new Set<Id> ();
		List<AccountTeamMember> newATMs = new List<AccountTeamMember> ();
	  
	
//       look at each account
	    for (Account nv : newValues)
	    { 
//                        will only execute for Medicare Broker Agent
			if (mbAccountRTbyId.get(nv.RecordTypeId)!=null
	         && ( mbAccountRTbyId.get(nv.RecordTypeId).Name == 'Agency'
		       || mbAccountRTbyId.get(nv.RecordTypeId).Name == 'Independent') )
	      	{
// 		 					on insert and on update when currently owned by Integration user 
	     		if ( nv.Co_Owner__c != null 
				  &&  ( triggerEvent == 'AfterInsert' || 
						( triggerEvent == 'AfterUpdate' 
						&& nv.Co_Owner__c <> beforeMap.get(nv.Id).Co_Owner__c ) ) )
	     		{
					AccountTeamMember atm = new AccountTeamMember();
					atm.AccountId = nv.Id;
					affectedAccts.add(nv.Id);
					atm.UserId = nv.Co_Owner__c;
					assignedUsers.add(nv.Co_Owner__c);
					newATMs.add(atm);	
				}
			}
		}
		if (newATMs.size() > 0 )
		{
			insert newATMs;
			List<AccountShare> shares = [select Id, AccountAccessLevel, RowCause from AccountShare where AccountId IN :affectedAccts and UserOrGroupId in :assignedUsers and RowCause = 'Team'];
// 						set all team members access to read/write
			for (AccountShare share : shares)
			{
				share.AccountAccessLevel = 'Edit';
			}
			update shares; 
	    }
//                                     set flag to avoid recursion
    helper_AccountMBAddAccountTeam.happened = true;
 	} 
	
  } 
}