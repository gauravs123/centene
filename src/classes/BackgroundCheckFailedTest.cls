@isTest(seeAllData=false)
public class BackgroundCheckFailedTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        return listDocSign;
	}
    static testMethod void TestBackgroundCheckFailed()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        List<Id> contIds = new List<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(20);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        List<Contact> contList = TestDataFactory.createContactList(applicationList);
        for(Contact contObj : contList)
        {
            contIds.add(contObj.Id);
        }
        BackgroundCheckFailed.sendBackgroundCheckFailedEmail(contIds);
        BackgroundCheckFailed.SendEmailToPrincipal(contIds);
    }
}