/**************************************************************
* Author : PlumTreeGroup - RK
* Created Date: Feb 3rd, 2020
* Purpose: Regenerate Contract Controller for List View Button
**************************************************************/

public with sharing class GenerateContractController {
    
    private ApexPages.StandardSetController standardController;
    
    public static  List<vlocity_ins__Application__c> selectedApps;
    public static Map<String, String> docuSignSettingsMap = new Map<String, String>();
    // Carrier Signed Document Id    
    public static ID signedDocumentId;
    
    // Carrier Signed Document Version Id
    public static ID signedDocVersionId;
    
    public GenerateContractController(ApexPages.StandardSetController standardController) {
        this.standardController = standardController;
    }
    
    // Regenerate Contract Code 
    public PageReference regenerateContracts() {   
        
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        selectedApps = (List<vlocity_ins__Application__c>) standardController.getSelected();
        
        
        callSignApplications();
        
        // Redirect Page
        PageReference pageRef = new PageReference('/lightning/o/vlocity_ins__Application__c/list?filterName=Recent');
        pageRef.setRedirect(true);
        return pageRef;
        // return null;        
    }
    
    public static void setDocuSignSettings() {
        System.debug(' Inside setDocuSign SEttings ');
        for(DocuSignSetting__mdt docuSignSettings : [Select DeveloperName, DocuSignValue__c  From DocuSignSetting__mdt]) {
            docuSignSettingsMap.put(docuSignSettings.DeveloperName, docuSignSettings.DocuSignValue__c); 
            System.debug(' DocuSign Settings Value ' + docuSignSettings.DocuSignValue__c);
        }
        
    }
    
    public static void callSignApplications() {
        
        Map<Id,Id> mapDocumentContent = new Map<Id,Id>();
        Map<Id,Id> parentIdContentDocumentIdMap = new Map<Id,Id>();
        Set<Id> appIds = new Set<Id>();
        
        // get all Application Ids
        for(vlocity_ins__Application__c app : selectedApps) {
            appIds.add(app.Id);   
        }
        
        // Populate DocuSign Merge data and update the reference key in Application Object
        updateApplication(appIds);
        
        // Send Contract to Selected Brokers
        for (Id appId: appIds) {
            GenerateContractController.sendContractEmail(appId,mapDocumentContent,parentIdContentDocumentIdMap, signedDocVersionId);
        }
    }
    
    // create DocuSign Merge Data 
    // 
    public static void updateApplication(Set<Id> appIds) {
        
        // Application Data
        List<vlocity_ins__Application__c> applicationObjects = new  List<vlocity_ins__Application__c>();
        
        // Application DocuSign Merge Data
        List<Application_DocuSign_Merge__c> appDocuSignMergeObjects = new List<Application_DocuSign_Merge__c>();
        
        // Get State Carrier Value for the selected Objects
        List<vlocity_ins__Application__c> selectedAppObjects = new List<vlocity_ins__Application__c>();
        selectedAppObjects = [select id,  Broker__r.AccountId, Contract_Type__c, State_Carrier__c from vlocity_ins__Application__C where id in :appIds ];
        
        // Loop Thru eac
        for (vlocity_ins__Application__C selectedAppObj : selectedAppObjects) {
            vlocity_ins__Application__C appObj = new vlocity_ins__Application__C();
            appObj.id = selectedAppObj.Id;
            String carrierNames  = selectedAppObj.State_Carrier__c;
            carrierNames  = selectedAppObj.State_Carrier__c;
            List<String> stateCarrierNames = new List<String>();   
            stateCarrierNames = carrierNames.split(';');
            System.debug('State Carrier Name Size ' + stateCarrierNames.size() );
            
            Application_DocuSign_Merge__c appDocuSignMergeObject = new Application_DocuSign_Merge__c();
            
            // If its a Sub Producer Application, Look for the Principal to use 
            // it in template
            if (selectedAppObj.Contract_Type__c == 'Sub Producer') {
                List<Contact> principalOfSubProducer = findPrincial(selectedAppObj.Broker__r.AccountId);
                if(principalOfSubProducer.size() > 0 ) {
                    appDocuSignMergeObject.PrincipalofSubProducer__c = principalOfSubProducer[0].id;
                }
                
            }    
            // set Carrier Name Details
            if(stateCarrierNames.size() >= 1) {
                appDocuSignMergeObject.CarrierNameOne__c = stateCarrierNames[0];
            }
            if(stateCarrierNames.size() >= 2) {
                appDocuSignMergeObject.CarrierNameTwo__c = stateCarrierNames[1];
            }
            if(stateCarrierNames.size() >= 3) {
                appDocuSignMergeObject.CarrierNameThree__c = stateCarrierNames[2];
            }
            if(stateCarrierNames.size() >= 4) {
                appDocuSignMergeObject.CarrierNameFour__c = stateCarrierNames[3];
            }
            if(stateCarrierNames.size() >= 5) {
                appDocuSignMergeObject.CarrierNameFive__c = stateCarrierNames[4];
            }
            if(stateCarrierNames.size() >= 6) {
                appDocuSignMergeObject.CarrierNameSix__c = stateCarrierNames[5];
            }
            if(stateCarrierNames.size() >= 7) {
                appDocuSignMergeObject.CarrierNameSeven__c = stateCarrierNames[6];
            }
            if(stateCarrierNames.size() >= 8) {
                appDocuSignMergeObject.CarrierNameEight__c = stateCarrierNames[7];
            }
            if(stateCarrierNames.size() >= 9) {
                appDocuSignMergeObject.CarrierNameNine__c = stateCarrierNames[8];
            }
            if(stateCarrierNames.size() >= 10) {
                appDocuSignMergeObject.CarrierNameNine__c = stateCarrierNames[9];
            }
            if(stateCarrierNames.size() >= 11) {
                appDocuSignMergeObject.CarrierNameTen__c = stateCarrierNames[10];
            }
            
            insert appDocuSignMergeObject;
            appObj.AppDocuSignMerge__c  = appDocuSignMergeObject.id;
            
            // make sure Counter Signature is not true
            appObj.CounterSignature__c = false;
            
            // make sure Regenerate Contract is set to true
            appObj.Regenerate_Contract__c = true;
            
            System.debug(' Created App DocuSign Merge ' + appDocuSignMergeObject.id);
            applicationObjects.add(appObj);
            
        }
        
        if (applicationObjects.size() > 0 ) {
            system.debug(' Updating Application Records ');
            update applicationObjects;
        }
        System.debug('appsIds --> ' + appIds);
    }
    
    // set Application Objects
    public static void setApplicationId(List<vlocity_ins__Application__c> applicationObjList) {
        selectedApps = applicationObjList; 
    }
    
    
    @future(callout=true)
    public static void sendContractEmail(Id appId,Map<Id,Id> mapDocumentContent, Map<Id,Id> parentIdContentDocumentIdMap, Id signedDocVersionId ) {
        
        System.debug('Calling future method ');
        Id mySourceId = appId;
        
        //use the Recipient.fromSource method to create the Recipient
        // dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
        //    myContact.Name, // Recipient name
        //    myContact.Email, // Recipient email
        //    null, //Optional phone number
        //    'Signer 1', //Role Name. Specify the exact role name from template
        //    new dfsle.Entity(myContact.Id)); //source object for the Recipient
        
        List<vlocity_ins__Application__C> appObject = [select id, Broker__r.AccountId, Contract_Type__c, Broker__r.LastName,Broker__r.FirstName,  Broker__r.email from vlocity_ins__Application__C where id = :appId];
        String contactName = appObject[0].Broker__r.LastName + ' ' + appObject[0].Broker__r.FirstName;
        String contactEmail = appObject[0].Broker__r.Email;
        String contractType = appObject[0].Contract_Type__c;
        Id AccountId = appObject[0].Broker__r.AccountId;
        System.debug('AccountId:::'+AccountId);
        System.debug('RK Debug Contract Type ' + contractType );
        
        /**************************
		/ Set Template Details
		/**************************/
        setDocuSignSettings();

        dfsle.Envelope myEnvelope;
        dfsle.Recipient producerRecipientObj;
        dfsle.Recipient subProducerRecipientObj;    
        
        String templateId = '';
        String producerRecipient;
        
        boolean principal = false;
        boolean subProducer = false;
        
        if ((contractType != null) && (contractType == 'Principal') ) {     
            templateId = docuSignSettingsMap.get('Principal');
            producerRecipientObj = dfsle.Recipient.fromSource
                (contactName, 
                 contactEmail, 
                 null,
                 'Broker',
                 new dfsle.Entity(appId)); 
            system.debug(' Recipient Details ' + producerRecipientObj);
            principal = true;
        }
        
        if ((contractType != null) && (contractType == 'Sub Producer') ) {
            templateId = docuSignSettingsMap.get('Sub Producer');
            subProducerRecipientObj = dfsle.Recipient.fromSource
                (contactName, 
                 contactEmail, 
                 null,
                 'SubProducer',
                 new dfsle.Entity(appId)); 
            List<Contact>  principalContactOjects = findPrincial(AccountId);
            if (principalContactOjects.size() >0 ) {
                System.debug(' Principal Contact Objects of Sub  Producer ' + principalContactOjects);
                String principalContactName = principalContactOjects[0].FirstName + ' ' + principalContactOjects[0].LastName;
                String principalEmail = principalContactOjects[0].Email;
                
                system.debug(' RK Producer Details ' + principalContactName + ' ====> ' + principalEmail);
                
                
                producerRecipientObj = dfsle.Recipient.fromSource
                    (principalContactName, 
                     principalEmail, 
                     null,
                     'Producer',
                     new dfsle.Entity(appId)); 
            }
            subProducer = true;
        }

        // Sub Producer Agreement
        if(!Test.isRunningTest()){
            dfsle.UUID myTemplateId = dfsle.UUID.parse(templateId); 
            dfsle.Document templateDocument = dfsle.Document.fromTemplate(myTemplateId,'Ambetter Producer Agreement');
            List<dfsle.Document> allDocumentList = new List<dfsle.Document>();
            allDocumentList.add(templateDocument); 
            dfsle.CustomField myCustomField1 = new dfsle.CustomField('text', 'Contract_Type__C', 'Producer', null, true, true);
            if (subProducer == true && !Test.isRunningTest()) {
                myEnvelope = dfsle.EnvelopeService.getEmptyEnvelope(new dfsle.Entity(mySourceId)).withDocuments(allDocumentList).withRecipients(new List<dfsle.Recipient> { subProducerRecipientObj, producerRecipientObj }).withCustomFields(new List<dfsle.CustomField> { myCustomField1 } );
            }
            if (principal == true) {
                myEnvelope = dfsle.EnvelopeService.getEmptyEnvelope(new dfsle.Entity(mySourceId)).withDocuments(allDocumentList).withRecipients(new List<dfsle.Recipient> {  producerRecipientObj }).withCustomFields(new List<dfsle.CustomField> { myCustomField1 } );
            }
            if(!Test.isRunningTest())
            {
                myEnvelope = dfsle.EnvelopeService.sendEnvelope(myEnvelope, true);
            }
        }
    }
    
    public static void logMessage(String message) {
        System.debug(' RK Debug ' + message);
    }
    
    
    public static List<Contact> findPrincial(Id AccountId) {
        List<Contact> contactObjects = new List<Contact>();
        contactObjects = [select id, FirstName, LastName, AccountId, Email from Contact where AccountId = :AccountId  and
                          Contact_Type__c  in ('Principal')];
        return contactObjects;
    }
    
    public PageReference updateApplications() {       
        // Call StandardSetController 'save' method to update (optional, you can use your own DML)
        PageReference pageRef = new PageReference('/');
        pageRef.setRedirect(true);
        return pageRef;
        //return standardController.save();   
    }
}