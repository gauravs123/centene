/****************************************************************************************
 * Name    : AccountEventTest
 * Author  : Developer 
 * Date    : Feb 09, 2019
 * Purpose : AccountEventTest: Helper class ContactEvents

 * ---------------------------------------------------------------------------
 * MODIFICATION HISTORY:
 * DATE             AUTHOR                              DESCRIPTION
 * ---------------------------------------------------------------------------
 * 09/02/2019       Developer                             Created, added method to publish Contract to Queue
*/
@isTest
public class AccountEventTest {
    
    
    
    @isTest
	public static void testAccountEvent(){
		Account a = new Account(
            Name = 'TestAcc',
            Agent_Id__c = '12345',
            BillingStreet = '123 Test Street',
            BillingCity = 'Test City',
            BillingState = 'TX',
            BillingPostalCode = '12345',
            Broker_Tax_Id__c = '45542',
            Agent_Master_Id__c = '19782',
            TaxId__c = '987654321',
            Email__c = 'testEmail@test.com',
            vlocity_ins__NPNNumber__c = '123456',
            NPN__c = '123456',
            Phone = '5555555555',
            vlocity_ins__TaxID__c = '12345',
            agentsync__ID_FEIN__c= '1234141',
            Fax = '6666666666',
            is_Pilot_Broker__c = true
        );
        
        insert a;
        
        a.Name = 'Test Account';
        update a;
         
        
        a.BillingStreet = '555 Test Street';
        update a;
        
        
        
        a.BillingCity = 'City Test';
        update a;
        
        a.Broker_Tax_Id__c = '234323233';
        update a;
        


        
        a.BillingState = 'CA';
        update a;
        
        a.BillingPostalCode = '12311';
        update a;
        
        a.Email__c = '555TestEmail@test.com';
        update a;
        
        a.Fax = '555555555';
        update a;
        
        a.NPN__c = '1152123';
        update a;
        
        
        
        a.Phone = '1234567890';
        update a;
    }
}