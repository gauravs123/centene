@isTest
public class DashboardUtilityTest {
     @testSetup
    static void setupTestData(){
        
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US', 
                         ProfileId = pf.Id
                        ); 
        
        
        insert uu;
    }
	static testMethod void getDashboardDataTest() {
        Date today = Date.Today();
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier').getRecordTypeId();
        RecordType r = [SELECT Id FROM RecordType WHERE Id =:devRecordTypeId LIMIT 1];
        User u = [SELECT Id,Profile.Name FROM User WHERE Profile.Name = 'System Administrator' AND isActive=true LIMIT 1];
        Account[] accounts = accountEventTest(r, u);
        Account a = accounts[0];
        Account carrier = accounts[1];
        Contact testCon = new Contact();
        testCon.Account = a;
        testCon.AccountId = a.Id;
        testCon.MailingStreet = '123 Test Street';
        testCon.MailingCity = 'Test City';
        testCon.MailingState = 'TX';
        testCon.MailingPostalCode = '55555';
        testCon.Broker_Lead_Status__c = 'Appointed';
        testCon.Contract_Term__c = date.today();
        testCon.Broker_Status__c = 'Active';
        testCon.Contact_Type__c = 'Principal';
        testCon.Email = '123test@test.com';
        testCon.FirstName = 'testFirst';
        testCon.LastName = 'testLast';
        testCon.Phone = '5555555555';
        testCon.vlocity_ins__ProducerNumber__c = '12345123';
        testCon.Status__c = 'Active';
        insert testCon;
        vlocity_ins__Application__c testApp = new vlocity_ins__Application__c(
            Name = 'Test application',
            Broker__c = testCon.Id,
            SalesRep__c = u.Id,
            State_Carrier__c = 'TX - Celtic',
            vlocity_ins__Status__c = 'Submitted',
            vlocity_ins__Type__c = 'Contracting'
        );
        
        insert testApp;
        Contract testContract = new Contract(
            AccountId = a.Id,
            vlocity_ins__ProducerId__c = testCon.Id,
            //contactId = c.Id,
            vlocity_ins__ContractType__c = 'Ambetter Producer',
            vlocity_ins__ApplicationId__c = testApp.Id,
            Contract_Status__c = 'Active',
            Carrier__c = 'Celtic',
            State__c = 'TX',
            Carrier_lookup__c = carrier.Id,
            StartDate = today.addDays(0-180),
            Contract_End_Date__c = today.addDays(180),
            Novasys_Primary_Id__c = '12345'
        );
        insert testContract;
        user newUser = new user();
        System.runAs ( u ){
           Profile p = [select Id,name from Profile where name ='Agency Principal Admin' limit 1];
 				newUser.profileId = p.id;
                newUser.username = 'newUser@009testtest1245.com';
                newUser.email = 'pb@ff.com';
                newUser.emailencodingkey = 'UTF-8';
                newUser.localesidkey = 'en_US';
                newUser.languagelocalekey = 'en_US';
                newUser.timezonesidkey = 'America/Los_Angeles';
                newUser.alias='nuser';
                newUser.lastname='lastname';
                newUser.contactId = testCon.id;
                insert newUser;  
        createTestData(testCon,a,carrier,newUser);
        }
        System.RunAs(newUser){
          	DashboardUtility.getDashboardData();
            DashboardUtility.DocumentWrapper objDocWrapper = new DashboardUtility.DocumentWrapper(new ContentDocumentLink());
            List<DashboardUtility.DocumentWrapper> lstwraps = new List<DashboardUtility.DocumentWrapper>();
            lstwraps.add(objDocWrapper);
            DashboardUtility.ContractWrapper objcontrWrapper = new DashboardUtility.ContractWrapper(testContract,lstwraps);
        }
    }
    static Account[] accountEventTest(RecordType r, User u) {
        Account[] accounts = new Account[]{};
        
        
        Account testAcc = new Account();
        testAcc.Name = 'testAcc';
        testAcc.Agent_Id__c = '5555555';
        testAcc.BillingStreet = '123 Test Street';
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingState = 'TX';
        testAcc.BillingPostalCode = '55555';
        testAcc.Agent_Master_Id__c = '123456';
        testAcc.TaxId__c = '123456789';
        testAcc.Email__c = 'testEmail@test.com';
        testAcc.vlocity_ins__NPNNumber__c = '5555555';
        testAcc.Phone = '55555555';
        testAcc.vlocity_ins__TaxID__c = '12345';
        
        Account testCarrier = new Account(
            Name = 'TX - Celtic',
            RecordType = r,
            Type = '4 - Corporation',
            DBA_Name__c = 'Celtic',
            Owner = u,
            License_State__c = 'TX',
            License__c = '4725',
            TaxId__c = '123456789',
            vlocity_ins__TaxID__c = '06-0641618',
            Agent_Master_Id__c = '12345',
            Type_Code__c = '4',
            BillingState = 'TX',
            vlocity_ins__AgencyBrokerageNumber__c = '80799'
        );
        
        insert testCarrier;
        
        insert testAcc;
        //contactEventTest(testAcc, testCarrier, u);
        accounts.add(testAcc);
        accounts.add(testCarrier);
        return accounts;
    }
    static vlocity_ins__Application__c createTestData(Contact c, Account a, Account carrier, User u) {
        Date today = Date.Today();
        
        vlocity_ins__Application__c testApp = new vlocity_ins__Application__c(
            Name = 'Test application',
            Broker__c = c.Id,
            SalesRep__c = u.Id,
            State_Carrier__c = 'TX - Celtic',
            vlocity_ins__Status__c = 'Submitted',
            vlocity_ins__Type__c = 'Contracting'
        );
        
        insert testApp;
        vlocity_ins__ProducerLicense__c testLicense = new vlocity_ins__ProducerLicense__c(
            
            vlocity_ins__EffectiveDate__c = today.addDays(0-90),
            Name = 'testLicense',
            vlocity_ins__LicenseType__c = 'Resident Insurance Producer',
            vlocity_ins__ProducerId__c = c.Id,
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ExpirationDate__c = today.addDays(270),
            Novasys_Primary_Id__c = '12345'
        );
        
        insert testLicense;
        
        vlocity_ins__ProducerAppointment__c testAppointment = new vlocity_ins__ProducerAppointment__c(
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ProducerId__c = c.Id,
            vlocity_ins__CarrierId__c = a.Id,
            NIPR_Code__c = '102519',
            Contract_Number__c = '00062524',
            vlocity_ins__Status__c = 'Pending',
            Novasys_Primary_Id__c = '12345'
        );
        
        insert testAppointment;
        vlocity_ins__ProducerEducation__c testEducation = new vlocity_ins__ProducerEducation__c(
            Name = 'Test Ex Cert',
            vlocity_ins__ProducerId__c = c.Id,
            vlocity_ins__Jurisdiction__c = 'TX',
            Effective_Date__c = today.addDays(0-180),
            Termination_Date__c = today.addDays(180),
            Novasys_Primary_Id__c = '12345'
        );
        insert testEducation;
        return testApp;
    }
}