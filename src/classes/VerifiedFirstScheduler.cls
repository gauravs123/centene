/************************************************************/
/* Call Verified First Background check 
/* Modified Date | By  | Changes 
/* Feb 23, 2020  | Rangesh Kona | Initial Version 
/************************************************************/

global class VerifiedFirstScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
     // Call Verified First Job in Queueable         
         ID jobID = System.enqueueJob(new VerifiedFirstQueueable());        
    }
}