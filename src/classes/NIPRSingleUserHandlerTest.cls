@isTest
public class NIPRSingleUserHandlerTest {
static testmethod void executeFlow()
{
    Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String UniqueName1 = String.valueOf(randomInt);
        Account acc= new Account();
        acc.Name='test account';
        acc.TaxId__c=UniqueName1+'1mc';
        insert acc;
        contact con=new Contact();
        con.lastname='testing con';
        con.AccountId=acc.id;
        con.TaxId__c=UniqueName1+'8co';
        con.Is_Pilot_Broker__c=true;    
        con.NPN__c='232322332';
        insert con;
    list<string> conids=new list<string>();
    conids.add(con.id);
    NIPRSingleUserHandler.callIP(conids);
}
}