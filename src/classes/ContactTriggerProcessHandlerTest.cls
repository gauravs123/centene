@isTest(seeAllData=false)
public class ContactTriggerProcessHandlerTest 
{
	static testMethod void testContactTriggerProcess()
    {
        /*List<Contact> 		listCont = new List<Contact>();
        List<Contact> 		listContOld = new List<Contact>();
        Map<Id, Contact> contactIdMapOld = new Map<Id, Contact>();
        Account accObj = new Account();
        accObj.Name = 'PlumTree';
        accObj.TaxId__c = 'Test Tax';
        insert accObj;
        
        Contact cont1 = new Contact();
        cont1.LastName = 'Indra';
        cont1.AccountId = accObj.Id;
        cont1.Email = 'balam@megnity.com';
        cont1.Contact_Type__c= 'Principal';
        cont1.vlocity_ins__Status__c = 'Active';
        listCont.add(cont1);
        insert listCont;
        
        for(Contact cont : listCont)
        {
            contactIdMapOld.put(cont.Id, cont);
            cont.Contact_Type__c = 'Sub Producer';
            listContOld.add(cont);
        }
        update listContOld;
        ContactTriggerProcessHandler.checkContactValidation(contactIdMapOld, listContOld);*/
        map<id,Contact> oldContactMap = new Map<id, Contact>();
        List<Id> contIds = new List<Id>();
        List<Contact> updateContList = new List<Contact>();
        List<vlocity_ins__Application__c> listApp = TestDataFactory.createApplication(1);
        listApp[0].vlocity_ins__Status__c = 'Active';
        update listApp;
        List<Contact> ContactList = TestDataFactory.createContactList(listApp);
        Account accObj = new Account();
        accObj.Name = 'PlumTree New';
        accObj.TaxId__c = 'Test Tax';
        accObj.Is_Pilot_Broker__c = true;
        insert accObj;
        test.startTest();
        Contact cont1 = new Contact();
        cont1.LastName = 'Indra';
        cont1.AccountId = accObj.Id;
        cont1.Email = 'balam@megnity.com';
        cont1.Contact_Type__c= 'Principal';
        cont1.vlocity_ins__Status__c = 'Active';
        cont1.Is_Pilot_Broker__c = true;
        insert cont1;
        
        Contact cont2 = new Contact();
        cont2.LastName = 'Indra';
        cont2.AccountId = accObj.Id;
        cont2.Email = 'balam@megnity.com';
        cont2.Contact_Type__c= 'Principal';
        cont2.vlocity_ins__Status__c = 'Active';
        cont2.Is_Pilot_Broker__c = true;
        try{
        insert cont2;
        }catch(Exception Ex){
            System.assert(true,Ex.getMessage().contains('Principal Broker Record already exists'));
        }
        test.stopTest();

        //ContactEventsHandler contHandler  = new ContactEventsHandler();
        for(Contact contObj : ContactList)
        {
            contIds.add(contObj.Id);
            contObj.Contact_Type__c = 'Principal';
            contObj.Is_Pilot_Broker__c = true;
            updateContList.add(contObj);
        }
        update updateContList;
        
    }
}