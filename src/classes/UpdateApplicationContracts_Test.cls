@isTest
private class UpdateApplicationContracts_Test{
    
       static List<Id> brokerIds = new List<Id>();
    
    @testsetup
    static void createTestData() {
        
        // Create Account
        Account Account_Obj = New Account(Name = 'Test Account');
        Insert Account_Obj ; 
        
        // Create Contact
        Contact Contact_Obj = New Contact(LastName = 'TestContact');
        Insert Contact_Obj; 
        
        brokerIds.add(Contact_Obj.id);
        
        System.debug('Broker Ids ' + brokerIds);
        
        // Create Contract
        Contract contract_Obj = new Contract(
            AccountId = Account_Obj.id, 
            StartDate = Date.today(), 
            Status = 'Draft', 
            vlocity_ins__TerminateDate__c = Date.newInstance(2999, 12, 31), 
            Contract_Status__c = 'Pending'
        );
        Insert contract_Obj; 
        
       vlocity_ins__ProducerAppointment__c producerAppointment = new vlocity_ins__ProducerAppointment__c(
           Name = 'Test Appointment',
           Contract__c = contract_Obj.id,
           vlocity_ins__Status__c = 'Appointed',
           vlocity_ins__ProducerId__c = Contact_Obj.Id
           );
           
        insert producerAppointment;
        
    }
    static testMethod void test_updateContracts_UseCase1(){
        List<Contact> contacts = new List<Contact>();
        contacts =[select Id from Contact];
        List<Id> brokerIds = new List<Id>();
        brokerIds.add(contacts[0].id);
        
        
         List<vlocity_ins__ProducerAppointment__c> producerAppointments = new List<vlocity_ins__ProducerAppointment__c>();
        
        // get the list of Appointments for the broker Ids 
        producerAppointments = [ SELECT id, contract__r.Contract_Status__c , Contract__c  
                                FROM    vlocity_ins__ProducerAppointment__c 
                                 
                               ];
        
        System.debug(' Producer Appoints ' + producerAppointments.size());
        
        UpdateApplicationContracts.updateContracts(brokerIds);
        
    }
}