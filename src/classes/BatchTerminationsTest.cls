@isTest(seeAllData=false)
public class BatchTerminationsTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	} 
    static testMethod void TestBatchTerminations()
    {
        getAllMetadata();
         RecordType rtype = [SELECT Id from RecordType where Name = 'Ambetter Agent' LIMIT 1];
        RecordType rtype1 = [SELECT Id from RecordType where Name = 'Ambetter Agency' LIMIT 1];
       
        insert new Account(RecordTypeId =rtype1.Id,NPN__c='12323323',is_Pilot_Broker__c = true,Name = 'Last Account',NIPRLastRundate__c=Date.today().addDays(-16));
        Contact con = new Contact(RecordTypeId =rtype.Id, Tech_Partner_User_Created__c=true, NPN__c='12323323',is_Pilot_Broker__c = true,LastName = 'Last Contact',NIPRLastRundate__c=Date.today().addDays(-16));
       insert con;
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(20);
        applicationList[0].Broker__c = con.Id;
        applicationList[0].Check_Contract_Termination__c = true;
        update applicationList;
        BatchTerminations batchable = new BatchTerminations();
        Database.executeBatch(batchable);
		system.schedule('Schedukle1', '0 0 0/12 * * ? *', new BatchTerminations());    
    }
}