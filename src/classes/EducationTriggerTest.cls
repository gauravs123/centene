@isTest
public class EducationTriggerTest {
    static testMethod void myTestMethod() {        
         Account accObj = new Account();
        accObj.Name = 'PlumTree';
        accObj.TaxId__c = 'Test Tax';
		accObj.Novasys_DBA_Name__c = 'DBA Name';
        insert accObj;
         List<Contact> contactList = new List<Contact>();
         Contact cont1 = new Contact();
        cont1.LastName = 'Indra';
        cont1.AccountId = accObj.Id;
        cont1.Email = 'balam@megnity.com';
        cont1.Contact_Type__c= 'Sub Producer';
        cont1.vlocity_ins__Status__c = 'Active';
        cont1.Contact_Type__c = 'Principal';
        contactList.add(cont1);
        Contact cont2 = new Contact();
        cont2.LastName = 'Sena Reddy';
        cont2.Contact_Type__c= 'Sub Producer';
        cont2.AccountId = accObj.Id;
        cont2.Email = 'balam@megnity.com';
        cont2.vlocity_ins__Status__c = 'Active';
        
        
        contactList.add(cont2);
        insert contactList;
        
        
        
        List<vlocity_ins__ProducerEducation__c> lstprdcer = new List<vlocity_ins__ProducerEducation__c>();
        vlocity_ins__ProducerEducation__c prdcer = new vlocity_ins__ProducerEducation__c(Name='Cert3',Termination_Date__c=system.today().adddays(1),vlocity_ins__CertificationNumber__c='Test1',vlocity_ins__Jurisdiction__c='CA',
                                                         Effective_Date__c=system.today().adddays(1),vlocity_ins__ProducerId__c=cont1.id);
          
        
        vlocity_ins__ProducerEducation__c prdcer1 = new vlocity_ins__ProducerEducation__c(Name='Cert3',Termination_Date__c=system.today().adddays(1),vlocity_ins__CertificationNumber__c='Test1',vlocity_ins__Jurisdiction__c='CA',
                                                         Effective_Date__c=system.today().adddays(1),vlocity_ins__ProducerId__c=cont2.id);
          
        
        lstprdcer.add(prdcer);
        lstprdcer.add(prdcer1);
        insert lstprdcer; 
        try{
        vlocity_ins__ProducerEducation__c prdcer2 = new vlocity_ins__ProducerEducation__c(Name='Cert3',Termination_Date__c=system.today().adddays(1),vlocity_ins__CertificationNumber__c='Test1',vlocity_ins__Jurisdiction__c='CA',
                                                         Effective_Date__c=system.today().adddays(1),vlocity_ins__ProducerId__c=cont1.id);
          
        
        insert prdcer2; 
            BC_AP13_ValidateLicenseError a = new BC_AP13_ValidateLicenseError();
            a.beforeInsertLogic(new list<vlocity_ins__ProducerEducation__c>{prdcer2});
        }
        Catch(Exception e){
            
        }
        
        
        
    }
     static testMethod void myTestMethod1() {        
         Account accObj = new Account();
        accObj.Name = 'PlumTree';
        accObj.TaxId__c = 'Test Tax';
		accObj.Novasys_DBA_Name__c = 'DBA Name';
        insert accObj;
         List<Contact> contactList = new List<Contact>();
         Contact cont1 = new Contact();
        cont1.LastName = 'Indra';
        cont1.AccountId = accObj.Id;
        cont1.Email = 'balam@megnity.com';
        cont1.Contact_Type__c= 'Sub Producer';
        cont1.vlocity_ins__Status__c = 'Active';
        cont1.Contact_Type__c = 'Principal';
        contactList.add(cont1);
        Contact cont2 = new Contact();
        cont2.LastName = 'Sena Reddy';
        cont2.Contact_Type__c= 'Sub Producer';
        cont2.AccountId = accObj.Id;
        cont2.Email = 'balam@megnity.com';
        cont2.vlocity_ins__Status__c = 'Active';
        
        
        contactList.add(cont2);
        insert contactList;
        
        
        
        List<vlocity_ins__ProducerEducation__c> lstprdcer = new List<vlocity_ins__ProducerEducation__c>();
        vlocity_ins__ProducerEducation__c prdcer = new vlocity_ins__ProducerEducation__c(Name='Cert3',Termination_Date__c=system.today().adddays(1),vlocity_ins__CertificationNumber__c='Test1',vlocity_ins__Jurisdiction__c='CA',
                                                         Effective_Date__c=system.today().adddays(1),vlocity_ins__ProducerId__c=cont1.id);
          
        
        vlocity_ins__ProducerEducation__c prdcer1 = new vlocity_ins__ProducerEducation__c(Name='Cert3',Termination_Date__c=system.today().adddays(1),vlocity_ins__CertificationNumber__c='Test1',vlocity_ins__Jurisdiction__c='US',
                                                         Effective_Date__c=system.today().adddays(1),vlocity_ins__ProducerId__c=cont1.id);
          
        
        lstprdcer.add(prdcer);
        lstprdcer.add(prdcer1);
        insert lstprdcer; 
        
        try{
        	prdcer1.vlocity_ins__Jurisdiction__c = 'CA';
            update prdcer1;
            BC_AP13_ValidateLicenseError a = new BC_AP13_ValidateLicenseError();
            a.beforeUpdateLogic(new list<vlocity_ins__ProducerEducation__c>{prdcer1});
        }
        Catch(Exception e){
            
        }
        
        
    }

}