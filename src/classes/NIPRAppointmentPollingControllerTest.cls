@isTest(seeAllData=false)
global class NIPRAppointmentPollingControllerTest {
    
    static testMethod void TestPollingController(){
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Carrier').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        agency.Is_Applicable_For_Agency__c = True;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Sub Producer';
        agent.LastBGCheck__c = System.Today();
        insert agent;
        
        Contact agent11 = new Contact();
        agent11.FirstName = 'John';
        agent11.LastName = 'Adams';
        agent11.email = 'email@gmail.com';
        agent11.TaxId__c = 'ABCDEFGHI';
        agent11.MailingCity = 'Tempe';
        agent11.MailingState = 'AZ';
        agent11.MailingStreet = '20 S Mill Ave' ;
        agent11.MailingPostalCode = '85251';
        agent11.Phone = '3334445555';
        agent11.RecordTypeId = contactAgentRT;
        agent11.AccountId = agency.Id;
        agent11.Contact_Type__c = 'Principal';    
        insert agent11;        
        
        Contract cont = new Contract();
        cont.AccountId = agency.Id;
        cont.Contract_Status__c = 'Active';
        insert cont;
        
        vlocity_ins__Application__c appli = new vlocity_ins__Application__c();
        appli.State_Carrier__c= 'AR - Celtic Insurance Company';
        appli.Broker__c = agent.Id;
        appli.vlocity_ins__Status__c = 'New';
        insert appli;
        
        vlocity_ins__AgencyAppointment__c agenApp = new vlocity_ins__AgencyAppointment__c();
        agenApp.vlocity_ins__AgencyBrokerageId__c = agency.Id;
        agenApp.vlocity_ins__Status__c='Pending';
        insert agenApp;
        
        vlocity_ins__ProducerAppointment__c propApp = new vlocity_ins__ProducerAppointment__c();
        propApp.vlocity_ins__ProducerId__c = agent11.Id;
        propApp.vlocity_ins__Status__c='Pending';
        insert propApp;
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(agenApp);
        AgencyAppointmentPollingController cntrl = new AgencyAppointmentPollingController(standardController);
        cntrl.callPollingIP();
        cntrl.backToAppointment();
        
        ApexPages.StandardController standardController1 = new ApexPages.StandardController(propApp);
        AppointmentPollingController cntrl1 = new AppointmentPollingController(standardController1);
        cntrl1.callPollingIP();
        cntrl1.backToAppointment();
            
    }
}