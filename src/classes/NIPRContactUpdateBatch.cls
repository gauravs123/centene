/****************************************************************************************
 * Name    : NIPRContactUpdateBatch

*/ 
global class NIPRContactUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
         Date executionDate = Date.today().addDays(-15);
        return Database.getQueryLocator(
            [SELECT Id FROM Contact Where RecordType.Name = 'Ambetter Agent' AND NPN__c != '' AND Tech_Partner_User_Created__c = True 
             and (NIPRLastRundate__c <: executionDate or NIPRLastRundate__c = null) and Is_Pilot_Broker__c = true]// AND (LastRunDate__c = LAST_N_DAYS:15 OR LastRunDate__c = null) LIMIT 2]
        );
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> contacts){
       //Id jobId = System.enqueueJob(new NIPRUpdateBatchQueueable(contacts)); 
       for (Contact c : contacts) {
        	System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => c.Id},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' =>'ProducerAdditionalInfoUpdate','queueableChainable'=> true}));
        }
    } 
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    global void execute(SchedulableContext SC) {
      database.executeBatch(new NIPRContactUpdateBatch(),1); 
   }
}