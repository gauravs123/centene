public with sharing class InProcessApplicationsCtrl {
    public List<vlocity_ins__OmniScriptInstance__c> savedOmniscripts {set; get;}
    
    public InProcessApplicationsCtrl(ApexPages.StandardController controller) {
        savedOmniscripts = new List<vlocity_ins__OmniScriptInstance__c>();     
        
        User user = [SELECT ContactId FROM User WHERE Id= :Userinfo.getUserId()];
        
        List<Id> applicationIds = new List<Id>(new Map<Id, vlocity_ins__Application__c>([SELECT Id from vlocity_ins__Application__c Where Broker__c = :user.contactId]).keySet());  
        
        system.debug('App Ids:'+applicationIds);
        savedOmniscripts = [SELECT Id, vlocity_ins__ResumeLink__c, vlocity_ins__RelativeResumeLink__c, Name, vlocity_ins__Status__c, vlocity_ins__LastSaved__c FROM  vlocity_ins__OmniScriptInstance__c WHERE vlocity_ins__ObjectLabel__c='Application' AND (vlocity_ins__Status__c='In Progress' OR  vlocity_ins__Status__c='Manual Reviewed') AND vlocity_ins__ObjectId__c IN :applicationIds ORDER BY vlocity_ins__LastSaved__c DESC];
    }

}