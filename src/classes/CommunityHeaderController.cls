public class CommunityHeaderController {
	
    @AuraEnabled
    public static Boolean isAppointedBroker() {
     	return [SELECT id,Contact.Tech_Partner_User_Created__c From User where Id=: UserInfo.getUserId()].Contact.Tech_Partner_User_Created__c;
    }
}