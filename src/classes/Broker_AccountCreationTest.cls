@isTest
public class Broker_AccountCreationTest 
{
    static testMethod void createUserAccount()
    {
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Principal';
        insert agent;
        
        vlocity_ins__Application__c testApp = new vlocity_ins__Application__c(
            Name = 'Test application',
            Broker__c = agent.Id,
            SalesRep__c = UserInfo.getUserId(),
            State_Carrier__c = 'TX - Celtic',
            vlocity_ins__Status__c = 'Submitted',
            vlocity_ins__Type__c = 'Contracting'
        );
        insert testApp;
        
        Map<String, String> inputmap = new Map<String,String>();
        inputmap.put('EmailId','email@gmail.com');
        inputmap.put('LastName','Test Last');
        inputmap.put('FirstName','Test First');
        inputmap.put('Password',String.valueOf(Datetime.now()));
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        inputs.put('ContextId',testApp.Id);
        inputs.put('Registration',inputmap);
        Broker_AccountCreation broker = new Broker_AccountCreation();
        broker.invokeMethod('createUserAccount',inputs,output,options);
        System.assert(true,output.get('error') != NULL);
        broker.invokeMethod('checkIfUserIsAlreadyRegistered',inputs,output,options);
        test.stopTest();
    }
    static testMethod void loginUserAccount()
    {
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Principal';
        insert agent;
        
        Map<String, String> inputmap = new Map<String,String>();
        Map<String, Object> inputmapIn = new Map<String,Object>();
        inputmapIn.put('LoginUsername','userNm');
        inputmapIn.put('LoginPassword','userPwd@');
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        inputs.put('ContextId',agent.Id);
        inputs.put('login',inputmapIn);
        Broker_AccountCreation broker = new Broker_AccountCreation();
        broker.invokeMethod('loginUserAccount',inputs,output,options);
        System.assert(true,output.get('error') != NULL);//asserts an error due to Null Pointer exception
        inputs.put('Login',inputmapIn);
        broker.invokeMethod('loginUserAccount',inputs,output,options);
        test.stopTest();
    }   
    static testMethod void prepareContractedStates()
    {
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Carrier').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'AL-Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        agency.Is_Applicable_For_Agency__c = True;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Principal';
        insert agent;
        
        Map<String, String> inputmap = new Map<String,String>();
        Map<String, Object> inputmapIn = new Map<String,Object>();
        inputmapIn.put('LoginUsername','userNm');
        inputmapIn.put('LoginPassword','userpwd');
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        options.put('ApplicationCarrier','AL-AK,AR-CA,DE-FL');
        options.put('ContactId ',agent.Id);
        Broker_AccountCreation broker = new Broker_AccountCreation();
        broker.invokeMethod('prepareContractedStates',inputs,output,options);
        test.stopTest();
    }        
    static testMethod void verifyExistingUser()
    {
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Carrier').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        agency.Is_Applicable_For_Agency__c = True;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Sub Producer';
        agent.LastBGCheck__c = System.Today();
        insert agent;
        
        Contact agent11 = new Contact();
        agent11.FirstName = 'John';
        agent11.LastName = 'Adams';
        agent11.email = 'email@gmail.com';
        agent11.TaxId__c = 'ABCDEFGHI';
        agent11.MailingCity = 'Tempe';
        agent11.MailingState = 'AZ';
        agent11.MailingStreet = '20 S Mill Ave' ;
        agent11.MailingPostalCode = '85251';
        agent11.Phone = '3334445555';
        agent11.RecordTypeId = contactAgentRT;
        agent11.AccountId = agency.Id;
        agent11.Contact_Type__c = 'Principal';    
        insert agent11;        
        
        Contract cont = new Contract();
        cont.AccountId = agency.Id;
        cont.Contract_Status__c = 'Active';
        insert cont;
        
        vlocity_ins__Application__c appli = new vlocity_ins__Application__c();
        appli.State_Carrier__c= 'AR - Celtic Insurance Company';
        appli.Broker__c = agent.Id;
        appli.vlocity_ins__Status__c = 'New';
        insert appli;
        
        vlocity_ins__AgencyAppointment__c agenApp = new vlocity_ins__AgencyAppointment__c();
        agenApp.vlocity_ins__AgencyBrokerageId__c = agency.Id;
        agenApp.vlocity_ins__Status__c='Active';
        insert agenApp;
        
        vlocity_ins__AgencyLicense__c ageLic = new vlocity_ins__AgencyLicense__c();
        ageLic.vlocity_ins__AgencyBrokerageId__c=agency.Id;
        ageLic.vlocity_ins__Status__c= 'Active';
        insert ageLic;
        
        Map<String, String> inputmap = new Map<String,String>();
        Map<String, Object> inputmapIn = new Map<String,Object>();
        inputmapIn.put('LoginUsername','userNm');
        inputmapIn.put('LoginPassword','userpwd');
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        inputs.put('ContextId',appli.Id);
        inputs.put('applicationId',appli.Id);
        inputs.put('status','appli.Id');

        inputs.put('userProfile','Broker Commissions Profile');
        Broker_AccountCreation broker = new Broker_AccountCreation();
        broker.invokeMethod('verifyExistingUser',inputs,output,options);
        broker.invokeMethod('updateApplicationStatus',inputs,output,options);
        test.stopTest();
    } 
    
    static testMethod void updateGroupInformation(){
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Carrier').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;     
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        agency.Is_Applicable_For_Agency__c = True;
        insert agency;
        
         Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Sub Producer';
        agent.LastBGCheck__c = System.Today();
        insert agent;
        System.assert(true,agency.TaxId__c == 'AGENCY');
        
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        inputs.put('AccountId',agency.Id);
        map<String,Object> agent1 =  new map<String,Object>();
        agent1.put( agent.Id,agent);
        inputs.put('Contact',agent1 );
        options.put('TIN','TEST');
        Broker_AccountCreation broker = new Broker_AccountCreation();
        broker.invokeMethod('updateGroupInformation',inputs,output,options);
        System.assert(true,agency.TaxId__c == 'TEST');
        test.stopTest();
    }
    
}