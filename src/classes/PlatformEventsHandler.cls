/****************************************************************************************
 * Name    : PlatformEventsHandler
 * Author  : Cory Bradley 
 * Date    : Nov 18, 2019
 * Purpose : PlatformEventsHandler : Handle the creation and sequencing of platform events

 * ---------------------------------------------------------------------------
 * MODIFICATION HISTORY:
 * DATE             AUTHOR                              DESCRIPTION
 * ---------------------------------------------------------------------------
 * 11/18/2019       Cory Bradley                        Created
 * 12/4/2019        Cory Bradley                        Bug fixes related to account query.
*/


global class PlatformEventsHandler {

    public static void AccountEvent(Account[] accounts) {
        System.debug('PlatformEventsHandler: AccountEvent');
        for (Account acc : accounts) {
            String postal;
            if (acc.BillingPostalCode.length() > 5) {
                postal = acc.BillingPostalCode.split('-')[0];
            } else {
                postal = acc.BillingPostalCode;
            }
            System.debug(acc);
            Account_Event__e accountEvent = new Account_Event__e();
            if (String.isBlank(acc.Agent_Master_Id__c)) {
                accountEvent.Agency_Name__c = acc.Name;
                accountEvent.Agent_Id__c = acc.Agent_Id__c;
                accountEvent.Billing_Address_Line_1__c = acc.BillingStreet;
                accountEvent.Billing_City__c = acc.BillingCity;
                accountEvent.Billing_State__c = acc.BillingState;
                accountEvent.Billing_Zip__c = postal;
                accountEvent.Broker_Type__c = acc.Broker_Type__c;
                accountEvent.Contract_Status__c = acc.Broker_Status__c;
                accountEvent.Email__c = acc.Email__c;
                accountEvent.Fax__c = acc.Fax;
                accountEvent.Phone__c = acc.Phone;
                accountEvent.SalesforceId__c = acc.Id;
                accountEvent.Tax_Id__c = acc.TaxId__c;
                accountEvent.Tech_Awaiting_Novasys_Receipt__c = acc.Tech_Awaiting_Novasys_Receipt__c;
                accountEvent.FEIN_Id__c = acc.agentsync__ID_FEIN__c;
                accountEvent.NPN__c = acc.NPN__c;
            } else {
                accountEvent.Agency_Name__c = acc.Name;
                accountEvent.Agent_Id__c = acc.Agent_Id__c;
                accountEvent.Agent_Master_Id__c = acc.Agent_Master_Id__c;
                accountEvent.Billing_Address_Line_1__c = acc.BillingStreet;
                accountEvent.Billing_City__c = acc.BillingCity;
                accountEvent.Billing_State__c = acc.BillingState;
                accountEvent.Billing_Zip__c = postal;
                accountEvent.Broker_Type__c = acc.Broker_Type__c;
                accountEvent.Contract_Status__c = acc.Broker_Status__c;
                accountEvent.Email__c = acc.Email__c;
                accountEvent.Fax__c = acc.Fax;
                accountEvent.Phone__c = acc.Phone;
                accountEvent.SalesforceId__c = acc.Id;
                accountEvent.Tax_Id__c = acc.TaxId__c;
                accountEvent.Tech_Awaiting_Novasys_Receipt__c = acc.Tech_Awaiting_Novasys_Receipt__c;
                accountEvent.FEIN_Id__c = acc.agentsync__ID_FEIN__c;
                accountEvent.NPN__c = acc.NPN__c;
            }
            
            Database.SaveResult saveResult = EventBus.publish(accountEvent);
            System.debug('Trigger Publish Account: ' + saveResult);
            
        }
        
        
    }
    
    public static void LicenseEvents(Map<Id, List<vlocity_ins__ProducerLicense__c>> licensesByContact, Map<Id, Contact> contact) {
        
        System.debug('PlatformEventHandlers: LicenseEvent');
        List<License_Event__e> licenseEvents  = new List<License_Event__e>();
        for(Id contactId :licensesByContact.keySet()){
            vlocity_ins__ProducerLicense__c[] licenses = licensesByContact.get(contactId);
            for(vlocity_ins__ProducerLicense__c license : licenses){
                if(license.vlocity_ins__Status__c == 'Active' &&
                  license.Applicable_License_Type__c == True) {
                    License_Event__e licenseEvent = new License_Event__e(
                            Agent_Master_Id__c = contact.get(contactId).Agent_Master_Id__c,
                            Effective_Date__c= license.vlocity_ins__EffectiveDate__c,
                            License_Number__c = license.Name,
                            License_Type__c = license.vlocity_ins__LicenseType__c,
                            Producer__c = license.vlocity_ins__ProducerId__c,
                            SalesforceId__c = license.Id,
                            State__c = license.vlocity_ins__Jurisdiction__c,
                            Termination_Date__c = license.vlocity_ins__ExpirationDate__c,
                            Tech_Awaiting_Novasys_Receipt__c = True,
                            Novasys_Primary_Id__c = license.Novasys_Primary_Id__c
                        );
                    licenseEvents.add(licenseEvent);
                }
            }//End Inner For
        }//End Outer For
        
        Database.SaveResult[] saveResult = EventBus.publish(licenseEvents);
    }
    
    public static void AccountLicenseEvents(Map<Id, List<vlocity_ins__AgencyLicense__c>> licensesByAccount, Map<Id, Account> account) {
        List<License_Event__e> licenseEvents = new List<License_Event__e>();
        for (Id accountId : licensesByAccount.keySet()) {
            vlocity_ins__AgencyLicense__c[] licenses = licensesByAccount.get(accountId);
            for (vlocity_ins__AgencyLicense__c license : licenses) {
                if(license.vlocity_ins__Status__c == 'Active' &&
                  license.Applicable_License_Type__c == True) {
                    License_Event__e licenseEvent = new License_Event__e(
                        Agent_Master_Id__c = account.get(accountId).Agent_Master_Id__c,
                        Effective_Date__c = license.vlocity_ins__EffectiveDate__c,
                        License_Number__c = license.Name,
                        License_Type__c = license.vlocity_ins__LicenseType__c,
                        Producer__c = license.vlocity_ins__AgencyBrokerageId__c,
                        SalesforceId__c = license.Id,
                        State__c = license.vlocity_ins__Jurisdiction__c,
                        Termination_Date__c = license.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = True,
                        Novasys_Primary_Id__c = license.Novasys_Primary_Id__c
                    );
                    licenseEvents.add(licenseEvent);
                }
            }
        }
        System.debug(licenseEvents);
        Database.SaveResult[] saveResult = EventBus.publish(licenseEvents);
        System.debug(saveResult);
    }
    
    public static void AppointmentEvents(Map<Id, List<vlocity_ins__ProducerAppointment__c>> appointmentsByContact, Map<Id, Contact> contact) {
        List<Appointment_Event__e> appointmentEvents = new List<Appointment_Event__e>();
        for(Id contactId :appointmentsByContact.keySet()){
            vlocity_ins__ProducerAppointment__c[] appointments = appointmentsByContact.get(contactId);
            for (vlocity_ins__ProducerAppointment__c appointment : appointments) {
                if (appointment.vlocity_ins__EffectiveDate__c != null && 
                appointment.vlocity_ins__ExpirationDate__c != null && 
                appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
                appointment.Applicable_Appointment_Type__c == true) {    
                
                    Appointment_Event__e appointmentEvent = new Appointment_Event__e(
                        Agent_Master_Id__c = contact.get(contactId).Agent_Master_Id__c,
                        Carrier_DBA_Name__c = appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                        Effective_Date__c = appointment.vlocity_ins__EffectiveDate__c,
                        Jurisdiction__c = appointment.vlocity_ins__Jurisdiction__c,
                        Producer__c = appointment.vlocity_ins__ProducerId__c,
                        SalesforceId__c = appointment.Id,
                        Contract_Number__c = appointment.Contract_Number__c,
                        Status__c = appointment.vlocity_ins__Status__c,
                        Termination_Date__c = appointment.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = True,
                        Novasys_Primary_Id__c = appointment.Novasys_Primary_Id__c
                    );
                    appointmentEvents.add(appointmentEvent);
                }
            }
        }
        System.debug(appointmentEvents);        
        Database.SaveResult[] saveResult = EventBus.publish(appointmentEvents);
       
    }
    
    public static void AccountAppointmentEvents(Map<Id, List<vlocity_ins__AgencyAppointment__c>>appointmentsByAccount, Map<Id, Account> account) {
        List<Appointment_Event__e> appointmentEvents = new List<Appointment_Event__e>();
        for(Id accountId : appointmentsByAccount.keySet()) {
            vlocity_ins__AgencyAppointment__c[] appointments = appointmentsByAccount.get(accountId);
            for(vlocity_ins__AgencyAppointment__c appointment : appointments) {
                if (appointment.vlocity_ins__EffectiveDate__c != null && 
                appointment.vlocity_ins__ExpirationDate__c != null && 
                appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
                appointment.Applicable_Appointment_Type__c == true) {
                    
                    Appointment_Event__e appointmentEvent = new Appointment_Event__e (
                        Agent_Master_Id__c = account.get(accountId).Agent_Master_Id__c,
                        Carrier_DBA_Name__c = appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                        Effective_Date__c = appointment.vlocity_ins__EffectiveDate__c,
                        Jurisdiction__c = appointment.vlocity_ins__Jurisdiction__c,
                        Producer__c = appointment.vlocity_ins__AgencyBrokerageId__c,
                        SalesforceId__c = appointment.Id,
                        Contract_Number__c = appointment.Contract_Number__c,
                        Status__c = appointment.vlocity_ins__Status__c,
                        Termination_Date__c = appointment.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = True,
                        Novasys_Primary_Id__c = appointment.Novasys_Primary_Id__c
                    );
                    appointmentEvents.add(appointmentEvent);
                }
            }
        }
        System.debug(appointmentEvents);
        Database.SaveResult[] saveResult = EventBus.publish(appointmentEvents);
        System.debug(saveResult);
    }
    
    public static void ContractEvents(Map<Id, List<Contract>> contractsByContact, Map<Id, Contact> contact, Map<Id, Account> accountByContact) {
        List<Contract_Event__e> contractEvents = new List<Contract_Event__e>();
        for(Id contactId :contractsByContact.keySet()){
            Contract[] contracts = contractsByContact.get(contactId);
            for (Contract contract : contracts) {
                if (!String.isBlank(contract.Carrier_lookup__r.Novasys_DBA_Name__c)) {
                    if (contract.vlocity_ins__ContractType__c == 'Ambetter Producer' && contract.vlocity_ins__ProducerId__c != null) {
                        Contract_Event__e contractEvent = new Contract_Event__e(
                            Agent_Master_Id__c = contact.get(contactId).Agent_Master_Id__c,
                            Broker_Id__c = contract.vlocity_ins__ProducerId__r.Id,
                            Carrier__c = contract.Carrier_lookup__r.Name,
                            Carrier_Id__c = contract.Carrier_lookup__r.Id,
                            Carrier_DBA_Name__c = contract.Carrier_lookup__r.Novasys_DBA_Name__c,
                            Contract_Status__c = contract.Contract_Status__c,
                            Contract_Type__c = contract.vlocity_ins__ContractType__c,
                            Effective_Date__c = contract.StartDate,
                            TerminationDate__c = contract.vlocity_ins__TerminateDate__c,
                            SalesforceId__c = contract.Id,
                            State__c = contract.State__c,
                            Tech_Awaiting_Novasys_Receipt__c = True,
                            Novasys_Primary_Id__c = contract.Novasys_Primary_Id__c
                        );
                        if (contract.Contract_Status__c == 'Active') {
                            contractEvent.Active__c = True;
                        } else {
                            contractEvent.Active__c = False;
                        }
                        contractEvents.add(contractEvent);
                    } else if (contract.vlocity_ins__ContractType__c == 'Ambetter Agency' && contract.vlocity_ins__ProducerId__c == null) {
                        Contract_Event__e contractEvent = new Contract_Event__e(
                            Agent_Master_Id__c = accountByContact.get(contract.AccountId).Agent_Master_Id__c,
                            //Broker_Id__c = contract.vlocity_ins__ProducerId__r.Id,
                            Carrier__c = contract.Carrier_lookup__r.Name,
                            Carrier_Id__c = contract.Carrier_lookup__r.Id,
                            Carrier_DBA_Name__c = contract.Carrier_lookup__r.Novasys_DBA_Name__c,
                            Contract_Status__c = contract.Contract_Status__c,
                            Contract_Type__c = contract.vlocity_ins__ContractType__c,
                            Effective_Date__c = contract.StartDate,
                            TerminationDate__c = contract.vlocity_ins__TerminateDate__c,
                            SalesforceId__c = contract.Id,
                            State__c = contract.State__c,
                            Tech_Awaiting_Novasys_Receipt__c = True,
                            Novasys_Primary_Id__c = contract.Novasys_Primary_Id__c
                        );
                        if (contract.Contract_Status__c == 'Active') {
                            contractEvent.Active__c = True;
                        } else {
                            contractEvent.Active__c = False;
                        }
                    }
                }
            }//End Inner For
        }//End Outer For
        
        Database.SaveResult[] saveResult = EventBus.publish(contractEvents);
    }
    
    public static void ContinuingEducationEvents(Map<Id, List<vlocity_ins__ProducerEducation__c>> educationsByContact, Map<Id, Contact> contact) {
        List<Continuous_Education_Event__e> educationEvents = new List<Continuous_Education_Event__e>();
        for(Id contactId :educationsByContact.keySet()){
            vlocity_ins__ProducerEducation__c[] educations = educationsByContact.get(contactId);
            for (vlocity_ins__ProducerEducation__c education : educations) {
                    Continuous_Education_Event__e educationEvent = new Continuous_Education_Event__e(
                        Agent_Master_Id__c = contact.get(contactId).Agent_Master_Id__c,
                        Certification_Number__c = education.vlocity_ins__CertificationNumber__c,
                        Effective_Date__c = education.Effective_Date__c,
                        FFM_Id__c = contact.get(contactId).FFM_Id__c,
                        SalesforceId__c = education.Id,
                        State__c = education.vlocity_ins__Jurisdiction__c,
                        Status__c = education.Status__c,
                        Termination_Date__c = education.Termination_Date__c,
                        Tech_Awaiting_Novasys_Receipt__c = True,
                        Novasys_Primary_Id__c = education.Novasys_Primary_Id__c
                    );
                educationEvents.add(educationEvent);
                }//End Inner For
        }//End Outer For 
        Database.SaveResult[] saveResult = EventBus.publish(educationEvents);
    }
    
}