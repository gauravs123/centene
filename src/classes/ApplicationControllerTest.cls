@isTest(seeAllData=false)
public class ApplicationControllerTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	}
    static testMethod void testApplicationCont()
    {
        getAllMetadata();
        Group testGroup = new Group(Name='Broker_Services_Group', type='Queue');
		insert testGroup;

        
        PageReference pageRef = Page.GenerateContract;
		Test.setCurrentPage(pageRef);
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(20);
        applicationList[0].vlocity_ins__Status__c = 'Manual Review';
        update applicationList;
        List<Contact> contLIst = TestDataFactory.createContactList(applicationList);
        applicationList[0].Broker__c = contLIst[0].Id;
        update applicationList;
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.standardController(applicationList[0]);
            ApplicationController controller = new ApplicationController(sc);
        controller.applicationStatusChange();
        TestDataFactory.insertOmniScript(1, applicationList[0].Id);
        Test.stopTest();
    }
}