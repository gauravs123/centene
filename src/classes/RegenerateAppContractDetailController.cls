/**************************************************************
* Author : PlumTreeGroup - RK
* Created Date: Feb 3rd, 2020
* Purpose: Regenerate Contract
**************************************************************/

public with sharing class RegenerateAppContractDetailController {

    private final vlocity_ins__Application__c application;
    public List<vlocity_ins__Application__c> applicationData{get;set;}
    
    public RegenerateAppContractDetailController(ApexPages.StandardController standardController){
        application = (vlocity_ins__Application__c)standardController.getRecord();
        applicationData = [SELECT Id,Name,vlocity_ins__Status__c,Broker__c,CreatedById,CreatedDate
                           FROM vlocity_ins__Application__c
                           WHERE Id = :application.Id];
    }
    
    public PageReference regenerateContract() { 
        GenerateContractController.setApplicationId(applicationData);
      	GenerateContractController.callSignApplications();
        // PageReference pageRef = new PageReference('lightning/r/vlocity_ins__Application__c/'+application.Id+'/view');
        PageReference pageRef = new PageReference('/'+application.Id);
        pageRef.setRedirect(true);
        return pageRef;
    } 
}