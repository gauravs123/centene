@isTest(seeAllData=false)
public class ApplicationUpdateCarrierDetailsTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        return listDocSign;
	}
    static testMethod void testApplicationCarrier()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(1);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        ApplicationUpdateCarrierDetails.ParseMultiSelect(applicationList);
    }
}