/************************************************************/
/* Call Verified First Background check in Queueable Mode
/* Called from Verified First Scheduler 
/* Modified Date | By  | Changes 
/* Feb 23, 2020  | Plum Tree Group - RK | Initial Version 
/************************************************************/
// How to call?  
// ID jobID = System.enqueueJob(new VerifiedFirstQueueable());


public class VerifiedFirstQueueable implements Queueable  {
    
    //
    // get valid Broker Records that needs background verification
    // 
    public void execute(QueueableContext context) {
        
        Map<String, String> brokerOnBoardingMap = new Map<String, String>();
        
        // read custom metadata 
        brokerOnBoardingMap = brokerOnBoadingMetaData();
        
        Integer pastBackgroundCheckDays = Integer.valueOf(brokerOnBoardingMap.get('LastBackgroundDays'));
        if (pastBackgroundCheckDays == null) {
            pastBackgroundCheckDays = 0;
        }
        Integer limitCount = Integer.valueOf(brokerOnBoardingMap.get('LimitCount'));
        
        String verifiedFirstSOQL = 'SELECT ID,               ' +
            'Broker__c ,            ' + 
            'Broker__r.LastBGCheck__c ,       ' +
            'Broker__r.BGCheck_Status__c,     ' + 
            'vlocity_ins__Status__c        ' + 
            'FROM   vlocity_ins__Application__C  ' +
            'WHERE IsPilotBroker__c = true and vlocity_ins__Status__c = \'Signed\'     ' + 
            'AND (( Broker__r.BGCheck_Status__c = null )     OR ' +
            '( broker__r.LastBGCheck__c != LAST_N_DAYS:' + pastBackgroundCheckDays + ') ' +   
            ')  ORDER BY broker__R.LastBGCheck__c  DESC LIMIT ' + limitCount;
        
        System.Debug(' Querying Data for : ' + verifiedFirstSOQL);
        
        List<vlocity_ins__Application__C> appObjects = Database.query(verifiedFirstSOQL);
        
        System.debug('RK:  Total No of Records Found ' + appObjects.size());
        
        for(vlocity_ins__Application__C appObject : appObjects) {
            // call Background check method
            initiateBackgroundCheck(appObject.Broker__c);
        }
        // end for-loop
    }
    // end execute-method
    
    
    public void initiateBackgroundCheck(String producerId) {
        
        System.debug('Background check Initiation for : ' + producerId);
        
        // updateBGCheckFieds(producerId);
        
        if (! Test.isRunningTest()) {   
            // DO NOT REMOVE THIS TEST ID
/*
            producerId = '0030x00000bGlMEAA0';   
            List<vfirst.SearchObjectWhenCreate.Request> requests = new List<vfirst.SearchObjectWhenCreate.Request>();
            requests.add(new vfirst.SearchObjectWhenCreate.Request());
            requests[0].objectId = producerId;
            requests[0].orderMethod = 'instant';
            vfirst.SearchObjectWhenCreate.searchObject(requests);
*/

        }
        
        
    }
    // end of initiateBackgroundCheck   
    
    public Map<String, String> brokerOnBoadingMetaData() {
        System.debug(' Inside setDocuSign SEttings ');
        Map<String, String> brokerOnBoardingMap = new Map<String, String>();
        for(BrokerOnboarding__mdt brokeronBoarding  : [select DeveloperName, Value__c from BrokerOnboarding__mdt]) {
            brokerOnBoardingMap.put(brokeronBoarding.DeveloperName, brokeronBoarding.Value__c); 
        }
        return brokerOnBoardingMap;
        
    }
    
    
    
}