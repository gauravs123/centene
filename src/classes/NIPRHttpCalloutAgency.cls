//
// Description: Get the appointment data from JSON 
// and insert into the agency Appointment Object
//
// Date | What's modified
// Jan 10, 2020 - Created new version
// 
global without sharing class NIPRHttpCalloutAgency implements vlocity_ins.VlocityOpenInterface2 {
    
    public static string niprUserName 	= '';
    public static String niprPassword 	= '';
    public static String niprURL		= '';
    public static final String ENTITY_INFO_URL = '/pdb-xml-reports/entityinfo_xml.cgi?id_entity=';
    public static String errorMessage = '';
    public static List<String> licenseStates; 
    
    global Boolean invokeMethod(String methodName, 
                                Map < String, Object > inputs, 
                                Map < String, Object > output, 
                                Map < String, Object > options) 
    {
        if(methodName == 'NIPRCalloutAgency'){
            
            System.debug('Inside NIPR Callout');
            
            String accountId 	= (String) inputs.get('AccountId');
            System.debug(' Got agency Id');
            
            String npnNumber	= (String) inputs.get('npnNumber');
              System.debug(' NPN Number');
            System.debug( ' Inputs ' + inputs);
            System.debug(' agency ID and NPN NUmber ' + accountId + ' ===> ' + npnNumber);
         //   output.put('ERROR_MESSAGE',errorMessage );
           //  output.put('STATES', licenseStates  );

            try {
            NIPRCalloutAgency(accountId, npnNumber, output);
            }
            catch(Exception ex) {
                output.put('ERROR', ex.getMessage());
                return false;
            }
            
 
           return true;
        }  


        return false;
    }
    
    public static Boolean NIPRCalloutAgency(String accountId, String npnNumber, Map<String, Object> output) { 
        
        // Get NIPR Credentials
        getNIPRCredentials();
        
        String nirpHttpURL = niprURL + ENTITY_INFO_URL + npnNumber;
        
      // nirpHttpURL = 'https://pdb-services-beta.nipr.com/pdb-xml-reports/entityinfo_xml.cgi?id_entity=17067266';
      //   niprUserName = 'ambetterbeta';
      //   niprPassword = 'Ambetter001';
        
     
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(nirpHttpURL);
        req.setMethod('GET');
        req.setTimeout(120000);
        Blob headerValue = Blob.valueOf(niprUserName + ':' + niprPassword);
        String authorizationHeader = 'Basic ' +
            EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        Http http = new Http();
        
        System.debug('Request ====> ' + req);        
        HTTPResponse res = http.send(req);
  
        // Convert XML Response to JSON
        // Added "Test.isRunningTest()" By Naresh K Shiwani Because Test Class was Breaking & 
        // no use of response so saved efforts for one line.
        Map<String,Object> xtj;
        if (!Test.isRunningTest()) {
        	xtj = vlocity_ins.XmlToJson.convertXMLToMap(res.getBody());
        }
        
        // Update NPN Number
        // NIPRDataExtractAgency.updateagencyNPN(accountId, npnNumber);

    if (!Test.isRunningTest()) {
        // Update agency License Info
        
        System.debug('Extracting ' + 92);
        NIPRDataExtractAgency.extractNIPRLicenses(xtj, accountId, output);
       // ID licenseJobId = System.enqueueJob(new NiprLicenseDataExtractQueable(xtj, accountId));
       // System.debug('jobID'+licenseJobId);
        System.debug('Extracting ' + 96);

        // Get License States 
        licenseStates=   NIPRDataExtract.getLicenseState();

        System.debug('Extracting Agency Addresses ');
        // Update agency Addresses
        NIPRDataExtractAgency.extractAgencyAddresses(xtj, accountId);
 		System.debug('Extracting Agency Addresses Ends ');
        
        //*************************************
        // Extract NIPR Appointment Info 
        //*************************************
        NIPRDataExtractAgency.extractNIPRAppointments(xtj, accountId);

        //*************************************
        // Extract NIPR Contact Regulatory Info 
        //*************************************
       //  NIPRDataExtract.extractContactRegulatoryActions(xtj, producerId);
        
        
        // Update agency Appointment Info
        // Update Contract Regulatory Action Info
      // ID jobID = System.enqueueJob(new NiprDataExtractQueable(xtj, accountId));
       //  System.debug('jobID'+jobID);
        
        //System.debug(' XML to JSON ' + xtj);
        //List<Object> objectData =  NIPRJsonExtraction.prepareObjectMapList(xtj, 'PDB');
        // system.debug(JSON.serialize(objectData));

    }
        
        return true;
        
    }
    
    public static void getNIPRCredentials() { 
        
        NIPR_Credentials__mdt[] niprCredentials = [select 
                                                   NIPR_Url__c, 
                                                   Password__c , 
                                                   Username__c, 
                                                   DeveloperName  
                                                   from 
                                                   NIPR_Credentials__mdt
                                                   where DeveloperName = 'Producer'
                                                  ];
        
        if(niprCredentials.size() > 0) {
            
            niprUserName = niprCredentials[0].Username__c;
            niprPassword = niprCredentials[0].Password__c;
            niprURL		 = niprCredentials[0].NIPR_Url__c;
            
        }
        
    }
    
    
    
    
}