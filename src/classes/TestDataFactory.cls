/* Used to create any required test data.   CHANGES to this will impact all test classes.
 * Methods:
 *   createSimpleSet - Agency, 1 Agent, 1 Stakeholder, 1 Administrative.  
 *                          1 Contract, 1 License at Agency. 
 *                          1 Certification, 1 Contract, 1 License at Agent.
 *   createProspect - Prospect Contact that then creates Prospect Account
 *   createBulkAgencies - 200 of Agency
 *   createBulkContacts  - 200 of Agent affiliated, Agent independent producer, Stakeholder, Administrative
 *   createBulkCerts     - for each Agent
 *   createBulkContracts
 *   createBulkLicenses  - for each Agency, Agent
 *   createTestUser      - Creates test user for the profile provided  
 *     
 *   createTestRules   - Creates test rules for owner assignment testing of agent/agency 
 *     
 *   createTestCounties - Creates test records for determination of county from postal code
 *
 *   createTestCampaigns - Creates test campaign for each of the 3 record types
 *
 *   createTestTermReasons - Creates record for verifying termination reason code translation to description
 *   createApplication      - creates list of applications
 *   insertContentDocumentList
 */
@isTest         
public class TestDataFactory {
    public static Integer setNum;
    @isTest public static void createSimpleSet() {
        if (setNum == null) {
            setNum = 1;
        } else {
            setNum++;
        }
        String taxId;
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String UniqueName = String.valueOf(randomInt);
//                                   create test Agency in CA 
        Id agencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Account agency=new Account(Name='ABC Test Agency', ABSId1__c ='AB123', ABSId2__c = 'AB234', ABSId3__c = 'AB345', ABSId4__c = 'AB456', ABSId5__c = 'AB567'
                                    , RecordTypeId = agencyRT
                                    , AlternatePhone__c ='(555) 897-9876', BillingCity = 'Laguna Beach', ContractStatus__c = 'Active', ContractType__c = 'FMO'
                                    , Phone = '(555) 123-4321', BillingPostalCode = '92651', BillingState = 'CA', BillingStreet = '1234 E Main'
                                    , VendorID1__c = 'VND12', VendorID2__c = 'VND23', VendorID3__c = 'VND34', VendorID4__c = 'VND45', VendorID5__c = 'VND56');
        agency.Name=agency.Name+String.valueOf(setNum);
        taxId = (UniqueName + 'CCC') +String.valueOf(setNum);
        agency.TaxId__c = taxId.substring(0,8);
        insert agency;
//                                   create Agent 
        Id agentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Contact agent=new Contact(FirstName='Adam', LastName='Ambrose', RecordTypeId = agentRT
                            , MailingStreet = '45 W Palm Dr', MailingCity = 'Los Angeles', MailingState = 'CA'
                            , MailingPostalCode = '90015'
                            ,   AccountId=agency.Id);
        agent.LastName=agent.LastName+string.ValueOf(setNum);
        taxId = (UniqueName + 'CCC') +String.valueOf(setNum);
        agent.TaxId__c = taxId.substring(0,8);
//                                   create Stakeholder 
        Id stakehRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Stakeholder').RecordTypeId;
        Contact stakeh =new Contact(FirstName='Susan', LastName='Smith', RecordTypeId = stakehRT
                            , MailingStreet = '45 W Palm Dr', MailingCity = 'Los Angeles', MailingState = 'CA'
                            , MailingPostalCode = '90015'
                            ,   AccountId=agency.Id);
        stakeh.LastName=stakeh.LastName+string.ValueOf(setNum);
//                                   create Administrative contact 
        Id adminRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Administrative').RecordTypeId;
        Contact admin=new Contact(FirstName='Martin', LastName='Buren', RecordTypeId = adminRT
                            , MailingStreet = '45 W Palm Dr', MailingCity = 'Los Angeles', MailingState = 'CA'
                            , MailingPostalCode = '90015'
                            ,  AccountId=agency.Id );
        admin.LastName=admin.LastName+string.ValueOf(setNum);

        helper_AccountMBAssignCounty.happened = false;
        helper_AccountMBAssignOwner.happened = false;
        helper_AccountMBAddAccountTeam.happened = false;
        helper_AccountMBVerifyOwnerChange.happened = false;
        helper_ContactMBAddAccountTeam.happened = false;
        helper_ContactMBAssignCounty.happened = false;
        helper_ContactMBSetPrimary.happened = false;
        helper_ContactMBSetParent.happened = false;
        helper_ContactMBVerifyOwnerChange.happened = false;
        
        insert new List<Sobject>{agent, stakeh, admin};  
//                                   create certification for Agent for current year
        Integer randomNumber = Integer.valueof((Math.random() * 10)) * Integer.valueof((Math.random() * 100));
        Certification__c cert = new Certification__c(name='Compliance Curr'+UniqueName, CourseName__c = 'Compliance'
                                    , Agent__c = agent.Id );
        cert.StartDate__c=Date.valueOf(system.today().Year() +'-01-02');
        cert.EndDate__c=Date.valueOf(system.today().Year() +'-12-31');
        cert.CompletionDate__c=Date.valueOf(system.today().addYears(-1).Year() +'-11-02');
        cert.CourseYear__c=string.valueOf(System.today().Year()+randomNumber);
        insert cert;
//                                   create license for Agency
        License__c l=new License__c(Name = 'LIC12345'+UniqueName, Agency__c = agency.Id, Jurisdiction__c = 'CA' );
        l.ExpirationDate__c=Date.valueOf('2015-12-31');
        insert l;
//                                   create contract for Agency
        MBContract__c c = new MBContract__c();
        c.Name='FMO 2015'+randomNumber;
        c.Agency__c = agency.Id;
        c.Agent__c = agent.Id;
        c.ContractStatus__c = 'TMUTUAL';
        c.ContractType__c = 'Selling Agent'; 
        c.PayType__c = 'Split';
        c.State__c = 'CA';
        c.Business_Unit__c = 'Centene Medicare';
        c.EffectiveEndDate__c=Date.valueOf(system.today().Year()+randomNumber +'-12-31');
        c.EffectiveStartDate__c=Date.valueOf(system.today().Year()+randomNumber +'-01-02');
        c.EligibletoSellDate__c=Date.valueOf(system.today().Year()+randomNumber +'-01-02');
        helper_MBContractSetTerminationReason.happened = false;
        insert c; 
//                                   create license for Agent
        License__c l2=new License__c(Name = 'LIC12145'+UniqueName, Agent__c = agent.Id, Jurisdiction__c = 'CA' );
        l2.ExpirationDate__c=Date.valueOf('2015-12-31');
        insert l2;
//                                   create contract for Agent
        Integer randomNumber1 = Integer.valueof((Math.random() * 100));
        MBContract__c c2 = new MBContract__c();
        c2.Name='Agency Authorized/Affiliated agent 2015';
        c2.Agent__c = agent.Id;
        c2.ContractStatus__c = 'Active';
        c2.ContractType__c = 'Agency Authorized/Affiliated agent';
        c2.PayType__c = 'Split';
        c2.State__c = 'AL';
        c2.EffectiveEndDate__c=Date.valueOf(system.today().Year()+randomNumber1 +'-12-31');
        c2.EffectiveStartDate__c=Date.valueOf(system.today().Year()+randomNumber1 +'-01-02');
        c2.EligibletoSellDate__c=Date.valueOf(system.today().Year()+randomNumber1 +'-01-02');
        c2.SalesTrainingComplete__c=Date.valueOf(system.today().Year()+randomNumber1 +'-01-02');
        helper_MBContractSetTerminationReason.happened = false;
        insert c2; 
    }
    @isTest public static void createProspect() {
        if (setNum == null) {
            setNum = 1;
        } else {
            setNum++;
        }
//                                   create test Prospect contact
        Id prospectRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Prospect').RecordTypeId;
        Contact prospect=new Contact();
        prospect.FirstName='Alice';
        prospect.RecordTypeId = prospectRT;
        prospect.MailingStreet = '125 W Plainview Dr';
        prospect.MailingCity = 'Los Angeles';
        prospect.MailingState = 'CA';
        prospect.MailingPostalCode = '90015';
        prospect.LastName='Jones '+string.ValueOf(setNum);

        helper_AccountMBAssignCounty.happened = false;
        helper_AccountMBAssignOwner.happened = false;
        helper_AccountMBAddAccountTeam.happened = false;
        helper_AccountMBVerifyOwnerChange.happened = false;
        helper_ContactMBAddAccountTeam.happened = false;
        helper_ContactMBAssignCounty.happened = false;
        helper_ContactMBSetPrimary.happened = false;
        helper_ContactMBSetParent.happened = false;
        helper_ContactMBVerifyOwnerChange.happened = false;
        
        insert prospect;  
    }
    public static void createIPAgent() {
        if (setNum == null) {
            setNum = 1;
        } else {
            setNum++;
        }
//                                   create test IP Agent
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;
            
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Smith'+string.ValueOf(setNum);
        agent.TaxId__c = 'INDEPTEST'+string.ValueOf(setNum);
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.ContractType__c = 'Independent Producer';
        
        helper_AccountMBAddAccountTeam.happened = false;
        helper_AccountMBVerifyOwnerChange.happened = false;
        helper_ContactMBAddAccountTeam.happened = false;
        helper_ContactMBAssignCounty.happened = false;
        helper_ContactMBSetPrimary.happened = false;
        helper_ContactMBSetParent.happened = false;
        helper_ContactMBVerifyOwnerChange.happened = false;
        insert agent;
    }
    @isTest public static void createBulkAgencies() {
//                                   create test Agency - 200 in CA 
        Id agencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        List<Account> agencies = new List<Account> ();
        for (Integer i = 0; i <200; i++)
        {
            Account agency=new Account(Name='ABC Test Agency', ABSId1__c ='AB123', ABSId2__c = 'AB234', ABSId3__c = 'AB345', ABSId4__c = 'AB456', ABSId5__c = 'AB567'
                                    , RecordTypeId = agencyRT, AlternatePhone__c ='(555) 897-9876', BillingCity = 'Laguna Beach', ContractStatus__c = 'Active', ContractType__c = 'FMO'
                                    , Phone = '(555) 123-4321', BillingPostalCode = '92651', BillingState = 'CA', BillingStreet = '1234 E Main'
                                    , VendorID1__c = 'VND12', VendorID2__c = 'VND23', VendorID3__c = 'VND34', VendorID4__c = 'VND45', VendorID5__c = 'VND56');
            
            agency.Name=agency.Name+' '+String.valueOf(i);
            agency.TaxId__c = 'AGENCY'+String.valueOf(i);
            agencies.add(agency);
        }
        helper_AccountMBAssignCounty.happened = false;
        helper_AccountMBAssignOwner.happened = false;
        helper_AccountMBAddAccountTeam.happened = false;
        helper_AccountMBVerifyOwnerChange.happened = false;
        insert agencies;
    }
    @isTest public static void createBulkContacts() {
//                                   create test Agency - 200 in CA 

        Id agencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id agentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;
        Id stakeRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Stakeholder').RecordTypeId;
        Id adminRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Administrative').RecordTypeId;

        List<Contact> contacts = new List<Contact> ();
//              get previously created agency records       
        List<Account> agencies = new List<Account> ([Select Id, TaxId__c from Account where RecordTypeId = :agencyRT]);
//                                   create Agent, Stakeholder, Admin for each Agency
        for (Account a:agencies)
        {
            Contact agent=new Contact();
            agent.FirstName='Adam';
            agent.LastName=agent.LastName+' '+a.TaxId__c;
            agent.RecordTypeId = agentRT;
            agent.TaxId__c=a.TaxId__c;
            agent.AccountId=a.Id;
            contacts.add(agent);
//                                 create Stakeholder 
            Contact stk=new Contact();
            stk.FirstName='DJ';
            stk.LastName='Stake '+a.TaxId__c;
            stk.RecordTypeId=stakeRT;
            stk.AccountId=a.Id;
            contacts.add(stk);
//                                 create Administrative 
            Contact adm=new Contact();
            adm.FirstName='Terry';
            adm.LastName='Turner '+a.TaxId__c;
            adm.RecordTypeId = adminRT;
            adm.AccountId=a.Id;
            contacts.add(adm);

        }
//                                   create Agent as independent Producer
        for (Integer i = 0; i <200; i++)
        {
            Contact agentIP=new Contact();
            agentIP.FirstName='Anne';
            agentIP.RecordTypeId = agentRT;
            agentIP.ContractType__c = 'Independent Producer';
            agentIP.LastName='Grant '+string.ValueOf(i);
            agentIP.TaxId__c = 'AGENTIP'+String.valueOf(i);
            contacts.add(agentIP);  
        }
        helper_AccountMBAssignCounty.happened = false;
        helper_AccountMBAssignOwner.happened = false;
        helper_AccountMBAddAccountTeam.happened = false;
        helper_AccountMBVerifyOwnerChange.happened = false;
        helper_ContactMBAddAccountTeam.happened = false;
        helper_ContactMBAssignCounty.happened = false;
        helper_ContactMBSetPrimary.happened = false;
        helper_ContactMBSetParent.happened = false;
        helper_ContactMBVerifyOwnerChange.happened = false;
        insert contacts;
    }
    @isTest public static void createBulkCerts() {
//         get just created agents - will create cert for each 
        Id agentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;
        List<Contact> addedAgents = new List<Contact> ([Select Id, TaxId__c, LastName from Contact where RecordTypeId = :agentRT]);
        List<Certification__c> certs = new List<Certification__c>();
        for (Contact aa : addedAgents)
        { 
            Certification__c cert = new Certification__c();
            cert.Name='Sales Training Curr';
            cert.CourseName__c='Sales Training';
            cert.StartDate__c=Date.valueOf(system.today().Year() +'-01-02');
            cert.EndDate__c=Date.valueOf(system.today().Year() +'-12-31');
            cert.CompletionDate__c=Date.valueOf(system.today().addYears(-1).Year() +'-11-02');
            cert.CourseYear__c=string.valueOf(System.today().Year());
            cert.Agent__c = aa.Id;
            certs.add(cert);
        }
        if (certs.size() > 0 )
        {
            insert certs;
        }
    }
    @isTest public static void createBulkLicenses() {
//                 simple set already created.  this will just add 200 licenses to agency, agent
        Id agencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id agentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;

        List<Account> agency = new List<Account> ([Select Id, RecordTypeId from Account where RecordTypeId = :agencyRT Limit 1]);
        List<Contact> agent = new List<Contact> ([Select Id, RecordTypeId from Contact where RecordTypeId = :agentRT Limit 1]);
        List<License__c> licenses = new List<License__c> ();
        
        if (agency.size() > 0)
        {
            for (Integer i = 0; i <200; i++)
            {
    //                                   create license for Agency
                License__c lic1=new License__c();
                lic1.Jurisdiction__c = 'CA';
                lic1.Name='LDEFR45 '+String.valueOf(i);
                lic1.ExpirationDate__c=Date.valueOf(system.today().Year() +'-12-31');
                lic1.Agency__c = agency[0].Id;
                licenses.add(lic1);
            }
        }
        if (agent.size() > 0)
        {
            for (Integer i = 0; i <200; i++)
            {
//                                   create license for Agent
                License__c lic2=new License__c();
                lic2.Jurisdiction__c = 'CA';
                lic2.Name='LABC54 '+String.valueOf(i);
                lic2.ExpirationDate__c=Date.valueOf(system.today().Year() +'-12-31');
                lic2.Agent__c = agent[0].Id;
                licenses.add(lic2);
            }
        }
        if (licenses.size() > 0 )
        {
            insert licenses;
        }
    }
    @isTest public static void createBulkContracts() {
//                 simple set already created.  this will just add 200 contracts to agency, agent
        Id agencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id agentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;

        List<Account> agency = new List<Account> ([Select Id, RecordTypeId from Account where RecordTypeId = :agencyRT Limit 1]);
        List<Contact> agent = new List<Contact> ([Select Id, RecordTypeId from Contact where RecordTypeId = :agentRT Limit 1]);
        List<MBContract__c> contracts = new List<MBContract__c> ();
        
        if (agency.size() > 0)
        {
            Integer yearKey = 1899;
            for (Integer i = 0; i <200; i++)
            {
        //                                   create contract for Agency
                MBContract__c contr1 = new MBContract__c();
                contr1.Name='FMO '+String.valueOf(i);
                contr1.ContractStatus__c = 'Active';
                contr1.ContractType__c = 'FMO';
                contr1.PayType__c = 'Split';
                contr1.State__c = 'CA';
                contr1.EffectiveEndDate__c=Date.valueOf('2015-12-31');
                yearKey++;
                contr1.EffectiveStartDate__c=Date.valueOf(yearKey+'-01-03');
                contr1.EligibletoSellDate__c=Date.valueOf('2014-12-10');
                contr1.Agency__c = agency[0].Id;
                contracts.add(contr1);
            }
        }
        if (agent.size() > 0)
        {
            Integer yearKey = 1899;
            for (Integer i = 0; i <200; i++)
            {
//                                   create contract for Agent
                MBContract__c contr2 = new MBContract__c();
                contr2.Name='Indep '+String.valueOf(i);
                contr2.ContractStatus__c = 'Active';
                contr2.ContractType__c = 'Indep';
                contr2.PayType__c = 'Split';
                contr2.State__c = 'AZ';
                yearKey++;
                contr2.EffectiveStartDate__c=Date.valueOf(yearKey+'-01-02');
                contr2.EffectiveEndDate__c=Date.valueOf('2015-12-31');
                contr2.EligibletoSellDate__c=Date.valueOf('2014-12-10');
                contr2.Agent__c = agent[0].Id;
                contracts.add(contr2);
            }
        }
        if (contracts.size() > 0)
        {
            insert contracts;
        }
    }
    public static User createTestUser(string testingProfileId, string testingRoleId, string testUser) {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = 'UnitTest',
                                lastName = testUser,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                UserPermissionsMarketingUser = True,
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = testingProfileId,
                                UserRoleId = testingRoleId);
        insert tuser;
        return tuser;
        
     }
    
    @isTest public static void createTestRules () {
//    create test 'owners'
        List<Profile> salesProfile = new List<Profile> ([Select Id, Name from Profile where Name = 'MB Sales' Limit 1]);
        List<Profile> systemProfile = new List<Profile> ([Select Id, Name from Profile where Name = 'System Administrator' limit 1]);
        List<UserRole> salesRole = new List<UserRole> ([Select Id, Name from UserRole where Name like 'MB Sales Acct Exec%' Limit 1]);
        
        User owner1 = testDataFactory.createTestUser(salesProfile[0].Id, salesRole[0].Id, 'AE1' );
        User owner2 = testDataFactory.createTestUser(salesProfile[0].Id, salesRole[0].Id, 'AE2' );
        User owner3 = testDataFactory.createTestUser(salesProfile[0].Id, salesRole[0].Id, 'AE3' );
        User admin = testDataFactory.createTestUser(systemProfile[0].Id, salesRole[0].Id, 'Adm' );
        
        List<User> testUsers = new List<User> ([Select Id, LastName from User where FirstName = 'UnitTest']);
        Map<String, User> testUserMap = new Map<String, User> ();
        for (User u:testUsers)
        {
            testUserMap.put(u.LastName, u);
        }

        System.runAs ( admin ) 
        {

//    create rule for Assignment by Agency for CA (do not change data without changing related tests AccountMBAssignOwnerTest, ContactMBAssignOwnerTest
            List<Assignment__c> rules = new List<Assignment__c>();
            Assignment__c ruleAgency = new Assignment__c ();
            ruleAgency.Rule__c = 'Agency';
            ruleAgency.State__c = 'CA';
            ruleAgency.AgencyName__c = 'TestingAgency';
            ruleAgency.AgencyTaxId__c = 'Test555888';
            ruleAgency.OwnerId = testUserMap.get('AE1').Id;
            ruleAgency.SecondOwner__c =  testUserMap.get('AE2').Id;
            rules.add(ruleAgency);
//    create rule for Assignment by Postal Code for CA
            Assignment__c rulePostal = new Assignment__c ();
            rulePostal.Rule__c = 'Postal Code';
            rulePostal.State__c = 'CA';
            rulePostal.PostalCode__c = '777777';
            rulePostal.OwnerId =  testUserMap.get('AE2').Id;
            rulePostal.SecondOwner__c =  testUserMap.get('AE1').Id;
            rules.add(rulePostal);
//    create rule for Assignment by County for AZ
            Assignment__c ruleCounty = new Assignment__c ();
            ruleCounty.Rule__c = 'County';
            ruleCounty.State__c = 'AZ';
            ruleCounty.County__c = 'Maricopa';
            ruleCounty.OwnerId =  testUserMap.get('AE3').Id;
            ruleCounty.SecondOwner__c =  testUserMap.get('AE1').Id;
            rules.add(ruleCounty);
//    create rule for Assignment Default for AZ
            Assignment__c ruleDefault = new Assignment__c ();
            ruleDefault.Rule__c = 'Default';
            ruleDefault.State__c = 'AZ';
            ruleDefault.OwnerId =  testUserMap.get('AE3').Id;
            ruleDefault.SecondOwner__c =  testUserMap.get('AE2').Id;
            rules.add(ruleDefault);
            insert rules;
        }
    }
    @isTest public static void createTestCounties () {
        List<Assignment__c> zipCounties = new List<Assignment__c> ();
        Assignment__c zipCounty1 = new Assignment__c ();
        zipCounty1.State__c = 'AZ';
        zipCounty1.County__c = 'Maricopa';
        zipCounty1.PostalCode__c = '85226';
        zipCounties.add(zipCounty1);
        Assignment__c zipCounty2 = new Assignment__c ();
        zipCounty2.State__c = 'CA';
        zipCounty2.County__c = 'Orange';
        zipCounty2.PostalCode__c = '92651';
        zipCounties.add(zipCounty2);
        Assignment__c zipCounty3 = new Assignment__c ();
        zipCounty3.State__c = 'CA';
        zipCounty3.County__c = 'Los Angeles';
        zipCounty3.PostalCode__c = '90015';
        zipCounties.add(zipCounty3);
        insert zipCounties;

    }
    @isTest public static void createTestCampaigns () {
        Id defaultCmpnRT = Schema.SObjectType.Campaign.RecordTypeInfosByName.get('Default').RecordTypeId;
        Id mbEnterCmpnRT = Schema.SObjectType.Campaign.RecordTypeInfosByName.get('Entertainment Event').RecordTypeId;
        Id mbTrainCmpnRT = Schema.SObjectType.Campaign.RecordTypeInfosByName.get('Training Event').RecordTypeId;
 
        List<Campaign> testCampaigns = new List<Campaign> ();
        Campaign enterCampaign = new Campaign ();
        enterCampaign.Name = 'MB Entertainment';
        enterCampaign.Status = 'Open';
        enterCampaign.EventDescription__c = 'testing entertainment event';
        enterCampaign.EventStartDateTime__c = System.now();
        enterCampaign.EventEndDateTime__c = System.now().addDays(3);
        enterCampaign.Location__c = 'Sacramento, CA';
        enterCampaign.Scope__c = 'Region';
        enterCampaign.RecordTypeId = mbEnterCmpnRT;
        testCampaigns.add(enterCampaign);

        Campaign trainCampaign = new Campaign ();
        trainCampaign.Name = 'MB Training';
        trainCampaign.Status = 'Open';
        trainCampaign.EventDescription__c = 'testing training event';
        trainCampaign.EventStartDateTime__c = System.now().addDays(1);
        trainCampaign.EventEndDateTime__c = System.now().addDays(2);
        trainCampaign.Location__c = 'Sacramento, CA';
        trainCampaign.Scope__c = 'Region';
        trainCampaign.CourseId__c = '2222';
        trainCampaign.CourseName__c = 'Some more training';
        trainCampaign.TypeofTraining__c = 'Website';
        trainCampaign.RecordTypeId = mbTrainCmpnRT;
        testCampaigns.add(trainCampaign);

        Campaign defCampaign = new Campaign ();
        defCampaign.Name = 'Default nonMB';
        defCampaign.RecordTypeId = defaultCmpnRT;
        testCampaigns.add(defCampaign);

        helper_CampaignMemberMBGoal.happened = false;
        insert testCampaigns;
    }
    @isTest public static void createServiceRequest () {
        Id defaultServReqRT = Schema.SObjectType.Case.RecordTypeInfosByName.get('Default').RecordTypeId;
        Id mbServReqRT = Schema.SObjectType.Case.RecordTypeInfosByName.get('Medicare Broker').RecordTypeId;
 
        List<Case> testServiceRequests = new List<Case> ();
        Case mbServReq = new Case ();
        mbServReq.Status = 'Open';
        mbServReq.Subject = 'Testing';
        mbServReq.RecordTypeId = mbServReqRT;
        testServiceRequests.add(mbServReq);

        Case defServReq = new Case ();
        defServReq.Status = 'Open';
        defServReq.Subject = 'Testing';
        defServReq.Type = 'Administrative';
        defServReq.Type_Description__c = 'Audit';
        defServReq.RecordTypeId = defaultServReqRT;
        testServiceRequests.add(defServReq);

        insert testServiceRequests;
    }
    
    @isTest public static void createTestTermReason () 
    {
 
        MBTerminationReasons__c termReason = new MBTerminationReasons__c ();
        termReason.Name = 'SUSP';
        termReason.TerminationReason__c = 'Suspension';

        insert termReason;

    }
    public static List<vlocity_ins__Application__c> createApplication(Integer numApplications)
    {
        List<vlocity_ins__Application__c> applicationList = new  List<vlocity_ins__Application__c>();
        for(Integer i = 0; i<numApplications; i++)
        {
            vlocity_ins__Application__c vlApp   = new vlocity_ins__Application__c();
            vlApp.Name                          = 'Test Application' + Math.random();
            vlApp.vlocity_ins__Status__c        = 'In Progress';
            vlApp.State_Carrier__c              = 'AR - Celtic Insurance Company';
            vlApp.Contract_Type__c              = 'Sub Producer';
            applicationList.add(vlApp);
        }
        if(applicationList.size()>0)
        {
            insert applicationList;
        }
        return applicationList;
    }
    public static List<ContentDocumentLink> insertContentDocumentList(Integer numCDL, Id linkedEntityId)
    {
        List<ContentDocumentLink> listCDL = new List<ContentDocumentLink>();
        ContentVersion content=new ContentVersion(); 
        content.Title='Ambetter Sub Producer Agreement'; 
        content.PathOnClient='Test Path'; 
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        content.VersionData=bodyBlob; 
        content.origin = 'H';
        insert content;
        
        ContentVersion content1=new ContentVersion(); 
        content1.Title='Ambetter Sub Producer Agreement'; 
        content1.PathOnClient='Test Path'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test ContentVersion Body'); 
        content1.VersionData=bodyBlob; 
        content1.origin = 'H';
        insert content1;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.contentdocumentid=documents[0].Id;
        contentlink.ShareType = 'V';
        contentlink.Visibility = 'AllUsers';
        listCDL.add(contentlink);
        
        
        ContentDocumentLink contentlink1=new ContentDocumentLink();
        //contentlink1.LinkedEntityId=linkedEntityId;
        contentlink1.contentdocumentid=documents[0].Id;
        contentlink1.ShareType = 'V';
        contentlink1.Visibility = 'AllUsers';
        listCDL.add(contentlink1);
        
        if(listCDL.size()>0){
            insert listCDL;
        }
        return listCDL;
    }
    public static List<ContentDocumentLink> createCDL(Id LinkedEntityId)
    {
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Ambetter New Producer Agreement',
            PathOnClient = 'Penguins.pdf',
            VersionData = Blob.valueOf('Unit.Test'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        ContentVersion contVer = [SELECT ID, Title, FileType FROM ContentVersion WHERE Id=:contentVersion.Id];
        System.debug('File Type::::'+contVer.FileType);
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        //create ContentDocumentLink  record 
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = LinkedEntityId;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        cdlList.add(cdl);
        insert cdlList;
        return cdlList;
    }
    public static List<Contact> createContactList(List<vlocity_ins__Application__c> applicationList)
    {
        List<Contact> contactList = new List<Contact>();
        Account accObj = new Account();
        accObj.Name = 'PlumTree';
        accObj.Novasys_DBA_Name__c = 'NovasysDBA';
        accObj.TaxId__c = 'Test Tax';
        insert accObj;
        
        Contact cont1 = new Contact();
        cont1.LastName = 'Indra';
        cont1.AccountId = accObj.Id;
        cont1.Email = 'balam@megnity.com';
        cont1.Contact_Type__c= 'Sub Producer';
        cont1.vlocity_ins__Status__c = 'Active';
        contactList.add(cont1);
        Contact cont2 = new Contact();
        cont2.LastName = 'Sena Reddy';
        cont2.Contact_Type__c= 'Sub Producer';
        cont2.AccountId = accObj.Id;
        cont2.Email = 'balam@megnity.com';
        cont2.vlocity_ins__Status__c = 'Active';
        contactList.add(cont2);
        
        insert contactList;
        return contactList;
    }
    public static List<vlocity_ins__OmniScriptInstance__C> insertOmniScript(Integer numOfOmniscript, Id appId)
    {
        List<vlocity_ins__OmniScriptInstance__C> listOmniscript = new List<vlocity_ins__OmniScriptInstance__C>();
        for(Integer i=0; i<numOfOmniscript; i++)
        {
            vlocity_ins__OmniScriptInstance__C omniscriptObj = new vlocity_ins__OmniScriptInstance__C();
            omniscriptObj.vlocity_ins__ResumeLink__C = 'Test Resume'; 
            omniscriptObj.vlocity_ins__ObjectName__C = 'Omni Script';
            omniscriptObj.vlocity_ins__ObjectId__C= 'OmniObject'; 
            omniscriptObj.vlocity_ins__ObjectLabel__C='Application'; 
            omniscriptObj.vlocity_ins__Status__C = 'In Progress ';  
            omniscriptObj.vlocity_ins__ObjectId__C = appId;
            listOmniscript.add(omniscriptObj);
        }
        if(listOmniscript.size()>0)
        {
            System.debug('@@@@@');
            insert listOmniscript;
            //Database.insert(listOmniscript);
        }
        System.debug('listOmniscript::::'+listOmniscript);
        return listOmniscript;
    }
    public static List<Case> createCases(Id appId, Integer numCases)
    {
        List<Case> listCase = new List<Case>();
        Profile p = [SELECT Id, Name FROM Profile WHERE Name like '%Group Transaction%' limit 1]; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='grouptransactionuser@testorg.com');
        insert u;
        for(Integer i = 0; i<numCases; i++)
        {
            Case caseObj = new Case();
            caseObj.Subject = 'Test Subject' + Math.random();
            caseObj.Application__c = appId;
            caseObj.Status= 'In Process';
            caseObj.Type_Description__c= 'ABS Refresh';
            caseObj.SF_ORG__c = '';
            caseObj.Original_Requestor__c = u.id;
            //caseObj.RecordType.Name = 'Default';
            listCase.add(caseObj);
        }
        if(listCase.size()>0)
        {
            insert listCase;
        }
        return listCase;
    }
    public static List<Contract> createContractList(Integer numContracts, Contact broker)
    {
        List<Contract> contrctList = new List<Contract>();
        for(Integer i =0; i<numContracts; i++)
        {
            Contract contractObj = new Contract();
            contractObj.Status = 'Draft';
            contractObj.Broker__c = broker.Id;
            contractObj.Carrier__c = 'Absolute Total Care';
            contractObj.AccountId  = broker.AccountId;
            
            contrctList.add(contractObj);
        }
        if(contrctList.size()>0)
        {
            insert contrctList;
        }
        return contrctList;
    }

}