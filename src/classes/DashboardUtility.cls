public without sharing class DashboardUtility {
    
    @AuraEnabled(cacheable=true)
    public static Map <String, Object > getDashboardData() {
        String baseURL = System.Label.PortalBaseURL;
        Map < String, Object > output = new Map < String, Object >();
        // Get current user and corresponding contact ID
        String userId = UserInfo.getUserId();
        List<User> user = [Select Id,ContactId from User where Id = :userId];
        String contactId = '00';
        if( user != null && user.size() > 0 )
        {
            contactId =user[0].ContactId;
        }
         
        // Appointments
        List<AppointmentWrapper> appointments = new List<AppointmentWrapper>();
        for(vlocity_ins__ProducerAppointment__c item: [SELECT Name,vlocity_ins__EffectiveDate__c,vlocity_ins__ExpirationDate__c,vlocity_ins__Status__c,vlocity_ins__CarrierId__r.Name,vlocity_ins__LinesofAuthority__c,vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerAppointment__c where vlocity_ins__ProducerId__c=:contactId])
        {
            appointments.add(new AppointmentWrapper(item));
        }

        // Licenses
        List<LicenseWrapper> licenses = new List<LicenseWrapper>();
        for(vlocity_ins__ProducerLicense__c item: [SELECT Name,vlocity_ins__LicenseType__c,vlocity_ins__Jurisdiction__c,vlocity_ins__EffectiveDate__c,vlocity_ins__ExpirationDate__c,vlocity_ins__Status__c,vlocity_ins__OriginalIssueDate__c from vlocity_ins__ProducerLicense__c where vlocity_ins__ProducerId__c=:contactId])
        {
            licenses.add(new LicenseWrapper(item));
        }

        // Contracts
        Map<Id, List<Contract>> mapApplicationContract = new Map<Id, List<Contract>>();
        for(Contract item: [SELECT ContractNumber,ContractTerm,StartDate,EndDate,Contract_Status__c,
                            vlocity_ins__ApplicationId__r.Id,State__c,vlocity_ins__ContractType__c
                            FROM Contract 
                            WHERE vlocity_ins__ProducerId__c=:contactId]){
            if(mapApplicationContract.get(item.vlocity_ins__ApplicationId__r.Id) == null){
                mapApplicationContract.put(item.vlocity_ins__ApplicationId__r.Id, new List<Contract>());
            }  
            mapApplicationContract.get(item.vlocity_ins__ApplicationId__r.Id).add(item);              
        }

        Map<Id, List<DocumentWrapper>> mapDocumentLink = new Map<Id, List<DocumentWrapper>>();
        if(mapApplicationContract.size()>0){
            for(ContentDocumentLink documentLink: [SELECT Id , ContentDocumentId,LinkedEntityId,ContentDocument.Title 
                                                FROM ContentDocumentLink 
                                                WHERE LinkedEntityId in:mapApplicationContract.keySet()]){
                if(mapDocumentLink.get(documentLink.LinkedEntityId) == null){
                    mapDocumentLink.put(documentLink.LinkedEntityId, new List<DocumentWrapper>());
            }  
                mapDocumentLink.get(documentLink.LinkedEntityId).add(new DocumentWrapper(documentLink));   
            }
        }
        
        
        List<ContractWrapper> contracts = new List<ContractWrapper>();
        for(Id key: mapApplicationContract.keySet()){
            for(Contract item: mapApplicationContract.get(key)){
                contracts.add(new ContractWrapper(item,mapDocumentLink.get(key)));
            }
        }

        // Applications
        List<vlocity_ins__OmniScriptInstance__c> inProgressApps = new List<vlocity_ins__OmniScriptInstance__c>();
        List<ApplicationWrapper> applications = new List<ApplicationWrapper>();
        Map<Id,String> appIdName = new Map<Id,String>();
        for(vlocity_ins__Application__c item: [SELECT Name,Contract_Type__c,State_Carrier__c,vlocity_ins__Status__c 
                                                FROM vlocity_ins__Application__c 
                                                WHERE Broker__r.Id=:contactId])
        {
            applications.add(new ApplicationWrapper(item));
            
            if(item.vlocity_ins__Status__c != 'Rejected') {
                appIdName.put(item.Id, item.Name);
            }
            
        }

        // Fetch In progress Apps
        for(vlocity_ins__OmniScriptInstance__c saved: [SELECT vlocity_ins__ObjectId__c,Name,vlocity_ins__LastSaved__c,vlocity_ins__RelativeResumeLink__c,vlocity_ins__ResumeLink__c,vlocity_ins__Status__c 
                                                        FROM vlocity_ins__OmniScriptInstance__c 
                                                        WHERE vlocity_ins__ObjectLabel__c='Application' AND 
                                                        (vlocity_ins__Status__c='In Progress') AND 
                                                        vlocity_ins__ObjectId__c IN : appIdName.keySet()]){
            saved.vlocity_ins__RelativeResumeLink__c = baseURL + 'brokercommissions/s/resume-broker-details?actionUrl=' + saved.vlocity_ins__RelativeResumeLink__c;
            if(appIdName.containsKey(saved.vlocity_ins__ObjectId__c)){
                saved.Name = appIdName.get(saved.vlocity_ins__ObjectId__c);
            }
            inProgressApps.add(saved);
        }
        
        output.put('inProgressApps',inProgressApps);
        output.put('appointments',appointments);
        output.put('licenses',licenses);
        output.put('contracts',contracts);
        output.put('applications',applications);
        return output;
    }
    
    /** Wrapper Classes */
    public class ApplicationWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public String status;
        @AuraEnabled public String contractType;
        @AuraEnabled public String stateCarrier;
        
        public ApplicationWrapper(vlocity_ins__Application__c application){
            this.id = application.Id;
            this.name = application.Name;
            this.contractType = application.Contract_Type__c;
            this.stateCarrier = this.formatMultivalues(application.State_Carrier__c);
            this.status = application.vlocity_ins__Status__c;
        }

        public String formatMultivalues(String value){
            if(!String.isEmpty(value)){
                String formattedValue = value.replace(';', '\n');
                return formattedValue;
            }
            return '';
        }
    }

    public class DocumentWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String title;
        @AuraEnabled public String url;
        
        
        public DocumentWrapper(ContentDocumentLink documentLink){
            this.id = documentLink.ContentDocument.Id;
            this.title = documentLink.ContentDocument.Title;
            this.url = '/brokercommissions/sfc/servlet.shepherd/document/download/' + this.Id;
        }
    }

    public class ContractWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public Date startDate;
        @AuraEnabled public Date endDate;
        @AuraEnabled public String status;
        @AuraEnabled public Integer term;
        @AuraEnabled public List<DocumentWrapper> documents = new List<DocumentWrapper>();
        @AuraEnabled public String state;
        @AuraEnabled public String contractType;
        
        public ContractWrapper(Contract contract,List<DocumentWrapper> contractDocuments){
            this.id = contract.Id;
            this.name = contract.ContractNumber;
            this.startDate = contract.StartDate;
            this.endDate = contract.EndDate;
            this.term = contract.ContractTerm;
            this.status = contract.Contract_Status__c;
            this.documents = contractDocuments;
            this.state = contract.State__c;
            this.contractType = contract.vlocity_ins__ContractType__c;
        }
    }

    public class LicenseWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String licenseNumber;
        @AuraEnabled public String jurisdiction;
        @AuraEnabled public Date effectiveDate;
        @AuraEnabled public Date expirationDate;
        @AuraEnabled public String status;
        @AuraEnabled public Date issueDate;
        @AuraEnabled public String licenseType;

        
        public LicenseWrapper(vlocity_ins__ProducerLicense__c license){
            this.id = license.Id;
            this.licenseNumber = license.Name;
            this.licenseType = license.vlocity_ins__LicenseType__c;
            this.issueDate = license.vlocity_ins__OriginalIssueDate__c;
            this.expirationDate = license.vlocity_ins__ExpirationDate__c;
            this.status = license.vlocity_ins__Status__c;
            this.jurisdiction = license.vlocity_ins__Jurisdiction__c;
            this.effectiveDate = license.vlocity_ins__EffectiveDate__c;
        }
    }

    public class AppointmentWrapper{
        @AuraEnabled public String id;
        @AuraEnabled public String name;
        @AuraEnabled public Date effectiveDate;
        @AuraEnabled public Date expirationDate;
        @AuraEnabled public String status;
        @AuraEnabled public String producerName;
        @AuraEnabled public String carrierName;
        @AuraEnabled public String linesOfAuthority;
        @AuraEnabled public String jurisdiction;

        public AppointmentWrapper(vlocity_ins__ProducerAppointment__c appointment){
            this.id = appointment.Id;
            this.name = appointment.Name;
            this.jurisdiction = appointment.vlocity_ins__Jurisdiction__c;
            this.linesOfAuthority = appointment.vlocity_ins__LinesofAuthority__c;
            this.effectiveDate = appointment.vlocity_ins__EffectiveDate__c;
            this.expirationDate = appointment.vlocity_ins__ExpirationDate__c;
            this.status = appointment.vlocity_ins__Status__c;

            this.carrierName = appointment.vlocity_ins__CarrierId__r.Name;
        }
    }
    /** Wrapper classes ends here */
}