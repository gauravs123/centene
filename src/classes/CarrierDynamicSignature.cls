/*****************************************************
* Purpose : Controller class to create Carrier Signature
* PDF file
* Created By : Rangesh Kona
* Date : 18th, Nov-2019
* ***************************************************/

public with sharing class CarrierDynamicSignature {
    
    public static List<vlocity_ins__Application__C> appObjList = new List<vlocity_ins__Application__C>();
    
    public static String stateCarrierDetails = '';    
    public static List<String> stateList = new List<String>();
    public static List<String> states = new List<String>();
    public static List<String> stateCarrierList = new List<String>();
    public static List<Carrier_Signature__c> carrierSignature = new List<Carrier_Signature__c>();
    
    
    public static List<String> getStateList() {        
        return stateList;
    }
    
    // state carrier list like 'AR - Celtic Insurance Company'
    public static List<String> getStateCarrierList() {
        return stateCarrierList;
    }
    
    // State Carrier Details from Application Record( Multi-Select value )
    public static String getStateCarrierDetails() {
        return stateCarrierDetails;
    }
    
    public static PageReference generatePDF(){
        String appId =  ApexPages.currentPage().getParameters().get('appId');
        // appId = 'a21L0000001W9eWIAS';
        appObjList = [select State_Carrier__c from vlocity_ins__Application__C where id = :appId ];
        
        if (appObjList.size() > 0 ) {stateCarrierDetails = appObjList[0].State_Carrier__c;
            // system.debug('stateCarrierDetails ' + stateCarrierDetails);
            if (stateCarrierDetails != null) {stateCarrierList = stateCarrierDetails.split(';');
                
                for(String stateCarrier : stateCarrierList) {
                    states = stateCarrier.split('-');
                    String state = states[0];
                    state= state.replaceAll( '\\s+', '');
                    System.debug(' State ===> ' + state);
                    stateList.add(state);
                } 
            }
        }  
        // set carrier signature list
        getCarrierSignatureList();
        return null;
    }
    
    // send data to visualforce page
    public List<Carrier_Signature__c> getCarrierSignature() {
        return carrierSignature;
    }
    
    // get Carrier Signature matching list
    public static void getCarrierSignatureList() { 
        
        carrierSignature = [select Name, Name__C, Plan__C, State_Code__C, Title__C, State_Carrier__C, Signature_Image__c from Carrier_Signature__c 
                            where State_Carrier__C in :stateCarrierList];
        
        system.debug('RK : Signature Details : for stateList ' + ' ===>  ' + stateCarrierList + ' ===> ' + carrierSignature );
        
    }
    
    
}