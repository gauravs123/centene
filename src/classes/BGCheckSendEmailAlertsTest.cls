@isTest
private class BGCheckSendEmailAlertsTest {

 private static testMethod void doTest()
    {
		//Test.startTest();
		Account testAccount = new Account();
		testAccount.Name='Test Account' ;
        testAccount.TaxId__c = 'Test588';
		insert testAccount;
		
		Contact cont = new Contact();
		cont.FirstName='Test';
		cont.LastName='Test';
        cont.Email='test@test.com';
        cont.Contact_Type__c ='Sub Producer';
		cont.Accountid= testAccount.id;
		insert cont;
        
        Contact cont1 = new Contact();
		cont1.FirstName='Test';
		cont1.LastName='Test';
        cont1.Email='Test1@test.com';
        cont1.Contact_Type__c ='Principal';
		cont1.Accountid= testAccount.id;
		insert cont1;

        List<id> listid=new list<id>();
		listid.add(cont.id);
        
         BGCheckSendEmailAlerts.sendBackgroundCheckFailedEmail(listid);
      //  Test.stopTest();
    }
    
}