public class LicensePlatformEventTriggerHelper {
    public static License_Event__e createLicenseEvent(vlocity_ins__ProducerLicense__c license){
         License_Event__e licenseEvent = new License_Event__e(
                        Agent_Master_Id__c = license.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                        Novasys_Primary_Id__c = license.Novasys_Primary_Id__c,
                        Effective_Date__c= license.vlocity_ins__EffectiveDate__c,
                        License_Number__c = license.Name,
                        License_Type__c = license.vlocity_ins__LicenseType__c,
                        Producer__c = license.vlocity_ins__ProducerId__c,
                        SalesforceId__c = license.Id,
                        State__c = license.vlocity_ins__Jurisdiction__c,
                        Termination_Date__c = license.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = false//license.Tech_Awaiting_Novasys_Receipt__c
                    );
        return licenseEvent;
    }
    
    public static License_Event__e createLicenseEvent(vlocity_ins__AgencyLicense__c license){
         License_Event__e licenseEvent = new License_Event__e(
                    Agent_Master_Id__c = license.vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c,
                    Novasys_Primary_Id__c = license.Novasys_Primary_Id__c,
                    Effective_Date__c= license.vlocity_ins__EffectiveDate__c,
                    License_Number__c = license.Name,
                    License_Type__c = license.vlocity_ins__LicenseType__c,
                    Producer__c = license.vlocity_ins__AgencyBrokerageId__c,
                    SalesforceId__c = license.Id,
                    State__c = license.vlocity_ins__Jurisdiction__c,
                    Termination_Date__c = license.vlocity_ins__ExpirationDate__c,
                    Tech_Awaiting_Novasys_Receipt__c = license.Tech_Awaiting_Novasys_Receipt__c
                    );
        return licenseEvent;
    }
}