@isTest
public class Broker_RemoveStatesTest 
{
    static testMethod void createUserAccount()
    {
        test.startTest();
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.email = 'email@gmail.com';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.Contact_Type__c = 'Principal';
        insert agent;
        
        Contract cont = new Contract();
        cont.AccountId = agency.Id;
        cont.Contract_Status__c = 'Active';
        cont.vlocity_ins__ProducerId__c = agent.Id;
        cont.State__c = 'AL';
        cont.Contract_Status__c = 'Active';
        insert cont;
        
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        
        Map<String, Object> inputmap = new Map<String,Object>();
        List<Object> listCont = new List<Object>();
        List<Object> listAgent = new List<Object>();
        //listCont.add(cont);
        inputs.put('SelectedStateNames','AL,AK,AX');
        inputs.put('Contracts',listCont);
        inputs.put('PrincipalContact',inputmap);
        listAgent.add(agent);
        Map<String, Object> newMap = new Map<String, Object>();
        inputmap.put('SubProducers',Null);
        inputmap.put('BrokerId',agent.Id);
        Broker_RemoveStates broker = new Broker_RemoveStates();
        broker.invokeMethod('removeStates',inputs,output,options);
        System.assert(true,cont.Contract_Status__c == 'Termed');
        Map<String, Object> inputMapNew = new Map<String,Object>();
        Map<String, Object> subMap  = new Map<String, Object>();
        subMap.put('BrokerId',agent.Id);
        String jsonString = '{"SubProducers":[{"BrokerId":"'+agent.Id+'"}]}';
        Map<String, Object> mp = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
        inputs.put('PrincipalContact',mp);
        broker.invokeMethod('removeStates',inputs,output,options);
        test.stopTest();
    }
}