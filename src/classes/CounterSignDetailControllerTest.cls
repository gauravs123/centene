@isTest(seeAllData = false)
public class CounterSignDetailControllerTest 
{
    @TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
    }
    static testMethod void testConterSignDetail()
    {
        getAllMetadata();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(1);
        List<Contact> lstContact = TestDataFactory.createContactList(applicationList );
        lstContact[0].BGCheck_Status__c ='Passed';
        update lstContact;
        applicationList[0].Broker__c = lstContact[0].Id;
        update applicationList[0];
        List<Contract> contractList = TestDataFactory.createContractList(1,lstContact[0]);
        vlocity_ins__ProducerAppointment__c app = new vlocity_ins__ProducerAppointment__c (Contract__c =contractList[0].Id, vlocity_ins__Status__c='Appointed',
        vlocity_ins__ProducerId__c = lstContact[0].id);
        insert app;
        Test.startTest();
        //PageReference pageRef = Page.CounterSignDetail;
        //Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(applicationList[0]);
            CounterSignDetailController controller = new CounterSignDetailController(sc);
            List<ContentDocumentLink> CDLList = TestDataFactory.createCDL(applicationList[0].Id); 
            controller.signApplications();
        Test.stopTest();   
    }
}