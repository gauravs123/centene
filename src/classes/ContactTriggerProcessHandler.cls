/*
 * 
 *  Author : PTG ( RK ) 
 * 
 */
public class ContactTriggerProcessHandler {
    
    public static void checkContactValidation(Map<Id,Contact> newContacts, List<Contact> oldContacts) {
        
        System.debug('Executing Update Operation in Handler ');
        
        List<Id> accountIds = new List<Id>();
        List<Contact> allAccountContacts = new List<Contact>();
        Map<String,Integer>  contactAccountMap = new Map<String, Integer>();
        Map<Id, Id> contactMap = new Map<Id, Id>();
        
        for(Contact contact: oldContacts) {
            if(contact.AccountId != null) {
                if(contact.Contact_Type__c != newContacts.get(contact.Id).Contact_Type__c && newContacts.get(contact.Id).Contact_Type__c == 'Principal'){
                  accountIds.add(contact.AccountId);
                }//contactMap.put(contact.id, contact.id);
            } // end-if    
        } // end-for
        
        if (accountIds.size() >0 ) {
            allAccountContacts = [select id, AccountId, Contact_Type__c  from Contact where AccountId in :accountIds and Contact_Type__c = 'Principal'];
            for(Contact allContact : allAccountContacts) {
                String contactType = allContact.Contact_Type__c ;
                if ((contactType != null) && contactType.contains('Principal')) {
                    if (contactAccountMap.get(allContact.AccountId) > 0) {
                        contactAccountMap.put(allContact.AccountId, 1  + contactAccountMap.get(allContact.AccountId));
                    }
                    else {
                        // contactAccountMap.put(allContact.AccountId, 1);
                        System.debug(' Setting up Account Map to 1 ' + allContact.AccountId   );
                            contactAccountMap.put(allContact.AccountId, 1);
                    } 
                } // end if
            } // end for loop
        

        
        for(Contact contact: newContacts.values()) {
            if(contact.AccountId != null) {
                String contactType = contact.Contact_Type__c;
                Integer principalCount = contactAccountMap.get(contact.AccountId);
                System.debug(' Principal Count ' +principalCount );
                System.debug(' Contact Type ' + contactType );
                   System.debug(' Trigger is Update  ' + Trigger.isUpdate );
                if ((contactAccountMap.get(contact.AccountId) >= 1) && Trigger.isUpdate && contactType.contains('Principal')) {
                    System.debug(' Let me throw the error '  + contact.Id);
                    contact.addError('Principal Broker Record already exists to this Account');
                }
            }
        }
        } // end-if
    }
    
        public static void checkContactInsretValidation(List<Contact> contacts) {
            
         System.debug('Executing Insert Operation in Handler ');
         
             List<Id> accountIds = new List<Id>();
        List<Contact> allAccountContacts = new List<Contact>();
        Map<String,Integer>  contactAccountMap = new Map<String, Integer>();
        Map<Id, Id> contactMap = new Map<Id, Id>();
        
        for(Contact contact: contacts) {
            if(contact.AccountId != null) {
                accountIds.add(contact.AccountId);
                //contactMap.put(contact.id, contact.id);
            } // end-if    
        } // end-for
        
        if (accountIds.size() >0 ) {
            allAccountContacts = [select id, AccountId, Contact_Type__c  from Contact where AccountId in :accountIds and Contact_Type__c = 'Principal'];
            for(Contact allContact : allAccountContacts) {
                String contactType = allContact.Contact_Type__c ;
                if ((contactType != null) && contactType.contains('Principal')) {
                    if (contactAccountMap.get(allContact.AccountId) > 0) {
                        contactAccountMap.put(allContact.AccountId, 1  + contactAccountMap.get(allContact.AccountId));
                    }
                    else {
                        // contactAccountMap.put(allContact.AccountId, 1);
                            contactAccountMap.put(allContact.AccountId, 1);
                    } 
                } // end if
            } // end for loop
        } // end-if

        
        for(Contact contact: contacts) {
            if(contact.AccountId != null) {
                String contactType = contact.Contact_Type__c;
  		        if ((contactAccountMap.get(contact.AccountId) > 0) && contactType.contains('Principal') ) { 
                    contact.addError('Principal Broker Record already exists');
                }
            }
        }
     
        
        }
        
        
}