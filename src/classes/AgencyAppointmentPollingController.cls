public class AgencyAppointmentPollingController {
    public String result{get;set;}
    private vlocity_ins__AgencyAppointment__c appointment;
    public AgencyAppointmentPollingController(ApexPages.StandardController standardController){
        appointment =  (vlocity_ins__AgencyAppointment__c)standardController.getRecord();
    }
    public void callPollingIP(){
       // Input arguments for Integraiton Procedure
        Map<String, Object> ipInput = new Map<String, Object>();
        Map<String, Object> ipOutput = new Map<String, Object>();
        Map<String, Object> ipOptions = new Map<String, Object>();
        
        ipOptions.put('chainable', true);
        // set input parameter as ContextId , applicationId    
        // Call Ingegration Procedure NIPR Appointment    
        ipInput.put('ContextId', appointment.Id);
        ipOutput = (Map <String, Object>)
            vlocity_ins.IntegrationProcedureService.runIntegrationService('NIPR_AgencyPolling', ipInput, ipOptions);
        
        System.debug(' IP Output ' + ipOutput); 
        
        if(ipOutput.containskey('LogMessage')){
            result = Json.serialize(ipOutput.get('LogMessage'));
        }
        else{
            appointment = [SELECT ID,NIPR_Description__c From vlocity_ins__AgencyAppointment__c where Id =: appointment.Id];
            result = appointment.NIPR_Description__c;
        }
    }
    
    
    public PageReference backToAppointment() { 
        PageReference pageRef = new PageReference('/'+appointment.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

}