@isTest(seeAllData=false)
public class ApplicationStateUpdateTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        return listDocSign;
	}
    static testMethod void testApplicationStateUpdate()
    {
        getAllMetadata();
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        Set<Id> appIds	= new Set<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(1);
        String applicationId = applicationList[0].Id;
        inputs.put('applicationId', applicationId);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        ApplicationStateUpdate appStUpdate = new ApplicationStateUpdate();
        appStUpdate.invokeMethod('unCheckContractTermination', inputs, output, options);
        ApplicationStateUpdate.updateStateCarrier(new List<Object>{'AP','NV'}, applicationId);

        //appStUpdate.updateState(inputs, output, options);
    }
    static testMethod void testApplicationStateUpdate1()
    {
        getAllMetadata();
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        Set<Id> appIds	= new Set<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(1);
        String applicationId = applicationList[0].Id;
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        ApplicationStateUpdate appStUpdate = new ApplicationStateUpdate();
        options.put('states', 'AP');
        options.put('applicationId', applicationId);
        options.put('KA', 'KA');
        appStUpdate.invokeMethod('updateState', inputs, output, options);
    }
}