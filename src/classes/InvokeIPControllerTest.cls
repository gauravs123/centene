@isTest
public class InvokeIPControllerTest {
    static testmethod void executetest()
    {
         Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> ipOptions = new Map<String, Object>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(1);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
             inputs.put('ContextId', appObj.Id);
        }
        InvokeIPController.getIPData('NIPR_ProducerAdditionalInfoUpdate',inputs,ipOptions);
    }

}