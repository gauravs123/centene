public class ContentDocumentLinkTriggerHandler {
    public static void updateContentDocumentLinkVisibility(List <ContentDocumentLink> triggerNew){
        for(ContentDocumentLink cdl : triggerNew){
            if(cdl.LinkedEntityId != null && 
               Schema.vlocity_ins__Application__c.SObjectType == cdl.LinkedEntityId.getSobjectType()){
                   cdl.visibility	=	'AllUsers';
            }
        }
    }
}