public class Trigger_Utility {
    public static list<Sobject> filterList(list<sObject> listObject, String fieldName, Boolean isFormula){
        list<sObject> filteredList = new list<sObject>();
        list<FormulaRecalcResult> listCalculatedFormula ;
        if(isFormula){
            listCalculatedFormula =  Formula.recalculateFormulas(listObject);
        }
         
        for(SObject obj: listObject){
            system.debug('>>>>>>>>>>>>>>>'+obj.get(fieldName) );
            if(obj.get(fieldName) == true){
                filteredList.add(obj);
            }
        }
        return filteredList;
    }
    public static list<Sobject> filterList(list<sObject> listObject, String fieldName){
        list<sObject> filteredList = new list<sObject>();
         
        for(SObject obj: listObject){
            system.debug('>>>>>>>>>>>>>>>'+obj.get(fieldName) );
            if(obj.get(fieldName) == true){
                filteredList.add(obj);
            }
        }
        return filteredList;
    }
    
    public static boolean isTriggerDisabled() {
        
        Manage_Triggers__c mt = Manage_Triggers__c.getInstance(UserInfo.getUserId());
        boolean isDisable = mt != null? mt.DisableAllTriggers__c : false;
        return isDisable;
    }
}