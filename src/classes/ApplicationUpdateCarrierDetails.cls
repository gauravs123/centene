global class ApplicationUpdateCarrierDetails
{
    list<String> ls;
	public static List<vlocity_ins__Application__C> appObjects = new List<vlocity_ins__Application__C>();
    		
    
    @InvocableMethod
    public static List<String> ParseMultiSelect(List<vlocity_ins__Application__C> applicationObjects)
    {
        list<String> ls= new list<String>();
        vlocity_ins__Application__C appObj = new vlocity_ins__Application__C();
        
        for(vlocity_ins__Application__C applicationObject : applicationObjects)
        {
            String a=String.ValueOf(applicationObject.State_Carrier__c);
            ls=a.split(';');
            System.debug('&&&&&&&'+ls);
            if (ls[0] != null) {
                appObj.id = applicationObject.id;
            //    appObj.CarrierOne__c = ls[0];
            }
            appObjects.add(appObj);
        }
        update appObjects;
        System.debug('#######'+ls);
        return ls;
    }
}