global without sharing class Broker_RemoveStates implements vlocity_ins.VlocityOpenInterface2 {
    
    global Boolean invokeMethod(String methodName, Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options) {
        if (methodName.equals('removeStates')) {
            return removeStates(inputs, output, options);
        }
        return false;
    }
    
    public boolean removeStates(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options) {      
        Contract[] relatedContracts = new Contract[]{};
        String selectedStateNames = (String)inputs.get('SelectedStateNames');
        selectedStateNames = selectedStateNames.replaceAll('\\s+', '');
        Object[] contracts = (Object[]) inputs.get('Contracts');
        String[] states = (String[])selectedStateNames.split(',');
        
        for (Object contract : contracts) {
           Map<String, Object> mapContract = (Map<String, Object>) contract;
        }
        Map<String, Object> principalContact = (Map<String, Object>)inputs.get('PrincipalContact');
        Id principalId = (Id)principalContact.get('PrincipalId');
        Id brokerId = (Id)principalContact.get('BrokerId');
        system.debug('brokerId'+brokerId);
        system.debug('principalId'+principalId);
        if (brokerId == principalId) {
            system.debug('producer');
            Id[] brokerIds = new Id[]{};
            brokerIds.add((Id)principalContact.get('BrokerId'));
            Id AgencyId = (Id)principalContact.get('AgencyId'); 
            
            Object[] subProducers = (Object[]) principalContact.get('SubProducers');
            for (Object subProducer : subProducers) {
                Map<String, Object> mapSubProducers = (Map<String, Object>) subProducer;
                brokerIds.add((Id)mapSubProducers.get('BrokerId'));
            }
            relatedContracts = [SELECT Id, State__c, Contract_Status__c, vlocity_ins__ProducerId__c  FROM Contract WHERE AccountId =:AgencyId AND State__c IN: states AND Contract_Status__c != 'Termed'];
        } else {
            system.debug('subProducer');
            relatedContracts = [SELECT Id, State__c, Contract_Status__c, vlocity_ins__ProducerId__c   FROM Contract WHERE vlocity_ins__ProducerId__c =: principalId AND State__c IN: states AND Contract_Status__c != 'Termed'];
            System.debug(relatedContracts);
        }
        Contract[] updatedContracts = new Contract[]{};
        for (Contract relatedContract : relatedContracts) {
            relatedContract.Contract_Status__c = 'Termed';
            updatedContracts.add(relatedContract);
        }
        update updatedContracts;
        return true;
    }
}