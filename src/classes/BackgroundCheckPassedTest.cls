@isTest(seeAllData=false)
public class BackgroundCheckPassedTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        System.debug('listDocSign:::'+listDocSign);
        return listDocSign;
	}
    static testMethod void BackgroundCheckTest()
    {
        getAllMetadata();
        List<Id> contIds	= new List<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(20);
        List<Contact> contList = TestDataFactory.createContactList(applicationList);
        applicationList[0].Broker__C = contList[0].Id;
        applicationList[0].vlocity_ins__Status__c = 'In Progress';
        update applicationList;
        System.debug('contList in test class:::'+contList);
        for(Contact cont : contList)
        {
            contIds.add(cont.Id);
        }
        TestDataFactory.insertOmniScript(1, applicationList[0].Id);
        BackgroundCheckPassed.sendBackgroundCheckPassedEmail(contIds);
        BackgroundCheckPassed.getContactTypes();
        System.debug('appId in test class::::'+applicationList[0].Id);
        
    }
}