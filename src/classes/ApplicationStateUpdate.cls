global without sharing class ApplicationStateUpdate implements vlocity_ins.VlocityOpenInterface2 {
    
    global Boolean invokeMethod(String methodName, 
                                Map < String, Object > inputs, 
                                Map < String, Object > output, 
                                Map < String, Object > options) 
    {
        if(methodName == 'updateState'){
            return updateState(inputs, output, options);
        }
        if(methodName == 'unCheckContractTermination') {
            return unCheckContractTermination(inputs, output, options);
        }                             
        return false;
    }
    
    public boolean unCheckContractTermination(Map < String, Object > inputs, 
                                              Map < String, Object > output, 
                                              Map < String, Object > options)
    {
        
        String applicationId = (String)inputs.get('applicationId');
        system.debug('Application Id ' + applicationId);
        vlocity_ins__Application__c applicationList = [Select Id,Check_Contract_Termination__c from vlocity_ins__Application__c where Id = :applicationId];
        applicationList.Check_Contract_Termination__c = false;
        update applicationList;
        return true;
        
    }
    
    public boolean updateState(Map < String, Object > inputs, 
                               Map < String, Object > output, 
                               Map < String, Object > options)
    {
        
        List<String> allowedStates = new List<string> ();
        if(options.get('ApplicationCarrier') != null){
            allowedStates = ((String)options.get('ApplicationCarrier')).split(';'); 
            update new vlocity_ins__Application__c(Id = (String)options.get('ApplicationId'),State_Carrier__c=(String)options.get('ApplicationCarrier'));
        }
        else{
            try {
                allowedStates = ((String)options.get('states')).split(',');  
            } catch(Exception e) {
                List<Object> statesList = (List<Object>)options.get('states');
                for(Object state: statesList){
                    allowedStates.add(String.valueOf(state));
                }
                
            }
        }
        
        String applicationId = (String)options.get('applicationId');
        List<vlocity_ins__Application__c> applicationList = [Select Id,State_Carrier__c from vlocity_ins__Application__c where Id =: applicationId];
        if(applicationList.size()==0){ // Return if no application record found
            return false;
        }
        vlocity_ins__Application__c application = applicationList[0];
        String applicationStateCarrier = application.State_Carrier__c;
        
        if(applicationStateCarrier==null){
            return false;
        }
        
        List<String> stateCarrierList = applicationStateCarrier.split(';');
        List<String> finalStateCarrier = new List<String>();
        
        for(String t : stateCarrierList){
            List<String> stateCarrier = t.split('-'); // Split State Carrier. Example: FL - Celtic
            if(allowedStates.contains(stateCarrier[0].trim())){
                finalStateCarrier.add(t);
            }
        }
        
        application.State_Carrier__c = String.join(finalStateCarrier,';');
        update application;
        return false;
    }
    
    
    
    public static boolean updateStateCarrier(List<Object> ContractedStatesList, String applicationId)
    {
        
        List<String> allowedStates = new List<string> ();
        
        for(Object state: ContractedStatesList){
            allowedStates.add(String.valueOf(state));
        }
        
        List<vlocity_ins__Application__c> applicationList = [Select Id,State_Carrier__c from vlocity_ins__Application__c where Id =: applicationId];
        if(applicationList.size()==0){ // Return if no application record found
            return false;
        }
        vlocity_ins__Application__c application = applicationList[0];
        
        String applicationStateCarrier = application.State_Carrier__c;
        
        if(applicationStateCarrier==null){
            return false;
        }
        
        List<String> stateCarrierList = applicationStateCarrier.split(';');
        List<String> finalStateCarrier = new List<String>();
        
        for(String t : stateCarrierList){
            List<String> stateCarrier = t.split('-'); // Split State Carrier. Example: FL - Celtic
            if(allowedStates.contains(stateCarrier[0].trim())){
                finalStateCarrier.add(t);
            }
        }
        application.State_Carrier__c = String.join(finalStateCarrier,';');
        update application;
        return false;
    }
    
    
}