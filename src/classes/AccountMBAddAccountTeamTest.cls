/*
 *  Tests Class AccountMBAddAccountTeam. Testing as Integration user.
 */
@isTest 
public with sharing class AccountMBAddAccountTeamTest {
    private static User integ;
    private static User sysAdm;
    private static User sysAdm2;
    
    @testSetup static void setUpData() {
        TestDataFactory.createSimpleSet();
    }
    @testSetup static void setUpCounties() {
        TestDataFactory.createTestCounties();
    }
    @testSetup static void setUpAssignRules() {
        testDataFactory.createTestRules();
    }
    
    static void SetUpUsers(){
        String integProfileID = [Select Id from Profile WHERE Name = 'MB API Integration'].Id;
        String sysadmProfileID = [Select Id from Profile WHERE Name = 'System Administrator'].Id;
        String salesADMRoleID = [Select Id from UserRole WHERE Name = 'MB Sales Admin'].Id;
        integ = TestDataFactory.createTestUser(integProfileID, salesADMRoleID, 'Integ');
        sysadm = TestDataFactory.createTestUser(sysadmProfileID, salesADMRoleID, 'SysAdm');
        sysadm2 = TestDataFactory.createTestUser(sysadmProfileID, salesADMRoleID, 'SysAdm2');
    }
    static testMethod void testAddMBAccountTeam () 
    { 
      User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            SetUpUsers();
        }
        System.runAs ( sysadm ) {
            SetUpCounties();
        }
        System.runAs ( sysadm2 ) {
            SetUpAssignRules();
        }
        System.runAs ( integ ) 
        {
            test.startTest();
            setUpData();
            List<Assignment__c> testRules = new List<Assignment__c> ([Select Id, Name, State__c, Rule__c, AgencyName__c, AgencyTaxId__c, County__c
                                , PostalCode__c, OwnerId, SecondOwner__c from Assignment__c]);
            Map<String, Assignment__c> ruleToAssign = new Map<String, Assignment__c> ();
            for (Assignment__c a: testRules)
            { 
                ruleToAssign.put(a.Rule__c, a);
            }
            
            System.debug('** test rules:'+testRules);
            
    //    create account that will find owner and co-owner via agency assignment
            Account acct1 = new Account();
            acct1.BillingState = 'CA';
            acct1.BillingPostalCode = '777777';
            acct1.TaxId__c = 'Test555888';
            acct1.Name = 'AgencyAssignedAccount';
            helper_AccountMBAssignOwner.happened = false;
            helper_AccountMBAssignCounty.happened = false;
            helper_AccountMBAddAccountTeam.happened = false;
            insert acct1;
            List<Account> acct1i = new List<Account> ([Select Id, Name, OwnerId, Co_Owner__c from Account where Name = 'AgencyAssignedAccount' Limit 1]);
            System.debug('** AgencyAssigned:'+acct1i);
            System.assertEquals(acct1i[0].OwnerId, ruleToAssign.get('Agency').OwnerId);
            System.assertEquals(acct1i[0].Co_Owner__c, ruleToAssign.get('Agency').SecondOwner__c);
            List<AccountTeamMember> atm1 = new List<AccountTeamMember> ([Select Id, AccountId, UserId, TeamMemberRole, AccountAccessLevel, IsDeleted from AccountTeamMember 
                    where AccountId = :acct1i[0].Id and UserId = :acct1i[0].Co_Owner__c]);
            System.debug('*** atm 1:'+atm1);
            System.assertEquals(acct1i[0].Co_Owner__c,atm1[0].UserId);
            System.assertEquals(atm1[0].AccountAccessLevel, 'Edit');
            List<AccountShare> acctShare = new List<AccountShare> ([Select Id, AccountId, UserOrGroupId, AccountAccessLevel, CaseAccessLevel, ContactAccessLevel
                                        , RowCause, IsDeleted from AccountShare 
                    where AccountId = :acct1i[0].Id and UserOrGroupId = :acct1i[0].Co_Owner__c]);
            System.debug('*** acctShare:'+acctShare);
//          System.assertEquals(acct1i[0].Co_Owner__c,atm1[0].UserId);
    //      System.assertEquals(atm1[0].AccountAccessLevel, 'Edit');
        }
    }
}