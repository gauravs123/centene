/********************************************************************************************/
// Author : Rangesh Kona, Plum Tree Group
// Created Date : Dec 20th, 2019
// Description  :
// Send email to Producer if Producer Background Check Status is set to 'Passed'
// Send email to Sub Producer if Sub Producer Background Check status is set to 'Passed'
// Modification Log
// Jan 22nd, Removed sending email to Principal(Producer) if Sub Producer 
//           BG Check is passed
/********************************************************************************************/


public class BackgroundCheckPassed {
    
    public static String contactTypes;
    
    public static List<Id>          applicationIds              = new List<Id>();

    // Contact Id , OmniScript Resume Link
    public static Map<Id, String>   contactAppOmniScriptMap     = new Map<Id, String>();

    // Contact Id, OmniScript  
    public static Map<Id, Id>       contactAppOmniScriptIdMap   = new Map<Id, Id>();     
    
    public static String getContactTypes() {
        return contactTypes;
    }
    
    /*************************************************/
    // Invocable Method executed by Process Builder
    /*************************************************/

    @InvocableMethod
    public static void sendBackgroundCheckPassedEmail(List<ID> contactIds ) {
        
        System.debug('Send Sub Producer Email to ' + contactIds);
        
        Map<Id, Contact> contacts = new Map<Id, Contact>();
        List<Id> subProducerAccntIds = new List<Id>();
        
        // to keep all the triggered Account Ids, use these ids to identify the applications
        Map<Id, Id> contactAccntMap = new Map<Id, Id>();
           
        
        List<Id> producerIds = new List<Id>();
        
        String contactType = '';
        
        for (Contact contact: [ select Id, AccountId , Contact_Type__c  FROM Contact WHERE ID in :contactIds ]) {
            
            contactAccntMap.put(contact.Id, contact.AccountId);

            // get Application data for which only certain status values are allowed to resume the application
            for(vlocity_ins__Application__c appObj : [select id, Broker__C  from vlocity_ins__Application__c where Broker__C = :contact.Id
                                                      and vlocity_ins__Status__c in ('New', 'In Progress','Manual Review', 'Manual Reviewed')]) 
            {
                // get the Resume link from OmniScript Instance 
                for(vlocity_ins__OmniScriptInstance__C appOmniscript : 
                    [ select id, 
                     vlocity_ins__ResumeLink__C , 
                     vlocity_ins__ObjectName__C, 
                     LastModifiedDate, 
                     vlocity_ins__ObjectId__C, 
                     vlocity_ins__ObjectLabel__C, 
                     vlocity_ins__Status__C 
                     from vlocity_ins__OmniScriptInstance__C  
                     where vlocity_ins__ObjectLabel__c = 'Application'  
                     and vlocity_ins__ObjectId__C = :appObj.Id
                     order by vlocity_ins__ObjectId__C, LastModifiedDate desc limit 1 ]
                   ) 
                {
                   // Map with Key as Contact Id and Resume Link as Value
                    contactAppOmniScriptMap.put(contact.Id, appOmniScript.vlocity_ins__ResumeLink__C);

                    // Map with Key as Contact Id and appOmniscript Id as Value
                    // used as a merge field you want to add in template
                    contactAppOmniScriptIdMap.put(contact.Id,appOmniScript.Id );
                    
                 } // end application to omniscript selection for loop
                
            } // end contact to Application selection for loop
            
            if (contact.Contact_Type__c != null) { 
                contactType = contact.Contact_Type__C;
            }
            else {
                contactType = '';
            }
            if ( (contactType.contains('Sub Producer')) && (contact.AccountId != null)) {
                subProducerAccntIds.add(contact.AccountId);
            }
        } // end for contact iteration for loop
        
        /*
         -- Initially Code was written to send Email to both Principal in case if Manual check is passed for a Sub Producer
         -- Currently commenting this part as  

        // Identify the princial(producer) using the subProducerIds 
        if (subProducerAccntIds.size() > 0 ) {
            // Identify the princial contact under the Account
            for(Account acc:[Select Id, (Select Id, Contact_Type__c From Contacts where Contact_Type__c ='Principal' and Broker_Status__c = 'Active' limit 1) From Account Where Id=:subProducerAccntIds]) {
              	System.debug(' Checking for principal in account Id : ' + acc.id);
                
                if(acc.Contacts != null && acc.Contacts.size() >0) {
                    System.debug('Found Principal Record ' + acc.Contacts);
                    for(Contact contact : acc.Contacts) {
                        // found Princial/Producer Id
                        producerIds.add(contact.id);
                        System.debug(' Principal Record Id ' + contact.id);
                    } // end for loop   
                } 
                else {
                    // No Princial/Producer Record Found
                    System.debug('No Principal Record Found');
                } // end if
            } // end for loop
        } // end if 
        */
        
        // get all the Contact Ids of from Map Key
        Set<Id> brokerIdSet = new Set<Id>();
        brokerIdSet = contactAppOmniScriptMap.keySet();

        // convert Set to List
        List<Id> borkerIdsList = new List<ID>(brokerIdSet);
        
        if (borkerIdsList.size() > 0 ) {
          SendEmailToPrincipal(borkerIdsList, 'Background_Check_Passed_Producer');
        	System.debug(' BrokerId Set ' + borkerIdsList );
        }
        
        /*
        // Send Just a notification Email, ( no application resume needed )
        if (producerIds.size() > 0 ) {
            System.debug('Send Email to Principal Records ' + producerIds);
          SendEmailToPrincipal(producerIds, 'Background_Check_Passed_Producer');
        }
        */
        
    }
    
    /******************************************************************************/
    // Send Email to Producer/Sub Producer to Resume Application using Template
    /******************************************************************************/
    
    public static void SendEmailToPrincipal(List<Id> producerIds, String templateName) {
        
        // get the Template Name
        List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = :templateName];
        
        // List of Contacts to send Resume Link
        List<Contact> contacts = new List<Contact>();
        contacts = [select Id, Email, Contact_Type__c from Contact where id in :producerIds];

        // Initialize Contact Type
        String contactType = '';
        
        // Loop all the Contacts
        for (Contact cnt : contacts) {

            contactType = cnt.Contact_Type__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);  
            mail.setTargetObjectId(cnt.id);// Any contact or User id of your record
            mail.setToAddresses(new list<string>{cnt.Email});
            //  if (contactType.contains('Sub Producer')) {
            // Enter your record Id whose merge field you want to add in template
            mail.setWhatId(contactAppOmniScriptIdMap.get(cnt.id)); 
            //  }

            // Send email to Producer/Sub Producer
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
        } // end for-loop
        
    } // end method-SendEmailToPrincipal
    
} // end class-BackgroundCheckPassed