@isTest
public class NIPR_LogMessagesTest {
    
    static testmethod void executetest()
    {
        String methodName = 'LogMessage';
        Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        inputs.put('LogMessage','trd');
        NIPR_LogMessages nprLog = new NIPR_LogMessages();
        nprLog.invokeMethod(methodName, inputs, output, options);
        nprLog.invokeMethod('LogNIPRTransaction', inputs, output, options);
        
    }
    
}