/****************************************************************************************
 * Name    : NIPRAccountUpdateBatch
  
*/ 
global class NIPRAccountUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        Date executionDate = Date.today().addDays(-15);
        return Database.getQueryLocator(
            [select Id from Account where RecordType.name = 'Ambetter Agency' and NPN__c != '' and 
             (NIPRLastRundate__c <: executionDate or NIPRLastRundate__c = null) and Is_Pilot_Broker__c = true]// AND (LastRunDate__c = LAST_N_DAYS:15 OR LastRunDate__c = null) LIMIT 2]
        );
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts){
      // Id jobId = System.enqueueJob(new NIPRUpdateBatchQueueable(accounts)); 
        for (Account a : accounts) {
        	System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => a.Id},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' =>'AgencyAdditionalInfoUpdate','queueableChainable'=> true}));
        }
    } 
    
    global void finish(Database.BatchableContext bc) {
        
    }
    global void execute(SchedulableContext SC) {
      database.executeBatch(new NIPRAccountUpdateBatch(),1); 
   }
}