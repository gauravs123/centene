/***********************************************************************/
// Called from Process Builder when Application status is in Signed
// and Producer Background Check Status is Passed and Last BG Check is
// done with in an year
// 
// Date | What's modified
// Jan 29, 2020 - PlumTreeGroup - RK - Created new version
// Invoke NIPR Appointment Submission Integraiton Procedure
/***********************************************************************/


public with sharing class NIPRSubmissionInvocable {
    
    public NIPRSubmissionInvocable() {
        
    }
    
    /***********************************************************************/
    // Called from Process Builder when Application status is in Signed
    // and Producer Background Check Status is Passed and Last BG Check is
    // done with in an year
    /***********************************************************************/
    @invocableMethod
    public static void executeIntegrationProcedure(List<Id> appIds) {
        
        System.debug('App Ids ' + appIds);
        
        // process each batch of records
        for(Id appId: appIds ) {
            
            // Agenct Submission
            System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => appId},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' => 'Appointment','queueableChainable'=> true}));
		
            // Agency Submission
            System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => appId},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' => 'AgencyAppointment','queueableChainable'=> true}));
        	
		} 
    }
    // end of executeIntegrationProcedure

} 
// end Class