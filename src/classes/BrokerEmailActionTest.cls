@isTest
public class BrokerEmailActionTest {
    @TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
                       FROM DocuSignSetting__mdt];
        return listDocSign;
    }
    static testMethod void runTest()
    {
        test.startTest();
        getAllMetadata();
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        Set<Id> appIds	= new Set<Id>();
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String UniqueName1 = String.valueOf(randomInt);
        Account acc= new Account();
        acc.Name='test account';
        acc.TaxId__c=UniqueName1+'mac';
        insert acc;
        contact con=new Contact();
        con.lastname='testing con';
        con.AccountId=acc.id;
        con.TaxId__c=UniqueName1+'mco';
        con.Email='test@test.com';
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='mahistandarduser@testorg.com');
        system.runAs(u){
            EmailTemplate e = new EmailTemplate (developerName = 'test', FolderId =UserInfo.getUserId(), TemplateType= 'Text', Name = 'test',Body ='{{CONST_COMPANY}},{{CONST_STATES}}',htmlvalue='{{CONST_COMPANY}},{{CONST_STATES}}'); // plus any other fields that you want to set
            insert e;
            
            String jsonString = '{"brokerApplications":[{"BrokerId":"'+con.Id+'","AgencyName":"newAgency","AgencyPrincipal":"new principal","ContactType":"personcontact"}]}';
            Map<String, Object> mp = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
            //string code='{brokerApplications=({BrokerId='+con.Id+',AgencyName=newAgency,AgencyPrincipal=new principal,ContactType=personcontact})}';
            
            // lstobj.add('{"BrokerId":"'+con.Id+'","AgencyName":"newAgency1","AgencyPrincipal":"new principal1","ContactType":"personcontact1"}');
            //options = (Map<String, Object>) JSON.deserializeUntyped(jsonInput);
            options.put('emailTemplate', e.name);
            options.put('insCompany', 'AP');
            options.put('brokerApplications',mp.get('brokerApplications'));
            options.put('KA', 'KA');
            options.put('ApplicationCarrier','AL-AK,AR-CA,DE-FL');
            options.put('selectedStates','AL;AR');
            List<Object> lstobj=new List<Object>();
            
            
            
            BrokerEmailAction objclass=new BrokerEmailAction();
            objclass.invokeMethod('sendEmailToBrokers',inputs, output, options);
            Boolean status = (Boolean)output.get('status');
            System.assert(true,status == true);
            test.stopTest();
        }
    }
    
}