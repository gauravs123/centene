@isTest
public class ProducerHelperTest {
	static testMethod void editNPNBodyTest() {
    	Map<String,Object> inputMap  	= 	new map<String,Object>(); 
        Map<String,Object> outputMap 	= 	new map<String,Object>(); 
        Map<String,Object> options		=	new map<String,Object>();
        String env = '<soapenv:Envelope>'+
            			'<soapenv:Header>'+
              			'<sch:API-KEY>xxx</sch:API-KEY>'+
            			'</soapenv:Header>'+
            			'<soapenv:Body>'+
              			'<LicensingReport>'+
              			'<sch:maxRecords>100</sch:maxRecords>'+
              			'<sch:sortDirection>Ascending</sch:sortDirection>'+
              			'<sch:keyword></sch:keyword>'+
              			'<sch:searchBy>companyName</sch:searchBy>'+
              			'<sch:returnSearchNavigation>true</sch:returnSearchNavigation>'+
              			'<LicensingReportProcess>CompanyName</LicensingReportProcess>'+
            			'</LicensingReport>'+
              			'</soapenv:Body>'+
             			'</soapenv:Envelope>';    
        options.put('nbnNumber','000099993');
        inputMap.put('npnBodyFEIN',env);
        inputMap.put('npnBodySSN','');
        inputMap.put('npnBodyLicense','');
        ProducerHelper objProHelper = new ProducerHelper();
        objProHelper.invokeMethod('editNPNBody',inputMap,outputMap,options);
        inputMap.put('npnBodyFEIN','');
        inputMap.put('npnBodySSN',env);
        inputMap.put('npnBodyLicense','');
        objProHelper.invokeMethod('editNPNBody',inputMap,outputMap,options);
        inputMap.put('npnBodyFEIN','');
        inputMap.put('npnBodySSN','');
        inputMap.put('npnBodyLicense',env);
        objProHelper.invokeMethod('editNPNBody',inputMap,outputMap,options);
        inputMap.put('LookupType','Agency');
		objProHelper.invokeMethod('setUserPass',inputMap,outputMap,options);
        inputMap.put('pdbReport',env);
        objProHelper.invokeMethod('editAlertReport',inputMap,outputMap,options);
		Date today = Date.Today();
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier').getRecordTypeId();
        RecordType r = [SELECT Id FROM RecordType WHERE Id =:devRecordTypeId LIMIT 1];
        //User u = [SELECT Id,Profile.Name FROM User WHERE email = 'cbradley@plumtreegroup.net' LIMIT 1];
        Account[] accounts = accountEventTest(r);
        Account a = accounts[0];
        Account carrier = accounts[1];
        Contact testCon = new Contact();
        testCon.Account = a;
        testCon.AccountId = a.Id;
        testCon.MailingStreet = '123 Test Street';
        testCon.MailingCity = 'Test City';
        testCon.MailingState = 'TX';
        testCon.MailingPostalCode = '55555';
        testCon.Broker_Lead_Status__c = 'Appointed';
        testCon.Contract_Term__c = date.today();
        testCon.Broker_Status__c = 'Active';
        testCon.Contact_Type__c = 'Principal';
        testCon.Email = '123test@test.com';
        testCon.FirstName = 'testFirst';
        testCon.LastName = 'testLast';
        testCon.Phone = '5555555555';
        testCon.vlocity_ins__ProducerNumber__c = '12345123';
        testCon.Status__c = 'Active';
        insert testCon;
        inputMap.put('ContactRecId',testCon.Id);
        inputMap.put('LicensingReport',new list<object>());
        
        objProHelper.invokeMethod('retrieveProducerInfo',inputMap,outputMap,options);
        vlocity_ins__ContactRegulatoryAction__c objConReg = new vlocity_ins__ContactRegulatoryAction__c();
        objConReg.vlocity_ins__ContactId__c = testCon.Id;
        insert objConReg;
        objProHelper.invokeMethod('retrieveProducerInfo',inputMap,outputMap,options);
        //inputMap.put('npnBodySSN','<soapenv:Envelope>');
        //inputMap.put('npnBodyLicense','<soapenv:Envelope>');
        
    }
    static Account[] accountEventTest(RecordType r) {
        Account[] accounts = new Account[]{};
        
        
        Account testAcc = new Account();
        testAcc.Name = 'testAcc';
        testAcc.Agent_Id__c = '5555555';
        testAcc.BillingStreet = '123 Test Street';
        testAcc.BillingCity = 'TestCity';
        testAcc.BillingState = 'TX';
        testAcc.BillingPostalCode = '55555';
        testAcc.Agent_Master_Id__c = '123456';
        testAcc.TaxId__c = '123456789';
        testAcc.Email__c = 'testEmail@test.com';
        testAcc.vlocity_ins__NPNNumber__c = '5555555';
        testAcc.Phone = '55555555';
        testAcc.vlocity_ins__TaxID__c = '12345';
        
        Account testCarrier = new Account(
            Name = 'TX - Celtic',
            RecordType = r,
            Type = '4 - Corporation',
            DBA_Name__c = 'Celtic',
            License_State__c = 'TX',
            License__c = '4725',
            TaxId__c = '123456789',
            vlocity_ins__TaxID__c = '06-0641618',
            Agent_Master_Id__c = '12345',
            Type_Code__c = '4',
            BillingState = 'TX',
            vlocity_ins__AgencyBrokerageNumber__c = '80799'
        );
        
        insert testCarrier;
        
        insert testAcc;
        //contactEventTest(testAcc, testCarrier, u);
        accounts.add(testAcc);
        accounts.add(testCarrier);
        return accounts;
    }
}