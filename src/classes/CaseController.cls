public with sharing class CaseController {

    @AuraEnabled(cacheable = true)
    public static List<Case> getRelatedCases(String appId){
        return [SELECT Id, Subject, CaseNumber, Status, TypeOrDetailInformation__c 
                FROM Case 
                WHERE Application__c = :appId];
    }
        
}