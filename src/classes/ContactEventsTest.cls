/****************************************************************************************
 * Name    : ContactEventsTest
 * Author  : Developer 
 * Date    : Feb 09, 2019
 * Purpose : ContactEventsTest: Helper class ContactEvents

 * ---------------------------------------------------------------------------
 * MODIFICATION HISTORY:
 * DATE             AUTHOR                              DESCRIPTION
 * ---------------------------------------------------------------------------
 * 09/02/2019       Developer                             Created, added method to publish Contract to Queue
*/
@isTest
public class ContactEventsTest {
    
    @isTest
	public static void testContactEvents(){
		List<vlocity_ins__Application__c> listApp = TestDataFactory.createApplication(1);
        List<Contact> ContactList = TestDataFactory.createContactList(listApp);
        listApp[0].Broker__c = ContactList[0].Id;
        listApp[0].vlocity_ins__Status__c = 'Counter Signed';
        update listApp[0];
        
        vlocity_ins__ProducerLicense__c testLicense = new vlocity_ins__ProducerLicense__c(
            vlocity_ins__EffectiveDate__c = System.today().addDays(0-90),
            Name = 'testLicense',
            vlocity_ins__LicenseType__c = 'Resident Insurance Producer',
            vlocity_ins__ProducerId__c = ContactList[0].Id,
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ExpirationDate__c = System.today().addDays(270),
            Novasys_Primary_Id__c = '12345'
        );
        insert testLicense;
        
        vlocity_ins__ProducerAppointment__c testAppointment = new vlocity_ins__ProducerAppointment__c(
            vlocity_ins__Jurisdiction__c = 'TX',
            vlocity_ins__ProducerId__c = ContactList[0].Id,
            vlocity_ins__CarrierId__c = ContactList[0].AccountId,
            NIPR_Code__c = '102519',
            Contract_Number__c = '00062524',
            vlocity_ins__Status__c = 'Pending',
            Novasys_Primary_Id__c = '12345'
        );
        insert testAppointment;
        
        List<Contract> contractList = TestDataFactory.createContractList(1, ContactList[0]);
        contractList[0].vlocity_ins__ProducerId__c = ContactList[0].Id;
        update contractList[0];
        
        vlocity_ins__ProducerEducation__c testEducation = new vlocity_ins__ProducerEducation__c(
            Name = 'Test Ex Cert',
            vlocity_ins__CertificationNumber__c = '12396',
            vlocity_ins__ProducerId__c = ContactList[0].Id,
            vlocity_ins__Jurisdiction__c = 'TX',
            Effective_Date__c = System.today().addDays(0-180),
            Termination_Date__c = System.today().addDays(180),
            Novasys_Primary_Id__c = '12345'
        );
        
        insert testEducation;
        Test.startTest();
        ContactList[0].Agent_Master_Id__c = '12345678';
        update ContactList[0];
        System.assertNotEquals(ContactList[0].Agent_Master_Id__c, null);
        Test.stopTest();
    }
}