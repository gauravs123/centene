/************************************************************/
/* Test Class for : VerifiedFirstScheduler
/* Modified Date | By  | Changes 
/* Feb 24, 2020  | Plum Tree Group - RK | Initial Version 
/************************************************************/

@isTest
private class VerifiedFirstScheduler_Test {

    static testMethod void myTestMethod() {        
         test.starttest();
         VerifiedFirstScheduler schedulerVerifiedClass = new VerifiedFirstScheduler ();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Test Sched', chron, schedulerVerifiedClass);
         test.stopTest();
    }
}