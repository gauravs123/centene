/****************************************************************************************
 * Name    : NIPRContactUpdateBatch
  
*/ 
global class CancelQueuedJobs implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        
        return Database.getQueryLocator(
            [select id,status,ParentJobId from AsyncApexJob where status = 'Queued' ]// AND (LastRunDate__c = LAST_N_DAYS:15 OR LastRunDate__c = null) LIMIT 2]
        );
    }
    
    global void execute(Database.BatchableContext bc, List<AsyncApexJob> contacts){
        for(AsyncApexJob job : contacts){
            if(!Test.isRunningTest()){
            system.abortJob(job.Id);
            }
        }
        
    } 
    
    global void finish(Database.BatchableContext bc) {
        
    }
}