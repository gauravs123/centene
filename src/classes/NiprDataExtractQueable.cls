public class NiprDataExtractQueable  implements Queueable {
    
    public Map<String,Object> xtj;
    public String producerId;

    public NiprDataExtractQueable(Map<String,Object> xtjMap, String producerId){
        this.xtj 		= xtjMap ;  
        this.producerId = producerId;
    }

    public void execute(QueueableContext qc) {
        
        //*************************************
        // Extract NIPR Appointment Info 
        //*************************************
        NIPRDataExtract.extractNIPRAppointments(xtj, producerId);

        //*************************************
        // Extract NIPR Contact Regulatory Info 
        //*************************************
        NIPRDataExtract.extractContactRegulatoryActions(xtj, producerId);
        
    
    }
    

}