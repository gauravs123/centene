/****************************************************************************************
 * Name    : ContactPlatformEventUpdateTest
 * Author  : Developer 
 * Date    : Feb 09, 2019
 * Purpose : ContactPlatformEventUpdateTest: Helper class ContactEvents

 * ---------------------------------------------------------------------------
 * MODIFICATION HISTORY:
 * DATE             AUTHOR                              DESCRIPTION
 * ---------------------------------------------------------------------------
 * 09/02/2019       Developer                             Created, added method to publish Contract to Queue
*/
@isTest
public class ContactPlatformEventUpdateTest {
    
    @isTest
	public static void testContactPlatformEventUpdate1(){
        List<vlocity_ins__Application__c> listApp = TestDataFactory.createApplication(1);
        List<Contact> ContactList = TestDataFactory.createContactList(listApp);
        listApp[0].Broker__c = ContactList[0].Id;
        listApp[0].vlocity_ins__Status__c = 'Counter Signed';
        update listApp[0];
        
        ContactList[0].Tech_Awaiting_Novasys_Receipt__c  = false;
     	update ContactList;
        	
        
                        ContactList[0].MailingStreet= 'test';
						update ContactList;
        
                        ContactList[0].MailingCity = 'test';
						update ContactList;
        
                        ContactList[0].MailingState = 'test';
						update ContactList;
        
                        ContactList[0].MailingPostalCode = 'test';
					update ContactList;
        
                     
        
                        
    
        
    }
    
    
    @isTest
	public static void testContactPlatformEventUpdate2(){
        List<vlocity_ins__Application__c> listApp = TestDataFactory.createApplication(1);
        List<Contact> ContactList = TestDataFactory.createContactList(listApp);
        listApp[0].Broker__c = ContactList[0].Id;
        listApp[0].vlocity_ins__Status__c = 'Counter Signed';
        update listApp[0];
       
        
        ContactList[0].Tech_Awaiting_Novasys_Receipt__c  = false;
     	update ContactList;
        	
        
                        
        
                        ContactList[0].Fax = 'test';
					update ContactList;
        
                        ContactList[0].FirstName= 'test';
					update ContactList;
                        ContactList[0].LastName = 'test';
						update ContactList;
                        ContactList[0].NPN__c = 'test';
					update ContactList;
                       
    
        
    }
    
    
    @isTest
	public static void testContactPlatformEventUpdate(){
        List<vlocity_ins__Application__c> listApp = TestDataFactory.createApplication(1);
        List<Contact> ContactList = TestDataFactory.createContactList(listApp);
        listApp[0].Broker__c = ContactList[0].Id;
        listApp[0].vlocity_ins__Status__c = 'Counter Signed';
        update listApp[0];
        List<Contract> contractList = TestDataFactory.createContractList(1, ContactList[0]);
        contractList[0].vlocity_ins__ProducerId__c = ContactList[0].Id;
        update contractList[0];
        
        ContactList[0].Agent_Master_Id__c = '12345678';
        update ContactList[0];
        
         
    }

}