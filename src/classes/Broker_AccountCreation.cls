global without sharing class Broker_AccountCreation implements vlocity_ins.VlocityOpenInterface2 {
    
    global Boolean invokeMethod(String methodName, Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options) {
        if (methodName == 'createUserAccount') {
            return createUserAccount(inputs, output, options);
        }
        if (methodName == 'loginUserAccount') {
            return loginUserAccount(inputs, output, options);
        }
        if (methodName == 'verifyExistingUser') {
            return verifyExistingUser(inputs, output, options);
        }
        if(methodName== 'updateGroupInformation'){
            return updateGroupInformation(inputs, output, options);
        }
        if (methodName == 'prepareContractedStates') {
            return prepareContractedStates(inputs, output, options);
        }
        if (methodName == 'checkIfUserIsAlreadyRegistered') {
            return checkIfUserIsAlreadyRegistered(inputs, output, options);
        }
        if (methodName == 'updateApplicationStatus') {
            return updateApplicationStatus(inputs, output, options);
        }
        
        return false;
        
    }
    
    public boolean updateGroupInformation(Map<String,object> inputs, Map < String, Object > output, Map < String, Object > options){
        String conId = (String)((MAp<String,Object>)inputs.get('Contact')).get('Id');
        String accId = (String)inputs.get('AccountId');
        String tin = (String)options.get('TIN');
        if(String.isNotBlank(tin)){
            update new Account(Id = accId, TaxId__c = tin);
        }  
        return true;
    }
    
    public class contractedStates{
        public String State {get;set;}
        public String Carrier {get;set;}    
    }
    
    public boolean prepareContractedStates(Map<String,object> inputs, Map < String, Object > output, Map < String, Object > options) {
        
        String applicationCarrier = (String)options.get('ApplicationCarrier');
        String contactId = (String)options.get('ContactId');
        String accountId = (String)inputs.get('AccountId');
        String applicationId = (String) options.get('ContextId');
        
        List<String> stateCarrierList = applicationCarrier.split(';');
        List<Object> ContractedStatesList = new List<Object>();
        
        List<Object> contractedAgencyStateList = new List<Object>();
        
        
        Map<String,Set<String>> contractedStateMap = new Map<String,Set<String>>(); // Stores State and Carrier Mapping since one state can have multiple carriers.
        
        
        Map<String,Set<String>> stateCarrierMap = new Map<String,Set<String>>(); // Stores State and Carrier Mapping since one state can have multiple carriers.
        
        for (String stateCarrier: stateCarrierList) {
            if(!String.isBlank(stateCarrier)){
                List<String> stateCar = stateCarrier.split('-');
                String state = stateCar[0].trim();
                String carrier = stateCar[1].trim();
                
                if(stateCarrierMap.containsKey(state)){
                    Set<String> carriers = stateCarrierMap.get(state);
                    carriers.add(carrier);
                } else {
                    Set<String> tempCarrier = new Set<String>();
                    tempCarrier.add(carrier);
                    stateCarrierMap.put(state,tempCarrier);
                }
            }
        }
        
        
        Set<String> applicableLicenseTypeStates = new Set<String>();
        Set<String> notApplicableLicenseTypeStates = new Set<String>();
        List<contractedStates> noAppointmentRequiredProducerStates = new List<contractedStates>();
        
        for(vlocity_ins__ProducerLicense__c license : [select id,Applicable_License_Type__c,Jurisdiction__c from vlocity_ins__ProducerLicense__c 
                                                       where vlocity_ins__ProducerId__c != null 
                                                       AND (vlocity_ins__ExpirationDate__c > TODAY OR vlocity_ins__ExpirationDate__c = null)
                                                       AND vlocity_ins__Status__c = 'Active']){
            if(license.Applicable_License_Type__c){
                applicableLicenseTypeStates.add(license.Jurisdiction__c);
            }
            else{
                notApplicableLicenseTypeStates.add(license.Jurisdiction__c);
            }
        }
        
        for (String state : stateCarrierMap.keySet()){
            Set<String> strSet = stateCarrierMap.get(state);
            
            for (String c : strSet) {
                contractedStates contractedStatesObj = new contractedStates ();
                contractedStatesObj.State = State;
                contractedStatesObj.Carrier = c;
                ContractedStatesList.add(contractedStatesObj);
                
                if(!applicableLicenseTypeStates.contains(state) && notApplicableLicenseTypeStates.contains(state)){
                    noAppointmentRequiredProducerStates.add(contractedStatesObj);
                }
            }
            //contractedStatesObj.Carrier = String.join((Iterable<String>)strSet,';');
        }
        
        output.put('ContractedStates',ContractedStatesList);
        output.put('noAppointmentRequiredProducerStates',noAppointmentRequiredProducerStates);
        
        // Get Record Type Id for Carrier
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Carrier').getRecordTypeId();
        
        // pass it to the output at the end
        // List<String> contractedAgencyStateList = new List<String>();
        Set<String> allowedAgencyStates = new Set<String>();
        
        // Get the State Records
        List<Account> accountList = new List<Account>();
        accountList = [ SELECT id, 
                       is_applicable_for_agency__c,
                       recordtypeid,billingstate,
                       dba_name__c,
                       license_state__c,
                       name 
                       FROM 
                       Account 
                       WHERE (Is_Applicable_For_Agency__c = true AND RecordTypeId = :devRecordTypeId)
                      ];
        
        List<contractedStates> noAppointmentRequiredAgencyStates = new List<contractedStates>();
		Set<String> appointmentAllowedAgenciesStates = new Set<String>();
        
        // Loop the data to get the states and match it with Contracted States
        for(Account accnt : accountList) {
            List<String> stateCar = (accnt.Name).split('-');
            String agencyState = stateCar[0].trim();
            String carrier = stateCar[1].trim();
            appointmentAllowedAgenciesStates.add(agencyState);
        }
         
        for (String state : stateCarrierMap.keySet()){
            Set<String> strSet = stateCarrierMap.get(state);
            
            for (String c : strSet) {
                contractedStates contractedStatesObj = new contractedStates ();
                contractedStatesObj.State = State;
                contractedStatesObj.Carrier = c;
                contractedAgencyStateList.add(contractedStatesObj);
                
                if(!appointmentAllowedAgenciesStates.contains(state)){
                    noAppointmentRequiredAgencyStates.add(contractedStatesObj);
                }
            }
            //contractedStatesObj.Carrier = String.join((Iterable<String>)strSet,';');
        }
        
        output.put('ContractedAgencyStates',contractedAgencyStateList);
        output.put('noAppointmentRequiredAgencyStates',noAppointmentRequiredAgencyStates);
        
        return true;
        
    }
    
    /*public boolean loadproducerData(Map<String,object> inputs, Map < String, Object > output, Map < String, Object > options){

Date birthdate = Date.parse((String)inputs.get('birthDate'));
String fNAme= (String)inputs.get('firstName');
String lName= (String)inputs.get('lastName');
String pNumber= (String)inputs.get('producerNumber');

User u =  [SELECT contactId From User where Id = : Userinfo.getUserId()];

System.debug('Contact ID from the query:' +u.contactId +'  ' + Userinfo.getUserId()  );

update new Contact(Id = u.contactId,firstName = fName,lastName = lName, vlocity_ins__ProducerNumber__c = pNumber, Birthdate = birthdate);

output.put('DRId_Contact',u.contactId );
output.put('DRId_ProducerNumber', [SELECT vlocity_ins__ProducerNumber__c from Contact Where Id =:u.contactId LIMIT 1][0].vlocity_ins__ProducerNumber__c);

return true;
} */
    
    public boolean verifyExistingUser(Map<String,object> inputs, Map < String, Object > output, Map < String, Object > options){
        String applicationID = (String)inputs.get('ContextId');
        String userProfile = (String)inputs.get('userProfile');
        
        List<vlocity_ins__Application__c> applications = [SELECT 
                                                          ID,
                                                          LastModifiedDate,
                                                          State_Carrier__c, Broker__r.Id, 
                                                          Broker__r.Email, 
                                                          Broker__r.FirstName, Broker__r.LastName,
                                                          Broker__r.Contact_Type__c,
                                                          Broker__r.AccountId,Broker__r.LastBGCheck__c,
                                                          vlocity_ins__Status__c  
                                                          from 
                                                          vlocity_ins__Application__c 
                                                          where id =:applicationId 
                                                          and 
                                                          vlocity_ins__Status__c in ( 'New', 'In Progress', 'Manual Review', 'Manual Reviewed', 'Signed', 'Submitted', 'Counter Signed', 'Rejected')];
        
        output.put('ValidApplication',false);
        output.put('UserExists',false);
        output.put('ExistingUserHasAccess', false);
        output.put('UserLoggedIn', userProfile != 'Broker Commissions Profile');
        //output.put('MoreBrokerDetailsReqd', true);
        output.put('isApplicationExpired',false);
        
        if(!applications.isEmpty()){
            
            Date lastModifiedDate = date.newinstance(applications[0].LastModifiedDate.year(), applications[0].LastModifiedDate.month(), applications[0].LastModifiedDate.day());
            Integer NumberOfDaysInactive = lastModifiedDate.daysBetween(Date.today());
            
            if (NumberOfDaysInactive > 5 && applications[0].vlocity_ins__Status__c == 'New') {
                output.put('isApplicationExpired',true);

                applications[0].vlocity_ins__Status__c = 'Rejected';
                update applications[0];
            }
            
            output.put('ValidApplication',true);
            output.put('BrokerEmail',applications[0].Broker__r.Email);
            output.put('BrokerFirstName',applications[0].Broker__r.FirstName);
            output.put('BrokerLastName',applications[0].Broker__r.LastName);
            output.put('ApplicationStatus', applications[0].vlocity_ins__Status__c);
            output.put('resumeUrl', System.Label.PortalBaseURL +'brokercommissions/s/broker-details?recordId=' +applicationID);
            
            //List<User> users = [SELECT Id From User Where Email=:applications[0].Broker__r.Email and ContactID != null ];
            List<User> users = [SELECT Id From User Where ContactID=:applications[0].Broker__r.Id];
            
            if(!users.isEmpty()){
                output.put('UserExists',true);
                
                List<vlocity_ins__OmniScriptInstance__c> savedOmniscripts = new List<vlocity_ins__OmniScriptInstance__c>();     
                
                savedOmniscripts = [SELECT Id, vlocity_ins__Status__c FROM vlocity_ins__OmniScriptInstance__c  WHERE vlocity_ins__ObjectId__c =:applicationID ORDER BY vlocity_ins__LastSaved__c DESC];
                
                if (savedOmniscripts != null && savedOmniscripts.size() > 0 && savedOmniscripts[0].vlocity_ins__Status__c != 'Completed') {
                    output.put('resumeUrl', System.Label.PortalBaseURL +'brokercommissions/s/resume-broker-details?actionUrl=/apex/BrokerContract?OmniScriptInstanceId='+savedOmniscripts[0].Id+'&scriptMode=vertical&loadWithPage=false#');
                }
                
                output.put('ExistingUserHasAccess', Userinfo.getUserId() == users[0].Id);
            }
            
            //List<Contract> contracts= [SELECT Id From Contract Where vlocity_ins__ProducerId__c=:applications[0].Broker__r.Id and Contract_Status__c= 'Active'];
            //Integer numOfDays=0;
            //if(applications[0].Broker__r.LastBGCheck__c!=null){
            //    numOfDays = applications[0].Broker__r.LastBGCheck__c.daysBetween(Date.today());
            //}
            
            // Also check if Background verification check was done within an year
            //if (!contracts.isEmpty() && numOfDays < 365){
            //    output.put('MoreBrokerDetailsReqd',false);
            //}
            
            //output.put('ExistingUserHasAccess', Userinfo.getUserName() == (applications[0].Broker__r.Email+'.centene'));
            
            /*
*  If Broker is a Sub Producer verify if associated Producer has License, Active Contract and Active Appointment 
*  in a particular state. Also, making sure Agency has active Contract and Appointment in the same state.                               
*/
            String contactType = applications[0].Broker__r.Contact_Type__c;
            String applicationStateCarrier =  applications[0].State_Carrier__c;
            Set<String> invalidStates = new Set<String>();
            if(contactType=='Sub Producer'){
                List<String> states = new List<String>();
                String accountId = applications[0].Broker__r.AccountId;
                List<Account> accounts = [SELECT ID,IsRegisteredAsAgency__c from Account where ID=:accountId];
                if(!accounts.isEmpty()){
                    Account account = accounts[0];
                    List<Contact> principals = [SELECT ID from Contact where Contact_Type__c='Principal' and AccountId=:accountId];
                    if(!principals.isEmpty()){ 
                        String principalId = principals[0].ID; // Only one principal should be there on each account
                        
                        // Get Agency appointed States
                        List<vlocity_ins__AgencyAppointment__c> agencyAppointments = [SELECT vlocity_ins__Jurisdiction__c, vlocity_ins__Status__c from vlocity_ins__AgencyAppointment__c where vlocity_ins__AgencyBrokerageId__c=:accountId and vlocity_ins__Status__c='Appointed'];
                        Set<String> agencyAppointedStates = new Set<String>();
                        if(!agencyAppointments.isEmpty()){
                            for(vlocity_ins__AgencyAppointment__c agencyAppointment: agencyAppointments){
                                agencyAppointedStates.add(agencyAppointment.vlocity_ins__Jurisdiction__c);
                            }
                        }
                        
                        // Get Agency Contracted States    
                        List<Contract> agencyContracts= [SELECT Id,State__c From Contract Where AccountId=:accountId and Contract_Status__c= 'Active'];
                        Set<String> agencyContractedStates = new Set<String>();
                        if(!agencyContracts.isEmpty()){
                            for(Contract agencyContract: agencyContracts){
                                agencyContractedStates.add(agencyContract.State__c);
                            }
                        }
                        
                        // Get Agency Licensed States    
                        List<vlocity_ins__AgencyLicense__c> agencyLicenses= [SELECT vlocity_ins__Jurisdiction__c, vlocity_ins__Status__c From vlocity_ins__AgencyLicense__c Where vlocity_ins__AgencyBrokerageId__c=:accountId and vlocity_ins__Status__c= 'Active'];
                        Set<String> agencyLicensedStates = new Set<String>();
                        if(!agencyLicenses.isEmpty()){
                            for(vlocity_ins__AgencyLicense__c license: agencyLicenses){
                                agencyLicensedStates.add(license.vlocity_ins__Jurisdiction__c);
                            }
                        }
                        
                        
                        // Get Producer Licensed States
                        List<vlocity_ins__ProducerLicense__c> producerLicenses = [SELECT vlocity_ins__Jurisdiction__c, vlocity_ins__Status__c from vlocity_ins__ProducerLicense__c where vlocity_ins__ProducerId__c=:principalId and vlocity_ins__Status__c='Active'];
                        List<String> producerLicensedStates = new List<String>();
                        if(!producerLicenses.isEmpty()){
                            for(vlocity_ins__ProducerLicense__c producerLicense: producerLicenses){
                                producerLicensedStates.add(producerLicense.vlocity_ins__Jurisdiction__c);
                            }
                        }
                        
                        // Check Producer Appointment States
                        List<vlocity_ins__ProducerAppointment__c> producerAppointments = [SELECT vlocity_ins__Jurisdiction__c from vlocity_ins__ProducerAppointment__c where vlocity_ins__ProducerId__c=:principalId and vlocity_ins__Status__c='Appointed'];
                        List<String> producerAppointedStates = new List<String>();
                        if(!producerAppointments.isEmpty()){
                            for(vlocity_ins__ProducerAppointment__c producerAppointment: producerAppointments){
                                producerAppointedStates.add(producerAppointment.vlocity_ins__Jurisdiction__c);
                            }
                        }
                        
                        // Check Producer Contracted States
                        List<Contract> producerContracts= [SELECT State__c from Contract where vlocity_ins__ProducerId__c=:principalId and Contract_Status__c='Active'];
                        List<String> producerContractedStates = new List<String>();
                        if(!producerContracts.isEmpty()){
                            for(Contract producerContract: producerContracts){
                                producerContractedStates.add(producerContract.State__c);
                            }
                        }
                        
                        // Get application states
                        List<String> stateCarrierList = applicationStateCarrier.split(';');
                        List<String> applicationStates = new List<String>();
                        for (String stateCarrier: stateCarrierList) {
                            List<String> stateCar = stateCarrier.split('-');
                            applicationStates.add(stateCar[0].trim());
                        }
                        
                        for(String st:applicationStates){
                            if(account.IsRegisteredAsAgency__c=='Yes'){
                                if(!(agencyLicensedStates.contains(st) && producerLicensedStates.contains(st) && producerAppointedStates.contains(st) && producerContractedStates.contains(st))){
                                    invalidStates.add(st);
                                }
                            } else {
                                if(!(producerLicensedStates.contains(st) && producerAppointedStates.contains(st) && producerContractedStates.contains(st))){
                                    invalidStates.add(st);
                                }
                            }
                            
                        }
                        
                        if(invalidStates.size()>0){
                            output.put('InvalidStates',invalidStates);
                            output.put('InvalidStateNames',string.join((Iterable<String>)invalidStates,','));
                        }
                    }
                }
            }
        }
        
        
        return true; 
    }
    
    public boolean createUserAccount(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options){
        try{
            String appId = (String)inputs.get('ContextId');
            Map<String,Object> accountMap = (Map<String,Object>)inputs.get('Registration');
            vlocity_ins__Application__c appRecord = [select id,Broker__c from vlocity_ins__Application__c where Id =:appId Limit 1];
            String EmailId = accountMap.get('EmailId') + '';
            Contact con = [SELECT Id, AccountId, Contact_Type__c From Contact Where Id =:appRecord.Broker__c LIMIT 1];
            
            Profile[] profiles = [SELECT Id, Name FROM Profile WHERE Name = 'Agency Sub-Producer' OR Name = 'Agency Principal' ORDER BY Name ASC NULLS LAST];
            
            User u = new User();
            u.lastname =  accountMap.get('LastName') + ' ' ; 
            u.firstname =  accountMap.get('FirstName') + ' ' ;
            u.Username = accountMap.get('EmailId') + '.centene';
            u.Email = accountMap.get('EmailId') + '';
            //u.ProfileId = '00e0P000000S1JsQAK';
            u.ContactId = con.Id;
            
            if (con.Contact_Type__c.equals('Principal')) {
                u.ProfileId = profiles[0].Id;
            } else if (con.Contact_Type__c.equals('Sub Producer')) {
                u.ProfileId = profiles[1].Id;
            }
            
            //String randomToken = String.valueOf(Crypto.getRandomInteger() > 0 ? Crypto.getRandomInteger() : -(Crypto.getRandomInteger())).subString(0,4);
            
            List<User> existingUser = [SELECT Id From User Where UserName =:u.Username ];
            if(!existingUser.isEmpty()) {
                // output.put('error','There is already an account associated with this email address. Please log in or, if you\'ve forgotten the password, use the Forgot Password? link in the Profile menu above.');
                //return false;
                
            }
            String nickName = u.lastname + System.now().getTime();
            u.CommunityNickname  = nickName.length() > 40 ? nickName.substring(nickName.length()-41, nickName.length()-1) : nickName;
            u.TimeZoneSidKey = Label.AH_BrokerDefaultTimeZoneKey;
            String password = accountMap.get('Password') + '';
            String userId = Site.createExternalUser(u, con.AccountId, password);
            
            if (userId != null) {
                createPermissionSetAssignment(userId);
                
                Pagereference pageRef = Site.login(u.Username , password , '/s/broker-details?recordId='+appId);  
                output.put('redirectUrl', pageRef.getUrl());
                
                return true;
            } else {
                output.put('error',
                           'Username should be unique and password should contains Alphanumeric'+ 
                           ' with atleast one uppercase and one special character. '+ '\n' + ' Your Password cannot equal or contain your user name.');
                return false;
            }
            
        }
        catch(Site.ExternalUserCreateException ex) {
            List<String> errors = ex.getDisplayMessages();
            if(ex.getMessage().contains('User already exists') || ex.getMessage().contains('Duplicate Username')){
                output.put('error','There is already an account associated with this email address. Please log in or, if you\'ve forgotten the password, use the Forgot Password? link in the Profile menu above.');
            }else{
                output.put('error',String.join(errors,','));
            }
            System.debug('Ex--->'+ex.getMessage());
            return false;
        } 
        catch(Exception e){
            system.debug('Exception--->'+e + e.getLineNumber());
            output.put('error',e.getMessage());
            return false;
        }
    }
    
    public boolean checkIfUserIsAlreadyRegistered(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options){
        String appId = (String)inputs.get('ContextId');
        Map<String,Object> accountMap = (Map<String,Object>)inputs.get('Registration');
        
        String Username = accountMap.get('EmailId') + '.centene';
        
        List<User> existingUser = [SELECT Id From User Where UserName =:Username ];
        if(!existingUser.isEmpty()) {
            output.put('Error','There is already an account associated with this email address. Please click Continue to log in or reset your password.');
            return false;
        }
        return true;
    }
    
    public boolean updateApplicationStatus(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options){
         String appId = (String)inputs.get('applicationId');
         String status = (String)inputs.get('status');
         String screeningResult = (String)inputs.get('screeningResult');
         
          List<vlocity_ins__Application__c> applications = [SELECT ID, vlocity_ins__Status__c from vlocity_ins__Application__c where id =:appId];
          
          if(!applications.isEmpty()) {
              applications[0].vlocity_ins__Status__c = status;
              applications[0].vlocity_ins__ScreeningResult__c = screeningResult;
              update applications[0];
          }
          
          return true;
    }
    
    public boolean loginUserAccount(Map < String, Object > inputs, Map < String, Object > output, Map < String, Object > options) {
        try{
            String appId = (String)inputs.get('ContextId');
            Map<String,Object> accountMap = (Map<String,Object>)inputs.get('Login');
            
            String username= accountMap.get('LoginUsername') + '';
            String password = accountMap.get('LoginPassword') + '';
            
            Pagereference pageRef = Site.login(username, password, '/s/broker-details?recordId='+appId);  
            
            if (pageRef == null) {
                output.put('error','Invalid Password. Click Go Back and Try Again');
                return false;
            }
            output.put('redirectUrl', pageRef.getUrl());
            
            return true;
        }
        catch(Exception e){
            system.debug('Exception--->'+e + e.getLineNumber());
            output.put('error',e.getMessage());
            return false;
        }
    }
    
    @future
    public static void createPermissionSetAssignment(String userId){
        //PermissionSet perm = [select id,Name from PermissionSet where name = 'BC_CommissionStatementRead' LIMIT 1];
        //PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = perm.id, AssigneeId = userId);
        //insert psa;    
        PackageLicense pkg  = [SELECT Id, NamespacePrefix FROM PackageLicense where NameSpacePRefix = 'vlocity_ins' LIMIT 1];
        insert new UserPackageLicense(UserId = userId, PackageLicenseId=pkg.Id);
    } 
}