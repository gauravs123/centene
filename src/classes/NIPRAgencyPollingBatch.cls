/****************************************************************************************
 * Name    : NIPRProducerPollingBatch

*/ 
global class NIPRAgencyPollingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful,Schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext batchContext) {
        Date startDate = Date.today().addDays(-2);
        Date endDate = Date.today().addDays(-5);
        return Database.getQueryLocator(
            [select id from vlocity_ins__AgencyAppointment__c  where vlocity_ins__Status__c = 'Pending' and CreatedDate <: startDate
            And CreatedDate >: endDate and IsPilotBroker__c = true]// AND (LastRunDate__c = LAST_N_DAYS:15 OR LastRunDate__c = null) LIMIT 2]
        );
    }
    
    global void execute(Database.BatchableContext bc, List<vlocity_ins__AgencyAppointment__c> appointments){
       //Id jobId = System.enqueueJob(new NIPRUpdateBatchQueueable(contacts)); 
       for (vlocity_ins__AgencyAppointment__c app : appointments) {
        	System.enqueueJob(new AsyncIPHandler(new Map<String,Object>{'ContextId' => app.Id},
                                                 new Map<String,Object>{'IPType' => 'NIPR','IPSubType' =>'AgencyPolling','queueableChainable'=> true}));
        }
    } 
    
    global void finish(Database.BatchableContext bc) {
        
    }
    
    global void execute(SchedulableContext SC) {
      database.executeBatch(new NIPRAgencyPollingBatch(),1); 
   }
}