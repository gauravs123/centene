@isTest
public class GenerateContractControllerTest 
{
	@TestVisible 
    static DocuSignSetting__mdt[] getAllMetadata() 
    {
        List<DocuSignSetting__mdt> listDocSign = new List<DocuSignSetting__mdt>();
        listDocSign = [SELECT Id, DeveloperName, MasterLabel, DocuSignValue__c
            FROM DocuSignSetting__mdt];
        return listDocSign;
	}
    static testMethod void TestGenerateContract1()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(200);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        Test.startTest();
        PageReference pageRef = Page.GenerateContract;
		Test.setCurrentPage(pageRef);
        ApexPages.standardSetController sc = new ApexPages.standardSetController(applicationList);
        GenerateContractController controller = new GenerateContractController(sc);
        controller.regenerateContracts();
        GenerateContractController.setDocuSignSettings();
        GenerateContractController.callSignApplications();
        //GenerateContractController.regenerateContracts();
        
        Test.stopTest();
    }
    static testMethod void TestGenerateContract2()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(2);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
        }
        Test.startTest();
        GenerateContractController.updateApplication(appIds);
        Test.stopTest();
    }
    static testMethod void TestGenerateContract3()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        Map<Id,Id> mapDocumentContent;
        Map<Id,Id> parentIdContentDocumentIdMap;
        Id signedDocVersionId;
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(2);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
         }
        List<Contact> contList = TestDataFactory.createContactList(applicationList);
        applicationList[0].Broker__c = contList[0].Id;
        update applicationList;
        Test.startTest();
        PageReference pageRef = Page.GenerateContract;
		Test.setCurrentPage(pageRef);
        ApexPages.standardSetController sc = new ApexPages.standardSetController(applicationList);
        GenerateContractController controller = new GenerateContractController(sc);
        controller.updateApplications();
        GenerateContractController.logMessage('Test Message');
        GenerateContractController.setApplicationId(applicationList);
        GenerateContractController.sendContractEmail(applicationList[0].Id, mapDocumentContent, parentIdContentDocumentIdMap, signedDocVersionId);
        Test.stopTest();
    }
    static testMethod void TestGenerateContract4()
    {
        getAllMetadata();
        Set<Id> appIds	= new Set<Id>();
        Map<Id,Id> mapDocumentContent;
        Map<Id,Id> parentIdContentDocumentIdMap;
        Id signedDocVersionId;
        List<vlocity_ins__Application__c> updateAppList = new List<vlocity_ins__Application__c>();
        List<vlocity_ins__Application__c> applicationList = TestDataFactory.createApplication(2);
        for(vlocity_ins__Application__c appObj : applicationList)
        {
            appIds.add(appObj.Id);
            appObj.Contract_Type__c = 'Principal';
            updateAppList.add(appObj);
         }
        update updateAppList;
        Test.startTest();
        PageReference pageRef = Page.GenerateContract;
		Test.setCurrentPage(pageRef);
        ApexPages.standardSetController sc = new ApexPages.standardSetController(updateAppList);
        GenerateContractController.sendContractEmail(updateAppList[0].Id, mapDocumentContent, parentIdContentDocumentIdMap, signedDocVersionId);
        Test.stopTest();
    }
}