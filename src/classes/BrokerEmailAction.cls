global without sharing class BrokerEmailAction implements vlocity_ins.VlocityOpenInterface2 {

    global Boolean invokeMethod(String methodName, 
                                Map < String, Object > inputs, 
                                Map < String, Object > output, 
                                Map < String, Object > options) {
        if(methodName == 'sendEmailToBrokers'){
            return sendEmailToBrokers(inputs, output, options);
        }
        return false;
    }
    
    public boolean sendEmailToBrokers(Map < String, Object > inputs, 
                                      Map < String, Object > output, 
                                      Map < String, Object > options){                     
        System.Debug('brokerApplications ' + (List<Object>)options.get('brokerApplications'));
        List<Object> selectedBrokersList = (List<Object>)options.get('brokerApplications');
        List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage msg;
        
                                          
        /* Fetching and formatting passed data from options */
        String EmailTemplateName = (String)options.get('emailTemplate');
        String insCompany = (String)options.get('insCompany');
        String selectedStates = (String)options.get('selectedStates');
        // Generate comma separates state names from semicolon seprates state string
        selectedStates = selectedStates.replace(';',', ');
        
      // Make sure you add Ambetter Constracting email address to the OrgWide email address
        // get Orgwide Email Address
      OrgWideEmailAddress[] owea = [select id, DisplayName, Address  from OrgWideEmailAddress where DisplayName like 'Ambetter Contracting'  limit 1];
                                          
                                          
        EmailTemplate emailTemp = [select htmlvalue,body,subject from EmailTemplate where DeveloperName =: EmailTemplateName];
        String emailHTML= emailTemp.htmlvalue;
        String emailBody = emailTemp.body;
        String emailSubject = emailTemp.subject;
        
                                          
        // Replacing common values.
        emailBody = emailBody.replace('{{CONST_COMPANY}}',insCompany).replace('{{CONST_STATES}}',selectedStates);
        emailHTML = emailHTML.replace('{{CONST_COMPANY}}',insCompany).replace('{{CONST_STATES}}',selectedStates);
        
        for(Integer i=0; i<selectedBrokersList.size(); i++){
            
            Map<String,Object> selectedBroker = (Map<String,Object>)selectedBrokersList[i];
            msg = new Messaging.SingleEmailMessage();
			// if Orgwide email is not present then email will be sent with their same user account
			if ( owea.size() > 0 ) {
                System.debug(' Setting Org wide Email Address '+ owea.get(0).Id);
    			msg.setOrgWideEmailAddressId(owea.get(0).Id);
			}
            
            //
            msg.setTargetObjectId((Id)selectedBroker.get('BrokerId'));
            String emhtml,embody = '';
            // Replacing record specific values in the content

            String regUrl = System.Label.PortalBaseURL + 'brokercommissions/s/register-broker?recordId=' + (String) selectedBroker.get('ApplicationId');
            emhtml = emailHTML.replace('{{CONST_INS_COMPANY}}',(String) selectedBroker.get('AgencyName')).replace('{{CONST_PRINCIPAL}}',(String) selectedBroker.get('AgencyPrincipal')).replace('{{CONST_REG_URL}}',regURL).replace('{{CONST_CONTRACT}}',(String) selectedBroker.get('ContactType')).replace('{{BASE_URL}}',System.Label.PortalBaseURL);
            embody = emailBody.replace('{{CONST_INS_COMPANY}}',(String) selectedBroker.get('AgencyName')).replace('{{CONST_PRINCIPAL}}',(String) selectedBroker.get('AgencyPrincipal')).replace('{{CONST_CONTRACT}}',(String) selectedBroker.get('ContactType')).replace('{{BASE_URL}}',System.Label.PortalBaseURL);
            msg.setHtmlBody(emhtml);
            msg.setPlainTextBody(embody);
            msg.subject = emailSubject;
            lstMsgsToSend.add(msg);
        }
        System.Debug('Line 56');
        if(lstMsgsToSend != null && lstMsgsToSend.size() > 0 && !Test.isRunningTest()){
            Messaging.sendEmail(lstMsgsToSend);
            output.put('status',true);
            output.put('total',lstMsgsToSend.size());
            return true;
        }
        return false;
    }
}