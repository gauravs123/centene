public class NIPRBatchHandler {
    public static void NIPRBatchHandler() {
        /* Initialize variables */
        String procedureName = 'NIPRProducerBatchLookupReport';
        Map <String, Object> ipInput = new Map <String, Object> ();
        Map <String, Object> ipOutput = new Map <String, Object> ();
        Map <String, Object> ipOptions = new Map <String, Object> ();
        
        /* Populating input map for an Integration Procedure. 
        Follow whatever structure your VIP expects */
        String orderId = '80100000000abcd';
        ipInput.put('orderId', orderId);
        
        /* Call the IP via runIntegrationService, 
        and save the output to ipOutput */
        ipOutput = (Map <String, Object>) vlocity_ins.IntegrationProcedureService.runIntegrationService(procedureName, ipInput, ipOptions);
        
        System.debug('IP Output: ' + ipOutput);
    }
}