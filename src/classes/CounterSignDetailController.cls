public with sharing class CounterSignDetailController {
    
    private final vlocity_ins__Application__c application;
    public List<vlocity_ins__Application__c> applicationData{get;set;}
    
    public CounterSignDetailController(ApexPages.StandardController standardController){
        application = (vlocity_ins__Application__c)standardController.getRecord();
        applicationData = new List<vlocity_ins__Application__c>();
        List<vlocity_ins__Application__c>  applicationList = new List<vlocity_ins__Application__c>([SELECT Id,Name,vlocity_ins__Status__c,Broker__c,    CreatedById,CreatedDate
                           FROM vlocity_ins__Application__c
                           WHERE Id = :application.Id and Broker__r.BGCheck_Status__c = 'Passed']);
        list<Id> listbrokerId = new list<Id>();
        for(vlocity_ins__Application__c app: applicationList ){
            if(app.Broker__c != null){
                listbrokerId.add(app.Broker__c);
            }
        }
        map<Id,list<vlocity_ins__ProducerAppointment__c>> mapContactToAppoinment  = new  map<Id,list<vlocity_ins__ProducerAppointment__c>>();
        if(listbrokerId.size()>0){
            for(vlocity_ins__ProducerAppointment__c appoinment : [SELECT ID,vlocity_ins__Status__c,vlocity_ins__ProducerId__c From vlocity_ins__ProducerAppointment__c 
                                                                  WHERE vlocity_ins__ProducerId__c IN: listbrokerId
                                                                  and Contract__c != null]){
               if(!mapContactToAppoinment.containsKey(appoinment.vlocity_ins__ProducerId__c)){
               
                    mapContactToAppoinment.put(appoinment.vlocity_ins__ProducerId__c,new list<vlocity_ins__ProducerAppointment__c>());
               }
               mapContactToAppoinment.get(appoinment.vlocity_ins__ProducerId__c).add(appoinment);
            }
        }
        Set<Id> appointedBroker = new Set<Id>();
        for(Id contactId : mapContactToAppoinment.keyset()){
            for(vlocity_ins__ProducerAppointment__c appoinment : mapContactToAppoinment.get(contactId)){
                if(appoinment.vlocity_ins__Status__c != 'Appointed'){
                    appointedBroker.remove(contactId);
                    break;
                }
                else{
                    appointedBroker.add(contactId);
                }
            }
        }
        for(vlocity_ins__Application__c app :applicationList){
            if(appointedBroker.contains(app.Broker__c)){
                applicationData.add(app);
            }
        }
    }
    
    public PageReference signApplications() { 

        CounterSignatureActionController.setApplicationId(applicationData);
        CounterSignatureActionController.callSignApplications();
        
        
        // PageReference pageRef = new PageReference('lightning/r/vlocity_ins__Application__c/'+application.Id+'/view');
        PageReference pageRef = new PageReference('/'+application.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference backToApplication() { 

        // PageReference pageRef = new PageReference('lightning/r/vlocity_ins__Application__c/'+application.Id+'/view');
        PageReference pageRef = new PageReference('/'+application.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}