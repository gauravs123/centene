@isTest(seeAllData=false)
public class NIPRHttpCalloutAgencyTest 
{
    static testMethod void testCllout()
    {
        Id accountAgencyRT = Schema.SObjectType.Account.RecordTypeInfosByName.get('Agency').RecordTypeId;
        Id contactAgentRT = Schema.SObjectType.Contact.RecordTypeInfosByName.get('Agent').RecordTypeId;        
        Account agency = new Account();
        agency.Name = 'Agency Test';
        agency.TaxId__c = 'AGENCY';
        agency.BillingCity = 'Chandler';
        agency.BillingState = 'AZ';
        agency.RecordTypeId = accountAgencyRT;
        insert agency;
        
        Contact agent = new Contact();
        agent.FirstName = 'John';
        agent.LastName = 'Adams';
        agent.TaxId__c = 'ABCDEFGHI';
        agent.MailingCity = 'Tempe';
        agent.MailingState = 'AZ';
        agent.MailingStreet = '20 S Mill Ave' ;
        agent.MailingPostalCode = '85251';
        agent.Phone = '3334445555';
        agent.RecordTypeId = contactAgentRT;
        agent.AccountId = agency.Id;
        agent.ContractType__c = 'Agency Authorized/Affiliated agent';
        insert agent;
        // Updated By Naresh K Shiwani
        String methodName = 'NIPRCalloutAgency';
        Map < String, Object > inputs = new Map<String, Object>(); 
        Map < String, Object > output = new Map<String, Object>(); 
        Map < String, Object > options = new Map<String, Object>();
        // Set mock callout class 
        Test.startTest();
        Contact con = [select id from Contact Limit 1];
        System.debug('*****con*****'+con);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        NIPRHttpCalloutAgency niprAgencyCallout = new NIPRHttpCalloutAgency();
        inputs.put('AccountId',agent.AccountId);
        inputs.put('npnNumber','NIPR123');
        Boolean isInvoke = niprAgencyCallout.invokeMethod(methodName, inputs, output, options);
        //Boolean resp = NIPRHttpCalloutAgency.NIPRCalloutAgency(CON.Id, 'NIPR123', output);
        Test.stopTest();
        
    }
}