@isTest(seeAllData=false)
public class CarrierDynamicSignatureTest 
{
	static testMethod void testCarrierDynamicSign()
    {
        List<vlocity_ins__Application__C> appList = TestDataFactory.createApplication(20);
        CarrierDynamicSignature.getStateList();
        CarrierDynamicSignature.getStateCarrierList();
        CarrierDynamicSignature.getStateCarrierDetails();
        PageReference pageRef = Page.CarrierDynamicSignaturePDF;
		Test.setCurrentPage(pageRef);
        CarrierDynamicSignature.generatePDF();
        List<vlocity_ins__Application__C> listApp = [SELECT ID, State_Carrier__c FROM vlocity_ins__Application__C];
        // Put Id into the current page Parameters
		ApexPages.currentPage().getParameters().put('appId',listApp[0].Id);
        String id = ApexPages.currentPage().getParameters().get('appId');
        CarrierDynamicSignature carrierDynObj = new CarrierDynamicSignature();
        carrierDynObj.getCarrierSignature();
        
    }
}