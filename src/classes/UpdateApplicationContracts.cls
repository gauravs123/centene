/***********************************************************************/
// Called from Process Builder when Application status is in Signed
// and Producer Background Check Status is Passed and Last BG Check is
// done with in an year
// 
// Date | What's modified
// Feb 20, 2020 - PlumTreeGroup - RK - Created new version
// Update Application Contracts when Application status changes to counter sign
/***********************************************************************/


public with sharing class UpdateApplicationContracts {
    
    public UpdateApplicationContracts() {
        
    }
    
    /***********************************************************************/
    // Called from Process Builder when Application status changed to 
    // CounterSign, change Contracts status to Active when Appointment
    // status changes to 'Appointmented
    /***********************************************************************/
    @invocableMethod
    public static void updateContracts(List<Id> brokerIds) {
        
        // to hold contract Id that are in Pending status
        List<Id> contractIds = new List<Id>();
        
        List<vlocity_ins__ProducerAppointment__c> producerAppointments = new List<vlocity_ins__ProducerAppointment__c>();
        
        // get the list of Appointments for the broker Ids 
        producerAppointments = [ SELECT id, 
                                contract__r.Contract_Status__c , 
                                Contract__c  
                                FROM 	vlocity_ins__ProducerAppointment__c 
                                WHERE 	(contract__r.Contract_Status__c = 'Pending' OR  contract__r.Contract_Status__c = '')
                                AND 	vlocity_ins__Status__c = 'Appointed'  
                                AND		vlocity_ins__ProducerId__c in :brokerIds 
                               ];
        
        // List of Contract in Pending status
        List<Contract> contracts = new List<Contract>();
        
        // to update Contracts to active
        List<Contract> updateContracts = new List<Contract>();
        
        // get all the Contract Ids
        for (vlocity_ins__ProducerAppointment__c producerAppointment : producerAppointments ) {
            contractIds.add(producerAppointment.Contract__C);
        }
        
        // get all the contract Objects
        contracts = [ SELECT Contract_Status__c,
                     StartDate,
                     vlocity_ins__TerminateDate__c
                     FROM   Contract 
                     WHERE  Id in :contractIds 
                    ];
        
		// business identified Term Date
        Date termDate = Date.newInstance(2999, 12, 31);
        
        // loop all the pending contracts and make them active 
        for(Contract contract : contracts)  {
            contract.Contract_Status__c = 'Active';
            contract.StartDate = Date.today();
            //contract.ContractTerm = contract.StartDate.monthsBetween(termDate);
            contract.vlocity_ins__TerminateDate__c = termDate;    
            updateContracts.add(contract);
        }       
        
        // Update the Contracts
        if (updateContracts.size() >0 ) {
            update updateContracts;
        }
    }
    // end of updateContracts
    // 
} 
// end Class