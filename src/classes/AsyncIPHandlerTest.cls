@isTest
public class AsyncIPHandlerTest{
    
    static void setupIntegrationProcedureComplex()
    {
        Account parent = new Account(Name='AccName', Description='Test Account');
        insert parent;
        
        insert new Contact(FirstName='First', LastName='last', Email='test@last.com');
        
        Integer drMapNumber = 0;
        
        List<vlocity_ins__DRMapItem__c> mappings = new List<vlocity_ins__DRMapItem__c> 
        {
            
            new vlocity_ins__DRMapItem__c(
                Name='Extract',
                vlocity_ins__MapId__c ='xx'+(drMapNumber++),
                vlocity_ins__DomainObjectCreationOrder__c = 1, 
                vlocity_ins__InterfaceFieldAPIName__c = 'Account:Name', 
                vlocity_ins__DomainObjectAPIName__c = 'JSON', 
                vlocity_ins__DomainObjectFieldAPIName__c = 'Account:Name'
            ),
                new vlocity_ins__DRMapItem__c(
                    Name='Extract',
                    vlocity_ins__MapId__c ='xx'+(drMapNumber++),
                    vlocity_ins__DomainObjectCreationOrder__c = 1, 
                    vlocity_ins__InterfaceFieldAPIName__c = 'Account:Description', 
                    vlocity_ins__DomainObjectAPIName__c = 'JSON', 
                    vlocity_ins__DomainObjectFieldAPIName__c = 'Account:Description'
                ),
                new vlocity_ins__DRMapItem__c(
                    Name='Extract',
                    vlocity_ins__MapId__c ='xx'+(drMapNumber++),
                    vlocity_ins__DomainObjectCreationOrder__c = 1, 
                    vlocity_ins__InterfaceFieldAPIName__c = 'Account:Id', 
                    vlocity_ins__DomainObjectAPIName__c = 'JSON', 
                    vlocity_ins__DomainObjectFieldAPIName__c = 'Account:Id'
                )
                };
                    
                    insert mappings;
        
        vlocity_ins__OmniScript__c proc  = new vlocity_ins__OmniScript__c(Name='Test', vlocity_ins__Type__c='Procedure',vlocity_ins__IsActive__c=true, vlocity_ins__ProcedureKey__c='Procedure_Procedure', vlocity_ins__SubType__c='Procedure', vlocity_ins__IsProcedure__c=true,vlocity_ins__PropertySet__c='{"trackingCustomData":{"onething":"anotherThing"}}');
        insert proc;
        
        vlocity_ins__OmniScriptDefinition__c OmniDef = new vlocity_ins__OmniScriptDefinition__c(vlocity_ins__Sequence__c=0,vlocity_ins__Content__c=
                                                                                                '{"propSetMap":{"chainableSoslQueriesLimit":null,"chainableQueryRowsLimit":null,"chainableDMLRowsLimit":null,"chainableHeapSizeLimit":null,"chainableCpuLimit":2000,"chainableDMLStatementsLimit":null,"chainableQueriesLimit":50,"rollbackOnError":false,"nameColumn":"","description":"","labelPlural":"","labelSingular":"","relationshipFieldsMap":[],"columnsPropertyMap":[],"includeAllActionsInResponse":false,"trackingCustomData":{},"linkToExternalObject":""},"RPBundle":"","response":null,"prefillJSON":"{}","bReusable":false,"bpVersion":3,"bpType":"Procedure","bpSubType":"Procedure","bpLang":null,"bHasAttachment":false,"children":[{"type":"DataRaptor Extract Action","propSetMap":{"chainOnStep":false,"label":"GetSomethingFromExtract","bundle":"Extract","dataRaptor Input Parameters":[{"inputParam":"SomethingContextId","element":"ContextId"},{"inputParam":"constant","element":"contantValue"}],"executionConditionalFormula":null,"responseJSONPath":"","responseJSONNode":"GetSomethingFromExtract","sendJSONPath":"","sendJSONNode":"","failureConditionalFormula":"","failOnStepError":true},"offSet":0,"name":"GetSomethingFromExtractJSON","level":0,"indexInParent":0,"bHasAttachment":false,"bEmbed":false},{"type":"DataRaptor Extract Action","propSetMap":{"chainOnStep":true,"label":"GetSomethingFromExtract","bundle":"Extract","dataRaptor Input Parameters":[{"inputParam":"SomethingContextId","element":"ContextId"},{"inputParam":"constant","element":"contantValue"}],"executionConditionalFormula":null,"responseJSONPath":"","responseJSONNode":"GetSomethingFromExtract","sendJSONPath":"","sendJSONNode":"","failureConditionalFormula":"","failOnStepError":true},"offSet":0,"name":"GetSomethingFromExtractJSON","level":0,"indexInParent":0,"bHasAttachment":false,"bEmbed":false},{"Name":"Responsy","Type":"Response Action","Active":true,"propSetMap":{"label":"Responsdy","chainOnStep":false,"returnFullDataJSON":true,"responseFormat":"JSON","responseDefaultData":{"RESPONSE":"FAILURE"},"executionConditionalFormula":null,"responseJSONPath":"","responseJSONNode":"","sendJSONPath":"","sendJSONNode":"","failureConditionalFormula":"","failOnStepError":true,"integrationProcedureKey":"test"}}]}',
                                                                                                vlocity_ins__OmniScriptId__c=proc.id);
        insert OmniDef ;
        
    }
    
    
    
    static testMethod
        void testIntegrationProcedureService()
    {
        setupIntegrationProcedureComplex();
        Test.startTest();
        AsyncIPHandler VIPHandler = new AsyncIPHandler(new Map<String, Object>{'ContextId' => [ Select Id from Account ].Id }, 
                                                       new map<string,object>{'IPType'=>'Procedure','IPSubType'=>'Procedure','chainable'=>'true'});
        System.enqueueJob(VIPHandler);
        Test.stopTest();      
    }
}