/****************************************************************************************
* Name    : Validation Rule
* Author  : Developer 
* Date    : Dec 17th, 2019
* Purpose : ContactTrigger Validation Rule

* ---------------------------------------------------------------------------
* MODIFICATION HISTORY:
* DATE             AUTHOR                              DESCRIPTION
* ---------------------------------------------------------------------------
* 12/07/2019       Rangesh Kona                        Created
*/

//Trigger Deactivated to debug batch nipr process
trigger ContactValidationTrigger on Contact (before insert, before update) {
    if (!Trigger_Utility.isTriggerDisabled()) {    
        Boolean insertExecuted = false;
        
        if (trigger.isBefore) {
            if(trigger.isInsert ) {
                System.debug('Executing Insert Operation');
                insertExecuted = true;
                ContactTriggerProcessHandler.checkContactInsretValidation(Trigger.new);
            }
            if(trigger.isUpdate && insertExecuted == false) {     
                System.debug('Executing Update Operation' + insertExecuted);
                ContactTriggerProcessHandler.checkContactValidation(Trigger.newMap, Trigger.old);
            }      
        }
     
    
    }
}