trigger ContactPlatformEventUpdate on Contact (before update) {
    System.debug('Contact Platform Event Update Trigger Fired');
    if (!Trigger_Utility.isTriggerDisabled()) {    
        Id[] accountIds = new Id[]{};
        Map<Id, Account> accountsById = new Map<Id, Account>();
        Contact[] contacts = (contact[]) Trigger_Utility.filterList(trigger.new,'Is_Pilot_Broker__c', false);
        Map<Id, Contact> oldContacts = trigger.oldMap;
        Set<Id> brokerIds = new Set<Id>();
        Date earliestContractDate;
        Contact[] updatedContacts = new Contact[]{};
        Update_Centene_Contact__e[] contactEvents = new Update_Centene_Contact__e[]{};
        Map<Id, vlocity_ins__Application__c> appsByContact = new Map<Id, vlocity_ins__Application__c>();
        
        for (Contact con : contacts) {
            brokerIds.add(con.Id);
            accountIds.add(con.AccountId);
        }
        
        vlocity_ins__Application__c[] apps = [SELECT Id, vlocity_ins__Status__c, Broker__c, Broker__r.Id FROM vlocity_ins__Application__c WHERE Broker__r.Id IN: brokerIds];
        for (vlocity_ins__Application__c a : apps) {
            appsByContact.put(a.Broker__r.Id, a);
        }
        
        Contract[] contracts = [SELECT Id, StartDate, Contract_Status__c FROM Contract WHERE vlocity_ins__ProducerId__c IN: brokerIds AND Contract_Status__c = 'Active'];
        for (Contract c : contracts) {
            if (c.StartDate < earliestContractDate || earliestContractDate == null) {
                earliestContractDate = c.StartDate;
            }
        }
        
        Account[] accounts = [SELECT Id, Agent_Master_Id__c, Agent_Id__c, Name FROM Account WHERE Id IN: accountIds];
        System.debug(accounts);
        for (Account a : accounts) {
            accountsById.put(a.Id, a);
        }
        
        for (Contact c : contacts) {
            if(c.Agent_Master_Id__c != null && c.Agent_Id__c != null) {
                Contact oldC = oldContacts.get(c.Id);
                if (c.AccountId != oldC.AccountId) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.Last_W9_Date__c != oldC.Last_W9_Date__c) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.vlocity_ins__TaxId__c != oldC.vlocity_ins__TaxId__c) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.MailingStreet != oldC.MailingStreet) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.MailingCity != oldC.MailingCity) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.MailingState != oldC.MailingState) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.MailingPostalCode != oldC.MailingPostalCode) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.Email != oldC.Email) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.Fax != oldC.Fax) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.FirstName != oldC.FirstName) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.LastName != oldC.LastName) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.NPN__c != oldC.NPN__c) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.Pay_to_Agent_Id__c != oldC.Pay_to_Agent_Id__c) {
                    System.debug('pay to id changed');
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.Phone != oldC.Phone) {
                    System.debug('Phone number changed');
                    //c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                } else if (c.BGCheck_Status__c != oldC.BGCheck_Status__c) {
                    c.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedContacts.add(c);
                }
            }
        }
        
        for(Contact c : updatedContacts) {
            Account acc = accountsById.get(c.AccountId);
            Contact oldC = oldContacts.get(c.Id);
            System.debug(c.Tech_Awaiting_Novasys_Receipt__c);
            vlocity_ins__Application__c app = appsByContact.get(c.Id);
            System.debug(app);
            System.debug(c.Tech_Awaiting_Novasys_Receipt__c);
            if (c.Tech_Awaiting_Novasys_Receipt__c == true) {
                System.debug('Contact Event being Generated');
                System.debug(acc);
                String otherPostal;
                if(String.isNotBlank(c.OtherPostalCode) && c.OtherPostalCode.length() > 5) {
                    otherPostal = c.OtherPostalCode.split('-')[0];
                } else {
                    otherPostal = c.OtherPostalCode;
                }
                
                String postal;
                if (String.isNotBlank(c.MailingPostalCode) && c.MailingPostalCode.length() > 5) {
                    postal = c.MailingPostalCode.split('-')[0];
                } else {
                    postal = c.MailingPostalCode;
                }
                Update_Centene_Contact__e contactEvent = new Update_Centene_Contact__e();
                if (acc != null) {
                    contactEvent.Account_Agent_Master_Id__c = acc.Agent_Master_Id__c;
                    contactEvent.Account_Agent_Id__c = acc.Agent_Id__c;
                    contactEvent.Account_Name__c = acc.Name;
                }
                if (c.AccountId != null) {
                    contactEvent.Account_Id__c = c.AccountId;
                }
                contactEvent.Agent_Master_Id__c = c.Agent_Master_Id__c;
                contactEvent.Agent_Id__c = c.Agent_Id__c;
                contactEvent.Background_Status__c= c.BGCheck_Status__c;
                contactEvent.BGCheck_Date__c = c.LastBGCheck__c;
                contactEvent.Billing_Address_Line_1__c = c.OtherStreet;
                contactEvent.Billing_City__c = c.OtherCity;
                contactEvent.Billing_State__c = c.OtherState;
                contactEvent.Billing_Zip__c = otherPostal;
                contactEvent.Date_of_Birth__c = c.BirthDate;
                contactEvent.Broker_Lead_Status__c = c.Broker_Lead_Status__c;
                contactEvent.Contact_Type__c = c.Contact_Type__c;
                contactEvent.Contract_Status__c = c.Broker_Status__c;
                contactEvent.Contract_Term__c = c.Contract_Term__c;
                contactEvent.Email__c = c.Email;
                contactEvent.Effective_Date__c = earliestContractDate;
                contactEvent.Fax__c = c.Fax;
                contactEvent.FirstName__c = c.FirstName;
                contactEvent.MiddleName__c = c.Middle_Name_Initial__c;
                contactEvent.FFM_Id__c = c.FFM_Id__c;
                contactEvent.LastName__c = c.LastName;
                contactEvent.NPN__c = c.NPN__c;
                contactEvent.Pay_to_Agent__c = c.Pay_to_Agent_Id__c;
                contactEvent.Phone__c = c.Phone;
                contactEvent.Producer_Number__c = c.vlocity_ins__ProducerNumber__c;
                contactEvent.Resident_Address_Line_1__c = c.MailingStreet;
                contactEvent.Resident_City__c = c.MailingCity;
                contactEvent.Resident_State__c = c.MailingState;
                contactEvent.Resident_Zipcode__c = postal;
                contactEvent.SalesforceId__c = c.Id;
                contactEvent.Status__c = c.Status__c;
                contactEvent.SSN__c = c.Tax_Id__c;
                contactEvent.Tech_Awaiting_Novasys_Receipt__c = c.Tech_Awaiting_Novasys_Receipt__c;
                if(c.Last_W9_Date__c != null) {
                    contactEvent.w9_received__c = true;
                } else {
                    contactEvent.w9_received__c = true;
                }
                
                if(c.Pay_to_Agent_Id__c != oldC.Pay_to_Agent_Id__c) {
                    contactEvent.PayToChanged__c = true;
                } else {
                    contactEvent.PayToChanged__c = false;
                }
                        
                contactEvents.add(contactEvent);
            }
        }
        if(!contactEvents.isEmpty()) {                       
            System.debug('Contact Events being created');
            Database.SaveResult[] saveResult = EventBus.publish(contactEvents);
            System.debug(saveResult);
        }
    }
}