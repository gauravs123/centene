/****************************************************************************************
 * Name    : ContactEvents
 * Author  : Developer 
 * Date    : Sep 26, 2019
 * Purpose : ContactEvents Trigger: Contact Trigger Events

 * ---------------------------------------------------------------------------
 * MODIFICATION HISTORY:
 * DATE             AUTHOR                              DESCRIPTION
 * ---------------------------------------------------------------------------
 * 09/26/2019       Developer                             Created
*/
trigger ContactEvents on Contact (after update) {
    System.debug('ContactEvents trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        if (Trigger.isUpdate && !ContactEventHandler.isfired) {
            list<Contact> filterList = Trigger_Utility.filterList(Trigger.new,'Is_Pilot_Broker__c');
            map<Id,Contact> filteredMap = new map<Id,Contact>();
            for(Contact con: filterList){
                filteredMap.put(con.Id,con);
            }    
            ContactEventHandler.afterUpdate(filterList,Trigger.oldMap,filteredMap);
            
        }
    }
}