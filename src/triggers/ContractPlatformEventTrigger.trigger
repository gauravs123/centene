trigger ContractPlatformEventTrigger on Contract (after insert, after update) {
    System.debug('contract platform event trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {    
        List<Contract_Event__e> contractEvents = new List<Contract_Event__e>();
        Contract[] contracts = new Contract[]{};
        Map<Id, Contract> oldContracts = trigger.oldMap;
        list<Contract> listNewContract = [SELECT Id, Tech_Awaiting_Novasys_Receipt__c, Novasys_Primary_Id__c, 
                                  vlocity_ins__TerminateDate__c, vlocity_ins__ProducerId__r.Agent_Master_Id__c, 
                                  vlocity_ins__ProducerId__c, Carrier_lookup__r.Name, Carrier_lookup__r.Id,
                                  Carrier_Lookup__r.Novasys_DBA_Name__c, Contract_Status__c, 
                                  vlocity_ins__ContractType__c, StartDate, State__c,IsPilotBroker__c
                                  FROM Contract
                                  WHERE Id IN :Trigger.new
                                 ];
        list<Contract> listfilteredContract = (list<Contract> )Trigger_Utility.filterList(listNewContract,'IsPilotBroker__c');
        
        for (Contract contract : listfilteredContract) {
            if(Trigger.isInsert) {
                if (contract.vlocity_ins__ProducerId__r.Agent_Master_Id__c != null && !String.isBlank(contract.Carrier_lookup__r.Novasys_DBA_Name__c)) {
                    Contract_Event__e contractEvent = new Contract_Event__e(
                        Agent_Master_Id__c = contract.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                        Broker_Id__c = contract.vlocity_ins__ProducerId__c,
                        Carrier__c = contract.Carrier_lookup__r.Name,
                        Carrier_Id__c = contract.Carrier_lookup__r.Id,
                        Carrier_DBA_Name__c = contract.Carrier_lookup__r.Novasys_DBA_Name__c,
                        Contract_Status__c = contract.Contract_Status__c,
                        Contract_Type__c = contract.vlocity_ins__ContractType__c,
                        Effective_Date__c = contract.StartDate,
                        TerminationDate__c= contract.vlocity_ins__TerminateDate__c,
                        SalesforceId__c = contract.Id,
                        State__c = contract.State__c,
                        Tech_Awaiting_Novasys_Receipt__c = contract.Tech_Awaiting_Novasys_Receipt__c
                    );
                    if (contract.Contract_Status__c == 'Active') {
                        contractEvent.Active__c = True;
                    } else {
                        contractEvent.Active__c = False;
                    } 
                    contractEvents.add(contractEvent);
                }
            } else if (Trigger.isUpdate) {
                Contract oldContract = oldContracts.get(contract.Id);
                if (contract.vlocity_ins__ContractType__c != oldContract.vlocity_ins__ContractType__c) {
                    System.debug('contractType changed');
                    contract.Tech_Awaiting_Novasys_Receipt__c = true;
                    contracts.add(contract);
                } else if (contract.Contract_Status__c != oldContract.Contract_Status__c) {
                    System.debug('contract status changed');
                    contract.Tech_Awaiting_Novasys_Receipt__c = true;
                    contracts.add(contract);
                } else if (contract.Carrier_Lookup__c != oldContract.Carrier_Lookup__c) {
                    System.debug('dba name changed');
                    System.debug(contract.Carrier_Lookup__r.Novasys_DBA_Name__c);
                    System.debug(oldContract.Carrier_Lookup__r.Novasys_DBA_Name__c);
                    contract.Tech_Awaiting_Novasys_Receipt__c = true;
                    contracts.add(contract);
                } else if (contract.StartDate != oldContract.StartDate) {
                    System.debug('start date changed');
                    contract.Tech_Awaiting_Novasys_Receipt__c = true;
                    contracts.add(contract);
                } else if (contract.vlocity_ins__TerminateDate__c != oldContract.vlocity_ins__TerminateDate__c) {
                    System.debug('terminate date changed');
                    contract.Tech_Awaiting_Novasys_Receipt__c = true;
                    contracts.add(contract);
                }
            }
        }
        
        for (Contract c : contracts) {
            Contract oldContract = oldContracts.get(c.Id);
            if (!String.isBlank(c.Carrier_lookup__r.Novasys_DBA_Name__c)) {    
                Contract_Event__e contractEvent = new Contract_Event__e(
                    Agent_Master_Id__c = c.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                    Broker_Id__c = c.vlocity_ins__ProducerId__c,
                    Carrier__c = c.Carrier_lookup__r.Name,
                    Carrier_Id__c = c.Carrier_lookup__r.Id,
                    Carrier_DBA_Name__c = c.Carrier_lookup__r.Novasys_DBA_Name__c,
                    Contract_Status__c = c.Contract_Status__c,
                    Contract_Type__c = c.vlocity_ins__ContractType__c,
                    Effective_Date__c = c.StartDate,
                    TerminationDate__c= c.vlocity_ins__TerminateDate__c,
                    Novasys_Primary_Id__c = c.Novasys_Primary_Id__c,
                    SalesforceId__c = c.Id,
                    State__c = c.State__c,
                    Tech_Awaiting_Novasys_Receipt__c = c.Tech_Awaiting_Novasys_Receipt__c
                );
                if (c.Contract_Status__c == 'Active') {
                    contractEvent.Active__c = True;
                } else {
                    contractEvent.Active__c = False;
                }
                
                if (c.Contract_Status__c != oldContract.Contract_Status__c && c.Contract_Status__c == 'Termed') {
                    Date today = Date.Today();
                    contractEvent.TerminationDate__c = today;
                    c.vlocity_ins__TerminateDate__c= today;
                    update c;
                } else {
                    contractEvent.TerminationDate__c = c.vlocity_ins__TerminateDate__c;
                }
                contractEvents.add(contractEvent);
            }
        }
        System.debug(contractEvents.size());
        if (!contractEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(contractEvents);
        }
        if(!contracts.isEmpty()) {
            update contracts;
        }
    }
}