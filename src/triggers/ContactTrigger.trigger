/**
    * ContactTrigger - Calls handler for all events
    * @author: Dennis Graves - Slalom
    * @version: 1.0
*/

trigger ContactTrigger on Contact bulk (before insert,after insert,before update,after update,before delete,after delete) {

    TriggerFactory.CreateHandler();
}