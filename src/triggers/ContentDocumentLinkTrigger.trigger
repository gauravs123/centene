/*******************************************************************************************************************************
Apex Class Name : ContentDocumentLinkTriggerHandler Trigger
Version         : 1.0
Created Date    : 01/17/2020
Function        : Trigger on ContentDocumentLink Object
Test Class      :
Modification Log: 
Description     : Updated document's visibility for Application attachments (Broker Contract(s))

 * Modification ID         Developer Name          Code review               Date                       Description
*----------------------------------------------------------------------------------------------------------------------------------

* 1.0                      Durga Dutt                                        01/17/2020                 Original Version

*----------------------------------------------------------------------------------------------------------------------------------*/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert) {
    if (!Trigger_Utility.isTriggerDisabled()) {
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                ContentDocumentLinkTriggerHandler.updateContentDocumentLinkVisibility(Trigger.New);
            }
        }
    }
}