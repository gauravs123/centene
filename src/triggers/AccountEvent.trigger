trigger AccountEvent on Account (before update) {
    System.debug('AccountEvent Trigger');
    if (!Trigger_Utility.isTriggerDisabled()) {
        Account[] accountsToSend = new Account[]{};
        Account[] updatedAccounts = new Account[]{};
        Account[] accounts = trigger.new;
        Account_Event__e[] accountEvents = new Account_Event__e[]{};
        Map<Id, Account> accountsOld = trigger.oldMap;
        Account[] filteredAccounts =(Account[]) Trigger_Utility.filterList(trigger.new,'Is_Pilot_Broker__c', false);
        system.debug('>>>>>>>>>>>>>>.'+filteredAccounts);
       
        for (Account acc : filteredAccounts) {
            Account accOld = accountsOld.get(acc.Id);
            system.debug('new Acc->'+ acc.BillingStreet);
            system.debug('old Acc->'+ accOld.BillingStreet);
            system.debug('condition'+ acc.BillingStreet != accOld.BillingStreet);
            if (acc.NPN__c != null && acc.Broker_Tax_Id__c != null) {
                if (acc.Name != accOld.Name) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);   
                } else if (acc.BillingStreet != accOld.BillingStreet) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.BillingCity != accOld.BillingCity) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.BillingState != accOld.BillingState) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.BillingPostalCode != accOld.BillingPostalCode) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.Email__c != accOld.Email__c) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.Fax != accOld.Fax) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.NPN__c!= accOld.NPN__c) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.Broker_Tax_Id__c != accOld.Broker_Tax_Id__c) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                } else if (acc.Phone != accOld.Phone) {
                    acc.Tech_Awaiting_Novasys_Receipt__c = true;
                    updatedAccounts.add(acc);
                }
            }
        }
        
        for (Account acc : updatedAccounts) {
            String postal;
            if (acc.BillingPostalCode != null && acc.BillingPostalCode.length() > 5) {
                postal = acc.BillingPostalCode.split('-')[0];
            } else {
                postal = acc.BillingPostalCode;
            }
            Account_Event__e event = new Account_Event__e(
                Agency_Name__c = acc.Name,
                Agent_Id__c = acc.Agent_Id__c,
                Agent_Master_Id__c = acc.Agent_Master_Id__c,
                Billing_Address_Line_1__c = acc.BillingStreet,
                Billing_City__c = acc.BillingCity,
                Billing_State__c = acc.BillingState,
                Billing_Zip__c = postal,
                Broker_Type__c = acc.Broker_Type__c,
                Contract_Status__c = acc.Broker_Status__c,
                Email__c = acc.Email__c,
                Fax__c = acc.Fax,
                Phone__c = acc.Phone,
                SalesforceId__c = acc.Id,
                Tax_Id__c = acc.Broker_Tax_Id__c,
                Tech_Awaiting_Novasys_Receipt__c = acc.Tech_Awaiting_Novasys_Receipt__c,
                FEIN_Id__c = acc.agentsync__ID_FEIN__c,
                NPN__c = acc.NPN__c
            );
            
            accountEvents.add(event);
        }
        if (!accountEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(accountEvents);
        }
    }
}