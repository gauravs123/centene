trigger AppointmentPlatformEventTrigger on vlocity_ins__ProducerAppointment__c (after insert, after update) {
    System.debug('appointment platform event trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        List<Appointment_Event__e> appointmentEvents = new List<Appointment_Event__e>();
        vlocity_ins__ProducerAppointment__c[] appointments = new vlocity_ins__ProducerAppointment__c[]{};
        Map<Id, vlocity_ins__ProducerAppointment__c> oldApps = trigger.oldMap;
         list<vlocity_ins__ProducerAppointment__c> listNewApp =  [SELECT Id, Tech_Awaiting_Novasys_Receipt__c, Novasys_Primary_Id__c, vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                                                               vlocity_ins__EffectiveDate__c, vlocity_ins__Jurisdiction__c, Contract_Number__c, vlocity_ins__ProducerId__c,
                                                               vlocity_ins__Status__c, vlocity_ins__ExpirationDate__c, vlocity_ins__CarrierId__r.Novasys_DBA_Name__c, Applicable_Appointment_Type__c,IsPilotBroker__c
                                                               FROM vlocity_ins__ProducerAppointment__c 
                                                               WHERE Id IN : Trigger.new
                                                              ];
        list<vlocity_ins__ProducerAppointment__c> listfilteredApp = (list<vlocity_ins__ProducerAppointment__c>)Trigger_Utility.filterList(listNewApp,'IsPilotBroker__c');
        for(vlocity_ins__ProducerAppointment__c appointment :listfilteredApp){
            if(Trigger.isInsert) {
                if (appointment.vlocity_ins__ProducerId__r.Agent_Master_Id__c != null && 
                  (appointment.vlocity_ins__EffectiveDate__c != null && appointment.vlocity_ins__ExpirationDate__c != null) && 
                  appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
                  appointment.Applicable_Appointment_Type__c == true) {
                    Appointment_Event__e appointmentEvent = new Appointment_Event__e(
                        Agent_Master_Id__c = appointment.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                        Carrier_Id__c = appointment.vlocity_ins__CarrierId__c,
                        Carrier_DBA_Name__c = appointment.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                        Effective_Date__c = appointment.vlocity_ins__EffectiveDate__c,
                        Jurisdiction__c = appointment.vlocity_ins__Jurisdiction__c,
                        Producer__c = appointment.vlocity_ins__ProducerId__c,
                        SalesforceId__c = appointment.Id,
                        Contract_Number__c = appointment.Contract_Number__c,
                        Status__c = appointment.vlocity_ins__Status__c,
                        Termination_Date__c = appointment.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = appointment.Tech_Awaiting_Novasys_Receipt__c,
                        Novasys_Primary_Id__c = appointment.Novasys_Primary_Id__c
                    );
                    appointmentEvents.add(appointmentEvent);
                }
            } else if (Trigger.isUpdate) {
            System.debug('appt updated');
                vlocity_ins__ProducerAppointment__c oldApp = oldApps.get(appointment.Id);
                System.debug(oldApp.Contract_Number__c);
                System.debug(appointment.Contract_Number__c);
                if (appointment.vlocity_ins__Status__c != oldApp.vlocity_ins__Status__c) {
                    System.debug('status changed');
                    appointment.Tech_Awaiting_Novasys_Receipt__c = true;
                    appointments.add(appointment);
                } else if (appointment.Contract_Number__c != oldApp.Contract_Number__c) {
                    System.debug('contract number changed');
                    appointment.Tech_Awaiting_Novasys_Receipt__c = true;
                    appointments.add(appointment);
                } else if (appointment.vlocity_ins__EffectiveDate__c != oldApp.vlocity_ins__EffectiveDate__c) {
                    System.debug('effective date changed');
                    appointment.Tech_Awaiting_Novasys_Receipt__c = true;
                    appointments.add(appointment);
                } else if (appointment.vlocity_ins__ExpirationDate__c != oldApp.vlocity_ins__ExpirationDate__c) {
                    System.debug('expiration date changed');
                    appointment.Tech_Awaiting_Novasys_Receipt__c = true;
                    appointments.add(appointment);
                }
           }
       }
       
       for(vlocity_ins__ProducerAppointment__c a : appointments) {
           if (a.vlocity_ins__ProducerId__r.Agent_Master_Id__c != null && 
                 (a.vlocity_ins__EffectiveDate__c != null && 
                 a.vlocity_ins__ExpirationDate__c != null) && 
                 a.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
                 a.Applicable_Appointment_Type__c == true) {    
               
               Appointment_Event__e appointmentEvent = new Appointment_Event__e(
                    Agent_Master_Id__c = a.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                    Carrier_Id__c = a.vlocity_ins__CarrierId__c,
                    Carrier_DBA_Name__c = a.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                    Effective_Date__c = a.vlocity_ins__EffectiveDate__c,
                    Jurisdiction__c = a.vlocity_ins__Jurisdiction__c,
                    Producer__c = a.vlocity_ins__ProducerId__c,
                    SalesforceId__c = a.Id,
                    Contract_Number__c = a.Contract_Number__c,
                    Status__c = a.vlocity_ins__Status__c,
                    Termination_Date__c = a.vlocity_ins__ExpirationDate__c,
                    Tech_Awaiting_Novasys_Receipt__c = a.Tech_Awaiting_Novasys_Receipt__c,
                    Novasys_Primary_Id__c = a.Novasys_Primary_Id__c
                );
                appointmentEvents.add(appointmentEvent);
            }
       }
       if (!appointmentEvents.isEmpty()) {
           Database.SaveResult[] saveResult = EventBus.publish(appointmentEvents);
       }
       if (!appointments.isEmpty()) {
           update appointments;
       }
    }
}