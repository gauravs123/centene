trigger counterSignTrigger on vlocity_ins__Application__c (after update) {
    System.debug('counterSignTrigger: Fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        Map<Id, vlocity_ins__Application__c> oldApplications = trigger.oldMap;
        
        Map<Id, Account> accountsById = new Map<Id, Account>();
        Id[] accountIds = new Id[]{};
         
        Set<Id> brokerIds = new Set<Id>();
        
        list<vlocity_ins__Application__c> listNewApplications = [SELECT Id, vlocity_ins__Status__c,IsPilotBroker__c,Broker__c
                                              FROM vlocity_ins__Application__c
                                              WHERE Id IN :Trigger.new];
        
        list<vlocity_ins__Application__c> listFilteredApplications = Trigger_Utility.filterList(listNewApplications,'IsPilotBroker__c');
        for(vlocity_ins__Application__c application : listFilteredApplications){
            vlocity_ins__Application__c oldApplication = oldApplications.get(application.Id);
            if (!String.isBlank(application.vlocity_ins__Status__c) && oldApplication.vlocity_ins__Status__c != 'Counter Signed' && application.vlocity_ins__Status__c.equals('Counter Signed')) {
                System.debug(application.Broker__c);
                brokerIds.add(application.Broker__c);
            }
        }
        
        Contact[] relatedContacts = [SELECT AccountId, Agent_Id__c, Account_Tax_Id__c, Fax, FFM_Id__c, FirstName, LastName, LastBGCheck__c,
                                        MailingCity, MailingState, MailingPostalCode, MailingStreet, BirthDate, Middle_Name_Initial__c,
                                        Broker_Lead_Status__c, Contact_Type__c, Broker_Status__c, Contract_Term__c, BGCheck_Status__c,
                                        Email, NPN__c, Pay_to_Agent_Id__c, Phone, vlocity_ins__TaxId__c, vlocity_ins__ProducerNumber__c, 
                                        Status__c, Tech_Awaiting_Novasys_Receipt__c, Agent_Master_Id__c, Tax_Id__c, Last_W9_Date__c,OtherPostalCode,
                                        OtherStreet,OtherCity,OtherState
                                        FROM Contact WHERE Id IN: brokerIds];
        System.debug(relatedContacts);
        Contract[] contracts = [SELECT Id, StartDate FROM Contract WHERE vlocity_ins__ProducerId__c IN: brokerIds];
        Date earliestContractDate;
        for (Contract c : contracts) {
            if (c.StartDate < earliestContractDate || earliestContractDate == null) {
                earliestContractDate = c.StartDate;
            }
        }
        for (Contact c : relatedContacts) {
            accountIds.add(c.AccountId);
        }
        
        Account[] accounts = [SELECT Id, Agent_Master_Id__c, Agent_Id__c, Name FROM Account WHERE Id IN: accountIds];
        for (Account a : accounts) {
            accountsById.put(a.Id, a);
        }
        
        if (relatedContacts.size() > 0) {
            for (Contact con : relatedContacts) {
                String otherPostal;
                if(String.isNotBlank(con.OtherPostalCode) && con.OtherPostalCode.length() > 5) {
                    otherPostal = con.OtherPostalCode.split('-')[0];
                } else {
                    otherPostal = con.OtherPostalCode;
                }
                
                String postal;
                if (String.isNotBlank(con.MailingPostalCode) && con.MailingPostalCode.length() > 5) {
                    postal = con.MailingPostalCode.split('-')[0];
                } else {
                    postal = con.MailingPostalCode;
                }
                Update_Centene_Contact__e ContactEvent = new Update_Centene_Contact__e();
                if(String.isBlank(con.AccountId) || !accountsById.containsKey(con.AccountId)){
                    continue;
                }
                
                Account acc = accountsById.get(con.AccountId);
                
                contactEvent.Account_Agent_Master_Id__c = acc.Agent_Master_Id__c;
                contactEvent.Account_Agent_Id__c = acc.Agent_Id__c;
                contactEvent.Account_Name__c = acc.Name;
                contactEvent.Account_Id__c = con.AccountId;
                contactEvent.Agent_Master_Id__c = con.Agent_Master_Id__c;
                contactEvent.Agent_Id__c = con.Agent_Id__c;
                contactEvent.Background_Status__c= con.BGCheck_Status__c;
                contactEvent.BGCheck_Date__c = con.LastBGCheck__c;
                contactEvent.Billing_Address_Line_1__c = con.OtherStreet;
                contactEvent.Billing_City__c = con.OtherCity;
                contactEvent.Billing_State__c = con.OtherState;
                contactEvent.Billing_Zip__c = otherPostal;
                contactEvent.Date_of_Birth__c = con.BirthDate;
                contactEvent.Broker_Lead_Status__c = con.Broker_Lead_Status__c;
                contactEvent.Contact_Type__c = con.Contact_Type__c;
                contactEvent.Contract_Status__c = con.Broker_Status__c;
                contactEvent.Contract_Term__c = con.Contract_Term__c;
                contactEvent.Email__c = con.Email;
                contactEvent.Effective_Date__c = earliestContractDate;
                contactEvent.Fax__c = con.Fax;
                contactEvent.FirstName__c = con.FirstName;
                contactEvent.MiddleName__c = con.Middle_Name_Initial__c;
                contactEvent.FFM_Id__c = con.FFM_Id__c;
                contactEvent.LastName__c = con.LastName;
                contactEvent.NPN__c = con.NPN__c;
                contactEvent.Pay_to_Agent__c = con.Pay_to_Agent_Id__c;
                contactEvent.Phone__c = con.Phone;
                contactEvent.Producer_Number__c = con.vlocity_ins__ProducerNumber__c;
                contactEvent.Resident_Address_Line_1__c = con.MailingStreet;
                contactEvent.Resident_City__c = con.MailingCity;
                contactEvent.Resident_State__c = con.MailingState;
                contactEvent.Resident_Zipcode__c = postal;
                contactEvent.SalesforceId__c = con.Id;
                contactEvent.Status__c = con.Status__c;
                contactEvent.SSN__c = con.Tax_Id__c;
                contactEvent.Tech_Awaiting_Novasys_Receipt__c = con.Tech_Awaiting_Novasys_Receipt__c;
                if(con.Last_W9_Date__c != null) {
                    contactEvent.w9_received__c = true;
                } else {
                    contactEvent.w9_received__c = true;
                }
                
                
                Database.SaveResult saveResultc = EventBus.publish(contactEvent);
                System.debug('Countersign Trigger publish Contact: ' + saveResultc);
                
            }
            
            
        }
    }
    
}