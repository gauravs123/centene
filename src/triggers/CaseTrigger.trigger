/**
* CaseTrigger - Calls handler for all events
* @author: Dianna Guilinger - Slalom
* @version: 1.0
*/

trigger CaseTrigger on Case bulk (before insert,after insert,before update,after update,before delete,after delete) {
    
    TriggerFactory.CreateHandler();
    if (!Trigger_Utility.isTriggerDisabled()) {
        if (Trigger.isInsert && Trigger.isBefore) {
            
            System.debug(' Inserted trigger before');
            List<Case> caseList = Trigger.new;
            
            for(Case caseObj : caseList) {
                System.debug('Case is not null' + caseObj.Application__c);
                List<vlocity_ins__Application__C> applicationObj = [select Broker__C from vlocity_ins__Application__c where id = :caseObj.Application__c];
                Id brokerId;
                if (applicationObj.size() > 0 ) {
                    if (applicationObj[0].Broker__C != null) {
                        brokerId = applicationObj[0].Broker__C;
                    }
                }
                Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Request for Information').getRecordTypeId();
                
                if (brokerId  != null && caseObj.RecordTypeId == caseRecordTypeId) {
                    System.debug('Broker Found ' + brokerId);
                    caseObj.ContactId = brokerId;
                    
                }
            }
        }
    }
}