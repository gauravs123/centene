trigger EducationPlatformEventTrigger on vlocity_ins__ProducerEducation__c (after insert, after update) {
    System.debug('Education Platform Event Trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {    
        List<Continuous_Education_Event__e> educationEvents = new List<Continuous_Education_Event__e>();
        vlocity_ins__ProducerEducation__c[] educations = new vlocity_ins__ProducerEducation__c []{};
        Map<Id, vlocity_ins__ProducerEducation__c> oldEducations = trigger.oldMap;
        list<vlocity_ins__ProducerEducation__c> listNewEducation = [SELECT Id, Tech_Awaiting_Novasys_Receipt__c, Novasys_Primary_Id__c, vlocity_ins__ProducerId__c,vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                                                           vlocity_ins__CertificationNumber__c,Effective_Date__c,vlocity_ins__Jurisdiction__c, vlocity_ins__ProducerId__r.FFM_Id__c,
                                                           Termination_Date__c, Status__c,IsPilotBroker__c FROM vlocity_ins__ProducerEducation__c
                                                           WHERE Id IN : Trigger.new ];
        list<vlocity_ins__ProducerEducation__c> listfilteredEducation = (list<vlocity_ins__ProducerEducation__c> )Trigger_Utility.filterList(listNewEducation ,'IsPilotBroker__c');
        for(vlocity_ins__ProducerEducation__c education : listfilteredEducation ){
            if (Trigger.isInsert) {
                if (education.vlocity_ins__ProducerId__r.Agent_Master_Id__c != null) {
                    Continuous_Education_Event__e educationEvent = new Continuous_Education_Event__e(
                        Agent_Master_Id__c = education.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                        Certification_Number__c = education.vlocity_ins__CertificationNumber__c,
                        Effective_Date__c = education.Effective_Date__c,
                        FFM_Id__c = education.vlocity_ins__ProducerId__r.FFM_Id__c,
                        Novasys_Primary_Id__c = education.Novasys_Primary_Id__c,
                        SalesforceId__c = education.Id,
                        State__c = education.vlocity_ins__Jurisdiction__c,
                        Status__c = education.Status__c,
                        Termination_Date__c = education.Termination_Date__c,
                        Tech_Awaiting_Novasys_Receipt__c = True
                    );
                    educationEvents.add(educationEvent);
                }
            } else if (Trigger.isUpdate) {
                vlocity_ins__ProducerEducation__c oldEducation = oldEducations.get(education.Id);
                if (education.vlocity_ins__CertificationNumber__c != oldEducation.vlocity_ins__CertificationNumber__c) {
                    education.Tech_Awaiting_Novasys_Receipt__c = true;
                    educations.add(education);
                } else if (education.vlocity_ins__Jurisdiction__c != oldEducation.vlocity_ins__Jurisdiction__c){
                    education.Tech_Awaiting_Novasys_Receipt__c = true;
                    educations.add(education);
                } else if (education.Termination_Date__c != oldEducation.Termination_Date__c) {
                    education.Tech_Awaiting_Novasys_Receipt__c = true;
                    educations.add(education);
                } else if (education.Effective_Date__c != oldEducation.Effective_Date__c) {
                    education.Tech_Awaiting_Novasys_Receipt__c = true;
                    educations.add(education);            
                }
            }
        }
        
        for (vlocity_ins__ProducerEducation__c e : educations) {
            Continuous_Education_Event__e educationEvent = new Continuous_Education_Event__e(
                Agent_Master_Id__c = e.vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                Certification_Number__c = e.vlocity_ins__CertificationNumber__c,
                Effective_Date__c = e.Effective_Date__c,
                FFM_Id__c = e.vlocity_ins__ProducerId__r.FFM_Id__c,
                Novasys_Primary_Id__c = e.Novasys_Primary_Id__c,
                SalesforceId__c = e.Id,
                State__c = e.vlocity_ins__Jurisdiction__c,
                Status__c = e.Status__c,
                Termination_Date__c = e.Termination_Date__c,
                Tech_Awaiting_Novasys_Receipt__c = True
            );
            educationEvents.add(educationEvent);
        }
        
        if (!educationEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(educationEvents);
        }
        if(!educations.isEmpty()) {
            update educations;
        }
    }
}