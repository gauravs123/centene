// Modified by RK - Added Tech_Awaiting_Novasys_Receipt__c  in the Query
trigger AgencyLicensePlatformEventTrigger on vlocity_ins__AgencyLicense__c (after insert, after update) {
    System.debug('Agency License Platform Event Trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        List<License_Event__e> licenseEvents = new List<License_Event__e>();
        vlocity_ins__AgencyLicense__c[] licenses = new vlocity_ins__AgencyLicense__c[]{};
        Map<Id, vlocity_ins__AgencyLicense__c> oldLicenses = trigger.oldMap;
        list<vlocity_ins__AgencyLicense__c> listNewlicense = [SELECT Id, vlocity_ins__EffectiveDate__c, Name, vlocity_ins__LicenseType__c, vlocity_ins__Status__c,
                                                     vlocity_ins__AgencyBrokerageId__c, vlocity_ins__Jurisdiction__c, vlocity_ins__ExpirationDate__c, Applicable_License_Type__c,
                                                     Novasys_Primary_Id__c, vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c, Tech_Awaiting_Novasys_Receipt__c ,IsPilotBroker__c
                                                     FROM vlocity_ins__AgencyLicense__c 
                                                     WHERE Id IN: Trigger.new];
        list<vlocity_ins__AgencyLicense__c> listfilteredLicense = (list<vlocity_ins__AgencyLicense__c>)Trigger_Utility.filterList(listNewlicense,'IsPilotBroker__c');
        for(vlocity_ins__AgencyLicense__c license : listfilteredLicense) {
            if (Trigger.isInsert) {
                if(license.vlocity_ins__Status__c == 'Active' &&
                  license.Applicable_License_Type__c == True) {    
                   
                    licenseEvents.add(LicensePlatformEventTriggerHelper.createLicenseEvent(license));
                }
            } else if (Trigger.isUpdate) {
                vlocity_ins__AgencyLicense__c oldLicense = oldLicenses.get(license.Id);
                if (license.vlocity_ins__EffectiveDate__c != oldLicense.vlocity_ins__EffectiveDate__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.Name != oldLicense.Name) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.vlocity_ins__Jurisdiction__c != oldLicense.vlocity_ins__Jurisdiction__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.vlocity_ins__ExpirationDate__c != oldLicense.vlocity_ins__ExpirationDate__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);      
                }
            }
        }
        
        for(vlocity_ins__AgencyLicense__c l : licenses) {
            if(l.vlocity_ins__Status__c == 'Active' &&
              l.Applicable_License_Type__c == True) {        
                
                licenseEvents.add(LicensePlatformEventTriggerHelper.createLicenseEvent(l));
            }    
        }
        if(!licenseEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(licenseEvents);
        }
        if(!licenses.isEmpty()) {
            update licenses;
        }
    }
}