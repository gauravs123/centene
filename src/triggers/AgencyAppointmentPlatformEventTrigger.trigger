trigger AgencyAppointmentPlatformEventTrigger on vlocity_ins__AgencyAppointment__c (after insert, after update) {
    System.debug('agency appointment platform event trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        List<Appointment_Event__e> appEvents = new List<Appointment_Event__e>();
        vlocity_ins__AgencyAppointment__c[] apps = new vlocity_ins__AgencyAppointment__c[]{};
        Map<Id, vlocity_ins__AgencyAppointment__c> oldApps = trigger.oldMap;
        list<vlocity_ins__AgencyAppointment__c> listNewApp = [SELECT Id, vlocity_ins__CarrierId__r.Novasys_DBA_Name__c, vlocity_ins__EffectiveDate__c, Applicable_Appointment_Type__c,
                                                     vlocity_ins__Jurisdiction__c, vlocity_ins__AgencyBrokerageId__c, Contract_Number__c, vlocity_ins__Status__c,
                                                     vlocity_ins__ExpirationDate__c, Novasys_Primary_Id__c, vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c,IsPilotBroker__c
                                                     FROM vlocity_ins__AgencyAppointment__c
                                                     WHERE Id IN: Trigger.new ];
        list<vlocity_ins__AgencyAppointment__c> listfilteredApp = (list<vlocity_ins__AgencyAppointment__c>)Trigger_Utility.filterList(listNewApp,'IsPilotBroker__c');
        for(vlocity_ins__AgencyAppointment__c app : listfilteredApp) {
            if (Trigger.isInsert) {
                if (app.vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c != null &&
                  (app.vlocity_ins__EffectiveDate__c != null && 
                   app.vlocity_ins__ExpirationDate__c != null) && 
                   app.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
                   app.Applicable_Appointment_Type__c == true) {  
                    Appointment_Event__e appointmentEvent = new Appointment_Event__e (
                        Agent_Master_Id__c = app.vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c,
                        Carrier_DBA_Name__c = app.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                        Effective_Date__c = app.vlocity_ins__EffectiveDate__c,
                        Jurisdiction__c = app.vlocity_ins__Jurisdiction__c,
                        Producer__c = app.vlocity_ins__AgencyBrokerageId__c,
                        SalesforceId__c = app.Id,
                        Contract_Number__c = app.Contract_Number__c,
                        Status__c = app.vlocity_ins__Status__c,
                        Termination_Date__c = app.vlocity_ins__ExpirationDate__c,
                        Tech_Awaiting_Novasys_Receipt__c = True,
                        Novasys_Primary_Id__c = app.Novasys_Primary_Id__c
                    );
                    appEvents.add(appointmentEvent);
                }
            } else if (Trigger.isUpdate) {
                vlocity_ins__AgencyAppointment__c oldApp = oldApps.get(app.Id);
                if (app.vlocity_ins__Status__c != oldApp.vlocity_ins__Status__c) {
                    System.debug('status changed');
                    app.Tech_Awaiting_Novasys_Receipt__c = true;
                    apps.add(app);
                } else if (app.Contract_Number__c != oldApp.Contract_Number__c) {
                    System.debug('contract number changed');
                    app.Tech_Awaiting_Novasys_Receipt__c = true;
                    apps.add(app);
                } else if (app.vlocity_ins__EffectiveDate__c != oldApp.vlocity_ins__EffectiveDate__c) {
                    System.debug('effective date changed');
                    app.Tech_Awaiting_Novasys_Receipt__c = true;
                    apps.add(app);
                } else if (app.vlocity_ins__ExpirationDate__c != oldApp.vlocity_ins__ExpirationDate__c) {
                    System.debug('expiration date changed');
                    app.Tech_Awaiting_Novasys_Receipt__c = true;
                    apps.add(app);
                }
            }
        }
        
        for (vlocity_ins__AgencyAppointment__c a : apps) {
            System.debug(a);
            if ((a.vlocity_ins__EffectiveDate__c != null && 
             a.vlocity_ins__ExpirationDate__c != null) && 
             a.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c != null &&
             a.Applicable_Appointment_Type__c == true) {     
                System.debug('fits criteria');
                Appointment_Event__e appointmentEvent = new Appointment_Event__e (
                    Agent_Master_Id__c = a.vlocity_ins__AgencyBrokerageId__r.Agent_Master_Id__c,
                    Carrier_DBA_Name__c = a.vlocity_ins__CarrierId__r.Novasys_DBA_Name__c,
                    Effective_Date__c = a.vlocity_ins__EffectiveDate__c,
                    Jurisdiction__c = a.vlocity_ins__Jurisdiction__c,
                    Producer__c = a.vlocity_ins__AgencyBrokerageId__c,
                    SalesforceId__c = a.Id,
                    Contract_Number__c = a.Contract_Number__c,
                    Status__c = a.vlocity_ins__Status__c,
                    Termination_Date__c = a.vlocity_ins__ExpirationDate__c,
                    Tech_Awaiting_Novasys_Receipt__c = True,
                    Novasys_Primary_Id__c = a.Novasys_Primary_Id__c
                );
                appEvents.add(appointmentEvent);
            }
        }
        
        if(!appEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(appEvents);
        }
        if(!apps.isEmpty()) {
            update apps;
        }
    }
}