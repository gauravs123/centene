trigger LicensePlatformEventTrigger on vlocity_ins__ProducerLicense__c (after insert, after update) {
    System.debug('License Platform Event Trigger fired');
    if (!Trigger_Utility.isTriggerDisabled()) {
        List<License_Event__e> licenseEvents = new List<License_Event__e>();
        vlocity_ins__ProducerLicense__c[] licenses = new vlocity_ins__ProducerLicense__c[]{};
        Map<Id, vlocity_ins__ProducerLicense__c> oldLicenses = trigger.oldMap;
        list<vlocity_ins__ProducerLicense__c> listNewlicense = [SELECT Id, Tech_Awaiting_Novasys_Receipt__c, Novasys_Primary_Id__c, 
                                                        Name,vlocity_ins__ProducerId__c,vlocity_ins__ProducerId__r.Agent_Master_Id__c,
                                                        vlocity_ins__EffectiveDate__c,vlocity_ins__LicenseType__c, Applicable_License_Type__c,
                                                        vlocity_ins__Jurisdiction__c,vlocity_ins__ExpirationDate__c, vlocity_ins__Status__c,IsPilotBroker__c 
                                                        FROM vlocity_ins__ProducerLicense__c
                                                        WHERE Id IN :Trigger.new
                                                       ];
        list<vlocity_ins__ProducerLicense__c> listfilteredLicense = Trigger_Utility.filterList(listNewlicense,'IsPilotBroker__c');
        for (vlocity_ins__ProducerLicense__c license : listfilteredLicense) {
            if (Trigger.isInsert) {
                if (license.vlocity_ins__ProducerId__r.Agent_Master_Id__c != null &&
                  license.Applicable_License_Type__c == True && 
                  license.vlocity_ins__Status__c == 'Active') {
                    licenseEvents.add(LicensePlatformEventTriggerHelper.createLicenseEvent(license));
                }
            } else if (Trigger.isUpdate) {
                System.debug(license.Id);
                vlocity_ins__ProducerLicense__c oldLicense = oldLicenses.get(license.Id);
                if (license.vlocity_ins__EffectiveDate__c != oldLicense.vlocity_ins__EffectiveDate__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.Name != oldLicense.Name) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.vlocity_ins__Jurisdiction__c != oldLicense.vlocity_ins__Jurisdiction__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);
                } else if (license.vlocity_ins__ExpirationDate__c != oldLicense.vlocity_ins__ExpirationDate__c) {
                    license.Tech_Awaiting_Novasys_Receipt__c = true;
                    licenses.add(license);      
                }
            }
        }
        
        for (vlocity_ins__ProducerLicense__c l : licenses) {
            if(l.vlocity_ins__Status__c == 'Active' &&
              l.Applicable_License_Type__c == True) {  
                
                licenseEvents.add(LicensePlatformEventTriggerHelper.createLicenseEvent(l));
            }
        }
        
        if (!licenseEvents.isEmpty()) {
            Database.SaveResult[] saveResult = EventBus.publish(licenseEvents);
        }
        if(!licenses.isEmpty()) {
            update licenses;
        }
    }
}