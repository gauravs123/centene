<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BG_Check_Failure_Email_Alert</fullName>
        <description>BG Check Failure Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Broker_Services/BG_Check_Failed</template>
    </alerts>
</Workflow>
